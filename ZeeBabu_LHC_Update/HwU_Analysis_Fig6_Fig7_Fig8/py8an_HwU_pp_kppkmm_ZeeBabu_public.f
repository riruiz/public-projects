c     py8an_HwU_pp_kppkmm_ZeeBabu.f
c     2022 June
c     R. Ruiz (Institute of Nuclear Physics)
c     Kinematic distributions for 'p p > k++ k-- [QCD]' in Zee-Babu Model
c     
c     Analysis Structure:
c     - Allocate memory / initialize var
c     - PID + global momenta information
c     - Check if charged leptons pass ID requirements
c     - Build charged lepton observables and apply cuts
c     - Cluters hadrons with fastjet (FJ)
c     - Build jet-level observables
c     - Fill histograms
c     
c     Based on HwU format for histogram booking and output.
c     The details of how to process/manipulate the resulting .HwU file,
c     in particular how to plot it using gnuplot, I refer the reader to this
c     FAQ:
c     
c     https://answers.launchpad.net/mg5amcnlo/+faq/2671
c     
c     It mostly relies on using the following madgraph5 module in standalone
c     
c     <MG5_aMC_install_dir>/madgraph/various/histograms.py
c     
c     You can learn about how to run it and what options are available with
c     
c     python <MG5_aMC_install_dir>/madgraph/various/histograms.py --help
c     
C----------------------------------------------------------------------
      SUBROUTINE RCLOS()
C     DUMMY IF HBOOK IS USED
C----------------------------------------------------------------------
      END


C----------------------------------------------------------------------
      SUBROUTINE PYABEG(nnn,wwwi)
C     USER''S ROUTINE FOR INITIALIZATION
C----------------------------------------------------------------------
      use HwU_wgts_info_len
      INCLUDE 'HEPMC.INC'
      include 'reweight0.inc'
      REAL*8 pi
      PARAMETER (PI=3.14159265358979312D0)
      integer j,kk,l,i,nnn
c
c     The type suffix of the histogram title, with syntax 
c     |T@<type_name> is semantic in the HwU format. It allows for
c     various filtering when using the histogram.py module
c     (see comment at the beginning of this file).
c     It is in general a good idea to keep the same title for the
c     same observable (if they use the same range) and differentiate
c     them only using the type suffix.
c
      character*8 HwUtype(2)
      data HwUtype/'|T@Base','|T@Cuts'/
      integer nwgt_analysis
      common/c_analysis/nwgt_analysis
      character*(wgts_info_len) weights_info(max_weight_shower)
     $     ,wwwi(max_weight_shower)
c
      do i=1,nnn
         weights_info(i)=wwwi(i)
      enddo
      nwgt=nnn
c     Initialize histograms
      call HwU_inithist(nwgt,weights_info)
c     Set method for error estimation to '0', i.e., 
c     use Poisson statistics for the uncertainty estimate
      call set_error_estimation(0)
      nwgt_analysis=nwgt
      do i=1,1
         l=0+(i-1)*1000 !=0(ZeeBabu), =1000(TypeII)
c     
c     Histograms to be generated:
c***************************************************************c
c     global books: 1-100
         call HwU_book(l+1,'total rate [pb]                    '//HwUtype(i),4,-2.d0,2.d0)
c***************************************************************c
c     local books: 101-200
         call HwU_book(l+101,'Q_T [GeV]'//HwUtype(i),20,0d0,1d2)
         call HwU_book(l+111,'Q [GeV]'  //HwUtype(i),20,0d0,2d3)
         call HwU_book(l+121,'Test: (E-M) after boost [GeV]'//HwUtype(i),50,-1d0,1d0)
c***************************************************************c
c     lepton books: 201-300
c***************************************************************c
c     hadron books: 301-400
         call HwU_book(l+301,'No. of aKT 0.4 Jets'     //HwUtype(i),20,0d0,20d0)
         call HwU_book(l+302,'No. of aKT 0.4 Jets, p_T>25 GeV'     //HwUtype(i),10,0d0,10d0)
         call HwU_book(l+311,'H_T^{incl}(Had.) [GeV]' //HwUtype(i),20,0d0,500d0)
         call HwU_book(l+312,'p_T^{incl}(Had.) [GeV]' //HwUtype(i),20,0d0,100d0)
         call HwU_book(l+321,'p_T^{j_1} [GeV]'        //HwUtype(i),20,0d0,100d0)
c***************************************************************c
c     lepton-hadron books: 401-500
c***************************************************************c
c     BSM: 501-600
         call HwU_book(l+501,'m_{h} [GeV]' //HwUtype(i), 300,450d0,1350d0)
         call HwU_book(l+503,'m_{k} [GeV]' //HwUtype(i), 300,450d0,1350d0)
         call HwU_book(l+505,'p_T^k [GeV]' //HwUtype(i), 20, 0d0,1400d0)
         call HwU_book(l+506,'y^k'         //HwUtype(i), 20, -2d0,2d0)
c k++
         call HwU_book(l+511,'cos {/Symbol Q}_{Qk} kpp'       //HwUtype(i), 20,-1d0,1d0)
         call HwU_book(l+512,'cos {/Symbol Q}_{Qk} Boost kpp' //HwUtype(i), 20,-1d0,1d0)
         call HwU_book(l+521,'{/Symbol DF}_{QHad.} kpp'    //HwUtype(i), 20,0d0,3.1d0)
c k--
         call HwU_book(l+531,'cos {/Symbol Q}_{Qk} kmm'       //HwUtype(i), 20,-1d0,1d0)
         call HwU_book(l+532,'cos {/Symbol Q}_{Qk} Boost kmm' //HwUtype(i), 20,-1d0,1d0)
         call HwU_book(l+541,'{/Symbol DF}_{QHad.} kmm'    //HwUtype(i), 20,0d0,3.1d0)
c***************************************************************c
c***************************************************************c
c     Misc: 601-700
c***************************************************************c
      enddo

      END
C----------------------------------------------------------------------
      SUBROUTINE PYAEND(IEVTTOT)
C     USER''S ROUTINE FOR TERMINAL CALCULATIONS, HISTOGRAM OUTPUT, ETC
C----------------------------------------------------------------------
      INCLUDE 'HEPMC.INC'
      REAL*8 XNORM,IEVTTOT
      INTEGER I,J,KK,l,nwgt_analysis
      integer NPL
      parameter(NPL=15000)
      common/c_analysis/nwgt_analysis
c     Collect accumulated results. IEVTTOT is such that we need to multiply
c     the results by this factor
      xnorm=ievttot
      call finalize_histograms(nevhep)
c     Write the histograms to disk. 
      open (unit=99,file='MADatNLO.HwU',status='unknown')
      call HwU_output(99,xnorm)
      close (99)
      END

C----------------------------------------------------------------------
      SUBROUTINE PYANAL(nnn,xww)
C     USER''S ROUTINE TO ANALYSE DATA FROM EVENT
C----------------------------------------------------------------------
c      implicit none ! uncomment for bug check
      INCLUDE 'HEPMC.INC'
      include 'reweight0.inc'
c     #      use dpevent_shapes
      integer nnn
c     
      double precision var,varEff
      integer iStatus,iPID
      INTEGER ICHSUM,ICHINI,IHEP,IV,IFV,IST,ID,IJ,ID1,JPR,IDENT
      LOGICAL DIDSOF,FOUNDL,FOUNDN,ISL,ISN
      logical isDebug
      parameter (isDebug = .false.)
      REAL*8 PI
      PARAMETER (PI=3.14159265358979312D0)
      REAL*8 WWW0,TINY,SIGNL,SIGNN
      INTEGER KK,I,L,IL,IN
      DATA TINY/1.0D-6/
      integer nwgt_analysis,max_weight
      common/c_analysis/nwgt_analysis
      integer maxRWGT
      parameter (maxRWGT=100)
      double precision wgtxsecRWGT(maxRWGT)
      parameter (max_weight=maxscales*maxscales+maxpdfs+maxRWGT+1)
      double precision ww(max_weight),www(max_weight)
      double precision xww(max_weight)
      common/cww/ww
c     
c     PID counters
      integer ii,jj
      integer nrHads
      integer nrJetsKT0p4,nrKT0p4PT25
      integer nrMCGams,nrGams
      integer nrMCLeps,nrLeps
      integer nrMCEles,nrEles
      integer nrMCPoss,nrPoss
      integer nrMCMums,nrMums
      integer nrMCMups,nrMups
      integer nrMCTams,nrTams
      integer nrMCTaps,nrTaps
      integer nrMCBSMs(1:4),nrBSMs(1:4)
      integer,parameter,dimension(1:6) ::
c     &     bsmPID = (/37,-37,38,-38/)
     &     bsmPID = (/37,-37,38,-38,61,-61/)
c     global momentum containers
      double precision pTot(0:3),pVis(0:3),pMET(0:3),pSys(0:3),q(0:3)
      double precision pTTot, met, pTSys, hardQ, qT
      double precision cosThetaQk(2), cosThetaQkBoost(2), dphiKX(2)
      double precision pTBSM,rapBSM
      double precision pBSM1(0:3),pBSM2(0:3),pBSM3(0:3),pBSM4(0:3)
      double precision pBSMHardFrame(0:3)
c     lepton and BSM momentum containers
      double precision, allocatable :: mcGamBag(:,:), gamBag(:,:)
      double precision, allocatable :: mcLepBag(:,:), lepBag(:,:)
      double precision, allocatable :: mcEleBag(:,:), eleBag(:,:)
      double precision, allocatable :: mcPosBag(:,:), posBag(:,:)
      double precision, allocatable :: mcMumBag(:,:), mumBag(:,:)
      double precision, allocatable :: mcMupBag(:,:), mupBag(:,:)
      double precision, allocatable :: mcTamBag(:,:), tamBag(:,:)
      double precision, allocatable :: mcTapBag(:,:), tapBag(:,:)
      double precision, allocatable :: mcBS1Bag(:,:), bs1Bag(:,:)
      double precision, allocatable :: mcBS2Bag(:,:), bs2Bag(:,:)
      double precision, allocatable :: mcBS3Bag(:,:), bs3Bag(:,:)
      double precision, allocatable :: mcBS4Bag(:,:), bs4Bag(:,:)
c     hadron momentum containers
      double precision, allocatable :: jetBagKT0p4(:,:)
      double precision pHad(0:3)
      double precision pTHad, pTj1, pTj1KT0p4
      double precision HTincl_Had, HTincl_KT0p4
c     fastjet
      integer, allocatable :: whichJet(:)
c     call fastjetppgenkt(pHadFJ,NHEP,rfj,sycut,palg,pJetFJ,nrJets,jetX)
      double precision, allocatable :: pHadFJ(:,:),pJetFJKT0p4(:,:) ! FJ in/outputs
      double precision  palg(2),rfj(2),minPTjDef(2),maxRapjDef(2) ! FJ inputs
c***********************c
c     kinematic cuts    c
c***********************c
      logical doCuts
      parameter (doCuts=.false.)
      logical passCuts
      logical passCuts_PIDX
      integer tmpIsIso          !,minNrLep,maxNrLep
      double precision tmpDR,tmpPT !,tmpRap

      double precision dRljMin,dRlaMin,dRllMin,dRllEps
      double precision pTaMin,pTlMin,pTjMin,pTjTag
      double precision etaMaxGlobal,etaMaxECAL,etaMaxHCAL,etaMaxMUON
c     double precision pTlTag
c     parameter (pTlTag=10.000d0)
      parameter (pTaMin=10.000d0)
      parameter (pTlMin=10.000d0)
      parameter (pTjMin=25.000d0)
      parameter (pTjTag=50.000d0)
      parameter (etaMaxGlobal=10d0)
      parameter (etaMaxECAL=2.4000d0)
      parameter (etaMaxHCAL=4.5000d0)
      parameter (etaMaxMUON=2.5000d0) ! not used
      parameter (dRlaMin=0.3d0)
      parameter (dRllMin=0.3d0)
      parameter (dRllEps=2.0d-4) ! triviality; in kin_functions.f, r2(lA,lA)=2e-8
      parameter (dRljMin=0.3d0)
c***************************c
c     external functions    c
c***************************c
c     double precision plBag
c     call from Source/kin_functions.f
      double precision SumDot,DOT,pT,rap,r2,DELTA_PHI
      external SumDot,DOT,pT,rap,r2,DELTA_PHI
c     call from py8an_HwU_Analysis_Lib.f
      double precision getrapidity,getRapFromP5,getPT2fromP5,getPTfromP5
      external getrapidity,getRapFromP5,getPT2fromP5,getPTfromP5
      double precision getCosTheta,getMfromP4,getRapFromP4,getPTfromP4
      external getCosTheta,getMfromP4,getRapFromP4,getPTfromP4
c     call from py8an_HwU_Analysis_PID.f
      logical pid_IsFidKinCand  
      external pid_IsFidKinCand
c****************************************************************
c     allocate space for FJ inputs and truth-level objects
c     allocate max size now and dynamically resize later
c****************************************************************
      allocate( whichJet(NHEP) )
      allocate( pHadFJ(4,NHEP) )
      allocate( pJetFJKT0p4(4,NHEP) )
      allocate( mcGamBag(0:3,NHEP) )
      allocate( mcLepBag(0:3,NHEP) )
      allocate( mcEleBag(0:3,NHEP) )
      allocate( mcPosBag(0:3,NHEP) )
      allocate( mcMumBag(0:3,NHEP) )
      allocate( mcMupBag(0:3,NHEP) )
      allocate( mcTamBag(0:3,NHEP) )
      allocate( mcTapBag(0:3,NHEP) )
      allocate( mcBS1Bag(0:3,NHEP) )
      allocate( mcBS2Bag(0:3,NHEP) )
      allocate( mcBS3Bag(0:3,NHEP) )
      allocate( mcBS4Bag(0:3,NHEP) )
c****************************************************************
c     set weights; do not touch
c****************************************************************
      if(nnn.eq.0) ww(1)=1d0
      do i=1,nnn
         ww(i)=xww(i)
      enddo
c     
c     if first weight is zero, then stop
      IF (WW(1).EQ.0D0) THEN
         WRITE(*,*)'WW(1) = 0. Stopping'
         STOP
      ENDIF
c     
C     INCOMING PARTONS MAY TRAVEL IN THE SAME DIRECTION: 
c     IT''S A POWER-SUPPRESSED EFFECT, SO THROW THE EVENT AWAY
      IF(SIGN(1.D0,PHEP(3,1)).EQ.SIGN(1.D0,PHEP(3,2)))THEN
         WRITE(*,*)'WARNING 111 IN PYANAL'
         GOTO 999
      ENDIF
      DO I=1,nwgt_analysis
         WWW(I)=EVWGT*ww(i)/ww(1)
c     write(*,*)'WWW(',I,') = ',WWW(I)
      ENDDO
c     event counter
      var     = 1.d0
      varEff = WWW(1)/abs(WWW(1))
c***************************************************************c
c     initialize                                                 c
c***************************************************************c
c     analysis logic: assume good event, then find reason to reject
      passCuts = .true.
c     composite four-momentum
      call resetP4(pTot)
      call resetP4(pVis)
      call resetP4(pSys)
      call resetP4(q)
      call resetP4(pMET)
      call resetP4(pBSM1)
      call resetP4(pBSM2)
      call resetP4(pBSM3)
      call resetP4(pBSM4)
      call resetP4(pHad)
      call resetP4(pBSMHardFrame)
      call resetP4(pAHardFrame)
c     allocated four-vectors
      do ii=1,NHEP
         call resetP4(pHadFJ(1,ii))
         call resetP4(pJetFJKT0p4(1,ii))
         call resetP4(mcGamBag(0,ii))
         call resetP4(mcLepBag(0,ii))
         call resetP4(mcEleBag(0,ii))
         call resetP4(mcPosBag(0,ii))
         call resetP4(mcMumBag(0,ii))
         call resetP4(mcMupBag(0,ii))
         call resetP4(mcTamBag(0,ii))
         call resetP4(mcTapBag(0,ii))
         call resetP4(mcBS1Bag(0,ii))
         call resetP4(mcBS2Bag(0,ii))
         call resetP4(mcBS3Bag(0,ii))
         call resetP4(mcBS4Bag(0,ii))
         whichJet(ii) = 0
      enddo
c     counters
      nrHads = 0
      nrJetsKT0p4 = 0
      nrKT0p4PT25 = 0
c*********************** ! counters
      nrGams = 0
      nrLeps = 0
      nrEles = 0
      nrPoss = 0
      nrMums = 0
      nrMups = 0
      nrTams = 0
      nrTaps = 0
      nrBSMs(1) = 0
      nrBSMs(2) = 0
      nrBSMs(3) = 0
      nrBSMs(4) = 0
c*********************** ! counters
      nrMCGams = 0
      nrMCLeps = 0
      nrMCEles = 0
      nrMCPoss = 0
      nrMCMums = 0
      nrMCMups = 0
      nrMCTams = 0
      nrMCTaps = 0
      nrMCBSMs(1) = 0
      nrMCBSMs(2) = 0
      nrMCBSMs(3) = 0
      nrMCBSMs(4) = 0
c***********************
c     set kinematics: global
      do kk=1,2
            cosThetaQk(kk) = -999d0
            cosThetaQkBoost(kk) = -999d0
            dphiKX(kk) = -999d0
      enddo
      qT    = -999d0
      met   = -999d0
      pTTot = -999d0
      pTSys = -999d0
      pTBSM = -999d0
      rapBSM = -999d0
      hardQ  = -999d0
c     set kinematics: lep
c     set kinematics: had
      pTHad = -999d0
      HTincl_Had   = 0d0        ! initialize to zero
      HTincl_KT0p4 = 0d0        ! initialize to zero
      pTj1 = -999d0
      pTj1KT0p4 = -999d0
c     set kinematics: misc
c**************************************************c
c     Running over objects in an event c
c**************************************************c
c IHEP = integer running over number of particles in event
      DO IHEP=1,NHEP
c     flag final-state objects
         iStatus=ISTHEP(IHEP)   ! particle status
         iPID=IDHEP(IHEP)       ! PID of particle IHEP
         if(isDebug.and.iStatus.eq.1) then 
            WRITE(*,*)'particle status = ', iStatus
            write(*,*)'PID of particle = ', iPID
            write(*,*)'\n'
         endif
c***************************************************************c   
c     look for final-state objects:
c***************************************************************c   
c     collect visible, final-state p4
         if(iStatus.eq.1) then
c***************************************************************c   
c     Note: logic here is decoupled from logic for charged leptons
c     and hadrons. 
c     => global momenta are collected here
c     => pVis and pTot built here
c     Note: neglecting beam remnant at |eta|>10
c     
c     add momentum to pTot as long as |eta|<10: 
            if(abs(getRapFromP5(PHEP(1,IHEP))).lt.etaMaxGlobal) then
               call addP4fromP5_Helas_Sgn(pTot,PHEP(1,IHEP),1)
            endif
c     is a neutrino?
            if(abs(iPID).eq.12.or.
     &         abs(iPID).eq.14.or.
     &         abs(iPID).eq.16) then
c     doing nothing is faster than checking if iPIDs are not neutrinos
            else
c     add momentum to pVis as long as |eta|<10: 
               if(abs(getRapFromP5(PHEP(1,IHEP))).lt.etaMaxGlobal) then
                  call addP4fromP5_Helas_Sgn(pVis,PHEP(1,IHEP),1)
               endif
            endif
c********************************************************************c
c     collect momenta of charged leptons, photons, and BSM
c********************************************************************c
            select case (iPID)
c     is a neutrino?
            case(12,-12,14,-14,16,-16)
c     is a photon?
            case (22)
               nrMCGams = nrMCGams + 1
               call getP4fromP5_Helas(mcLepBag(0,nrMCGams),PHEP(1,IHEP))
c     is a e-?
            case (11)
               nrMCEles = nrMCEles + 1
               nrMCLeps = nrMCLeps + 1
               call getP4fromP5_Helas(mcEleBag(0,nrMCEles),PHEP(1,IHEP))
               call getP4fromP5_Helas(mcLepBag(0,nrMCLeps),PHEP(1,IHEP))
c     is a e+?
            case (-11)
               nrMCPoss = nrMCPoss + 1
               nrMCLeps = nrMCLeps + 1
               call getP4fromP5_Helas(mcPosBag(0,nrMCPoss),PHEP(1,IHEP))
               call getP4fromP5_Helas(mcLepBag(0,nrMCLeps),PHEP(1,IHEP))
c     is a mu-?
            case (13)
               nrMCMums = nrMCMums + 1
               nrMCLeps = nrMCLeps + 1
               call getP4fromP5_Helas(mcMumBag(0,nrMCMums),PHEP(1,IHEP))
               call getP4fromP5_Helas(mcLepBag(0,nrMCLeps),PHEP(1,IHEP))
c     is a mu+?
            case (-13)
               nrMCMups = nrMCMups + 1
               nrMCLeps = nrMCLeps + 1
               call getP4fromP5_Helas(mcMupBag(0,nrMCMups),PHEP(1,IHEP))
               call getP4fromP5_Helas(mcLepBag(0,nrMCLeps),PHEP(1,IHEP))
c     is a tau-
            case (15)
               nrMCTams = nrMCTams + 1
               nrMCLeps = nrMCLeps + 1
               call getP4fromP5_Helas(mcTamBag(0,nrMCTams),PHEP(1,IHEP))
               call getP4fromP5_Helas(mcLepBag(0,nrMCLeps),PHEP(1,IHEP))
c     is a tau+?
            case (-15)
               nrMCTaps = nrMCTaps + 1
               nrMCLeps = nrMCLeps + 1
               call getP4fromP5_Helas(mcTapBag(0,nrMCTaps),PHEP(1,IHEP))
               call getP4fromP5_Helas(mcLepBag(0,nrMCLeps),PHEP(1,IHEP))
c     is BSM1?
            case (bsmPID(1))
               nrMCBSMs(1) = nrMCBSMs(1) + 1
               call getP4fromP5_Helas(mcBS1Bag(0,nrMCBSMs(1)),PHEP(1,IHEP))
               call addP4fromP5_Helas(pSys,PHEP(1,IHEP))
c     is BSM2?
            case (bsmPID(2))
               nrMCBSMs(2) = nrMCBSMs(2) + 1
               call getP4fromP5_Helas(mcBS2Bag(0,nrMCBSMs(2)),PHEP(1,IHEP))
               call addP4fromP5_Helas(pSys,PHEP(1,IHEP))
c     is BSM3?
            case (bsmPID(3))
               nrMCBSMs(3) = nrMCBSMs(3) + 1
               call getP4fromP5_Helas(mcBS3Bag(0,nrMCBSMs(3)),PHEP(1,IHEP))
               call addP4fromP5_Helas(pSys,PHEP(1,IHEP))
c     is BSM4?
            case (bsmPID(4))
               nrMCBSMs(4) = nrMCBSMs(4) + 1
               call getP4fromP5_Helas(mcBS4Bag(0,nrMCBSMs(4)),PHEP(1,IHEP))
               call addP4fromP5_Helas(pSys,PHEP(1,IHEP))
c     is BSM3/5? (recycle unsued Bag and mcBag) ! see bsmPID at ~L195
            case (bsmPID(5))
               nrMCBSMs(3) = nrMCBSMs(3) + 1
               call getP4fromP5_Helas(mcBS3Bag(0,nrMCBSMs(3)),PHEP(1,IHEP))
               call addP4fromP5_Helas(pSys,PHEP(1,IHEP))
c     is BSM4/6? (recycle unused Bag and mcBag) ! see bsmPID at ~L195
            case (bsmPID(6))
               nrMCBSMs(4) = nrMCBSMs(4) + 1
               call getP4fromP5_Helas(mcBS4Bag(0,nrMCBSMs(4)),PHEP(1,IHEP))
               call addP4fromP5_Helas(pSys,PHEP(1,IHEP))
c********************************************************************c
c     collect momenta of hadrons
c********************************************************************c
c     warning: assumes only hadrons have PID > 100
c     (check if stable, BSM possesses PID > 100)
c     (if running w/o hadronization, collect instead -b,..,-u,g,u.,b)
c     is a hadron, light quark, bottom, or g? 
            case ( : -100,-5:5,21,100 : )
               if(abs(getRapFromP5(PHEP(1,IHEP))).lt.etaMaxHCAL ) then ! is with HCAL?
                  nrHads     = nrHads+1
                  HTincl_Had = HTincl_Had + getPTfromP5(PHEP(1,IHEP)) ! add momentum magnitude
                  call addP4fromP5_Helas_Sgn(pHad,PHEP(1,IHEP),1) ! add momentum vector
c     call addP4fromP5_Helas_Sgn(pVT,PHEP(1,IHEP),-1)     ! sub momentum vector
                  call getP4fromP5_FJ(pHadFJ(1,nrHads),PHEP(1,IHEP)) ! collect hadron momenta for clustering
               endif
            case  default
               WRITE(*,*)'Error: unknown particle species. status = ',iStatus
               WRITE(*,*)'Error: unknown particle species. PID = ',iPID
               stop -99
            end select
c********************************************************************c
c********************************************************************c
         endif                  ! if(iStatus.eq.1)
c***************************************************************c   
c***************************************************************c   
      enddo                     ! DO IHEP=1,NHEP  
c***************************************************************c
c***************************************************************c   
c     Misc. Observables
c***************************************************************c
c***************************************************************c   
      met = pt(pvis)
      pTTot = pt(pTot)
      pTSys = pt(pSys)
      pTHad = pt(pHad)
      if(pTHad.lt.TINY) goto 999 ! skip events with pTHad < 1 keV (tiny=1d-6)
      do jj=1,3
            q(jj) = -pHad(jj)
      enddo
      q(0) = pHad(0)
      qT = pt(q)
      hardQ = getMfromP4(pSys)
c***************************************************************c
c     allocate memory for leptons; remember to deallocate
c***************************************************************c
      allocate( gamBag(0:3,nrMCGams) )
      allocate( lepBag(0:3,nrMCLeps) )
      allocate( eleBag(0:3,nrMCEles) )
      allocate( posBag(0:3,nrMCPoss) )
      allocate( mumBag(0:3,nrMCMums) )
      allocate( mupBag(0:3,nrMCMups) )
      allocate( tamBag(0:3,nrMCTams) )
      allocate( tapBag(0:3,nrMCTaps) )
      allocate( bs1Bag(0:3,nrMCBSMs(1)) )
      allocate( bs2Bag(0:3,nrMCBSMs(2)) )
      allocate( bs3Bag(0:3,nrMCBSMs(3)) )
      allocate( bs4Bag(0:3,nrMCBSMs(4)) )
c****************************************************************c
c     check if charged lepton is isolated; if not, call hadron    
c****************************************************************c
c***************************************************************c
c     collect good photons and charged leptons
c***************************************************************c
c***************************************************************c
c     collect good photons
c      do jj=1,nrMCGams
c         call resetP4(gamBag(0,jj))
c         if(pid_IsFidKinCand(doCuts,mcGamBag(0,jj),etaMaxECAL,pTaMin)) then
c     check is isolated from all charged leptons
c            tmpIsIso = 1           ! assume isolated
c            do ii=1,nrMCGams
c               tmpDR = sqrt(r2(mcGamBag(0,jj),mcLepBag(0,ii)))
c               if(tmpDR.gt.dRllEps .and. tmpDR.lt.dRlaMin.and.doCuts) then
c                  tmpIsIso = tmpIsIso * 0
c               endif
c            enddo
c     check why pid_IsFidKinCand==true:
c            if(.not.doCuts) then
c               nrGams = nrGams + 1
c               call addP4fromP4_Helas(gamBag(0,nrGams),mcGamBag(0,jj))
c            elseif(tmpIsIso>0) then
c               nrGams = nrGams + 1
c               call addP4fromP4_Helas(gamBag(0,nrGams),mcGamBag(0,jj))
c            endif
c         endif
c      enddo
c***************************************************************c
c     collect good leps
      do jj=1,nrMCLeps
         call resetP4(lepBag(0,jj))
         if(pid_IsFidKinCand(doCuts,mcLepBag(0,jj),etaMaxECAL,pTlMin)) then
c     check is isolated from all other charged leptons
         tmpIsIso = 1           ! assume isolated
         do ii=1,nrMCLeps
            tmpDR = sqrt(r2(mcLepBag(0,jj),mcLepBag(0,ii)))
            if(tmpDR.gt.dRllEps .and. tmpDR.lt.dRllMin.and.doCuts) then
               tmpIsIso = tmpIsIso * 0
            endif
         enddo
c     check why pid_IsFidKinCand==true:
         if(.not.doCuts) then
            nrLeps = nrLeps + 1
            call addP4fromP4_Helas(lepBag(0,nrLeps),mcLepBag(0,jj))
         elseif(tmpIsIso>0) then
            nrLeps = nrLeps + 1
            call addP4fromP4_Helas(lepBag(0,nrLeps),mcLepBag(0,jj))
         endif
      endif
      enddo
c***************************************************************c
c     collect good eles
      do jj=1,nrMCEles
         call resetP4(eleBag(0,jj))
         if(pid_IsFidKinCand(doCuts,mcEleBag(0,jj),etaMaxECAL,pTlMin)) then
c     check is isolated from all other charged leptons
         tmpIsIso = 1           ! assume isolated
         do ii=1,nrMCLeps
            tmpDR = sqrt(r2(mcEleBag(0,jj),mcLepBag(0,ii)))
            if(tmpDR.gt.dRllEps .and. tmpDR.lt.dRllMin.and.doCuts) then
               tmpIsIso = tmpIsIso * 0
            endif
         enddo
c     check why pid_IsFidKinCand==true:
         if(.not.doCuts) then
            nrEles = nrEles + 1
            call addP4fromP4_Helas(eleBag(0,nrEles),mcEleBag(0,jj))
         elseif(tmpIsIso>0) then
            nrEles = nrEles + 1
            call addP4fromP4_Helas(eleBag(0,nrEles),mcEleBag(0,jj))
         endif
      endif
      enddo
c***************************************************************c
c     collect good poss
      do jj=1,nrMCPoss
         call resetP4(posBag(0,jj))
         if(pid_IsFidKinCand(doCuts,mcPosBag(0,jj),etaMaxECAL,pTlMin)) then
c     check is isolated from all other charged leptons
         tmpIsIso = 1           ! assume isolated
         do ii=1,nrMCLeps
            tmpDR = sqrt(r2(mcPosBag(0,jj),mcLepBag(0,ii)))
            if(tmpDR.gt.dRllEps .and. tmpDR.lt.dRllMin.and.doCuts) then
               tmpIsIso = tmpIsIso * 0
            endif
         enddo
c     check why pid_IsFidKinCand==true:
         if(.not.doCuts) then
            nrPoss = nrPoss + 1
            call addP4fromP4_Helas(posBag(0,nrPoss),mcPosBag(0,jj))
         elseif(tmpIsIso>0) then
            nrPoss = nrPoss + 1
            call addP4fromP4_Helas(posBag(0,nrPoss),mcPosBag(0,jj))
         endif
      endif
      enddo
c***************************************************************c
c     collect good mums
      do jj=1,nrMCMums
         call resetP4(mumBag(0,jj))
         if(pid_IsFidKinCand(doCuts,mcMumBag(0,jj),etaMaxECAL,pTlMin)) then
c     check is isolated from all other charged leptons
         tmpIsIso = 1           ! assume isolated
         do ii=1,nrMCLeps
            tmpDR = sqrt(r2(mcMumBag(0,jj),mcLepBag(0,ii)))
            if(tmpDR.gt.dRllEps .and. tmpDR.lt.dRllMin.and.doCuts) then
               tmpIsIso = tmpIsIso * 0
            endif
         enddo
c     check why pid_IsFidKinCand==true:
         if(.not.doCuts) then
            nrMums = nrMums + 1
            call addP4fromP4_Helas(mumBag(0,nrMums),mcMumBag(0,jj))
         elseif(tmpIsIso>0) then
            nrMums = nrMums + 1
            call addP4fromP4_Helas(mumBag(0,nrMums),mcMumBag(0,jj))
         endif
      endif
      enddo
c***************************************************************c
c     collect good mups
      do jj=1,nrMCMups
         call resetP4(mupBag(0,jj))
         if(pid_IsFidKinCand(doCuts,mcMupBag(0,jj),etaMaxECAL,pTlMin)) then
c     check is isolated from all other charged leptons
         tmpIsIso = 1           ! assume isolated
         do ii=1,nrMCLeps
            tmpDR = sqrt(r2(mcMupBag(0,jj),mcLepBag(0,ii)))
            if(tmpDR.gt.dRllEps .and. tmpDR.lt.dRllMin.and.doCuts) then
               tmpIsIso = tmpIsIso * 0
            endif
         enddo
c     check why pid_IsFidKinCand==true:
         if(.not.doCuts) then
            nrMups = nrMups + 1
            call addP4fromP4_Helas(mupBag(0,nrMups),mcMupBag(0,jj))
         elseif(tmpIsIso>0) then
            nrMups = nrMups + 1
            call addP4fromP4_Helas(mupBag(0,nrMups),mcMupBag(0,jj))
         endif
      endif
      enddo
c***************************************************************c
c     collect good tams
      do jj=1,nrMCTams
         call resetP4(tamBag(0,jj))
         if(pid_IsFidKinCand(doCuts,mcTamBag(0,jj),etaMaxECAL,pTlMin)) then
c     check is isolated from all other charged leptons
         tmpIsIso = 1           ! assume isolated
         do ii=1,nrMCLeps
            tmpDR = sqrt(r2(mcTamBag(0,jj),mcLepBag(0,ii)))
            if(tmpDR.gt.dRllEps .and. tmpDR.lt.dRllMin.and.doCuts) then
               tmpIsIso = tmpIsIso * 0
            endif
         enddo
c     check why pid_IsFidKinCand==true:
         if(.not.doCuts) then
            nrTams = nrTams + 1
            call addP4fromP4_Helas(tamBag(0,nrTams),mcTamBag(0,jj))
         elseif(tmpIsIso>0) then
            nrTams = nrTams + 1
            call addP4fromP4_Helas(tamBag(0,nrTams),mcTamBag(0,jj))
         endif
      endif
      enddo
c***************************************************************c
c     collect good taps
      do jj=1,nrMCTaps
         call resetP4(tapBag(0,jj))
         if(pid_IsFidKinCand(doCuts,mcTapBag(0,jj),etaMaxECAL,pTlMin)) then
c     check is isolated from all other charged leptons
         tmpIsIso = 1           ! assume isolated
         do ii=1,nrMCLeps
            tmpDR = sqrt(r2(mcTapBag(0,jj),mcLepBag(0,ii)))
            if(tmpDR.gt.dRllEps .and. tmpDR.lt.dRllMin.and.doCuts) then
               tmpIsIso = tmpIsIso * 0
            endif
         enddo
c     check why pid_IsFidKinCand==true:
         if(.not.doCuts) then
            nrTaps = nrTaps + 1
            call addP4fromP4_Helas(tapBag(0,nrTaps),mcTapBag(0,jj))
         elseif(tmpIsIso>0) then
            nrTaps = nrTaps + 1
            call addP4fromP4_Helas(tapBag(0,nrTaps),mcTapBag(0,jj))
         endif
      endif
      enddo
c***************************************************************c      
c     move mcBag -> Bag
      do jj=1,nrMCBSMs(1)
         call resetP4(bs1Bag(0,jj))
         nrBSMs(1) = nrBSMs(1) + 1
         call addP4fromP4_Helas(bs1Bag(0,nrBSMs(1)),mcBS1Bag(0,jj))
      enddo      
      do jj=1,nrMCBSMs(2)
         call resetP4(bs2Bag(0,jj))
         nrBSMs(2) = nrBSMs(2) + 1
         call addP4fromP4_Helas(bs2Bag(0,nrBSMs(2)),mcBS2Bag(0,jj))
      enddo      
      do jj=1,nrMCBSMs(3)
         call resetP4(bs3Bag(0,jj))
         nrBSMs(3) = nrBSMs(3) + 1
         call addP4fromP4_Helas(bs3Bag(0,nrBSMs(3)),mcBS3Bag(0,jj))
      enddo      
      do jj=1,nrMCBSMs(4)
         call resetP4(bs4Bag(0,jj))
         nrBSMs(4) = nrBSMs(4) + 1
         call addP4fromP4_Helas(bs4Bag(0,nrBSMs(4)),mcBS4Bag(0,jj))
      enddo      
c***************************************************************c
c***************************************************************c
c     sanity
c***************************************************************c
c***************************************************************c
      if( (nrMCLeps - nrMCTaps - nrMCTams
     &              - nrMCMups - nrMCMums 
     &              - nrMCEles - nrMCPoss ).ne.0 ) then
         write (*,*) "Error: nrMCLeps != nrMCTaps + nrMCTams ",
     &            "+ nrMCMups + nrMCMums + nrMCEles + nrMCPoss:",
     &   nrMCLeps,nrMCTaps,nrMCTams,nrMCMups,nrMCMums,nrMCEles,nrMCPoss
         WRITE(*,*)'ERROR 776 IN PY8ANAL'
         STOP
      endif

      if( (nrLeps - nrTaps - nrTams 
     &            - nrMups - nrMums 
     &            - nrEles - nrPoss ).ne.0 ) then
         write (*,*) "Warning: nrMCLeps != nrMCTaps + nrMCTams ",
     &            "+ nrMCMups + nrMCMums + nrMCEles + nrMCPoss:",
     &   nrMCLeps,nrMCTaps,nrMCTams,nrMCMups,nrMCMums,nrMCEles,nrMCPoss      
         write (*,*) "Error: nrLeps != nrTaps + nrTams ",
     &               "+ nrMups + nrMums+ nrEles + nrPoss:",
     &   nrLeps,nrTaps,nrTams,nrMups,nrMums,nrEles,nrPoss
         WRITE(*,*)'ERROR 775 IN PY8ANAL'
         STOP
      endif

      do jj=1,4
            if(nrMCBSMs(jj).ne.nrBSMs(jj)) then
                  write(*,*)'Error: nrMCBSMs(jj) != nrBSMs(jj) for jj=',jj,
     &                      ', nrMCBSMs,nrBSMs = ',nrMCBSMs(jj),nrBSMs(jj)
                  WRITE(*,*)'ERROR 774 IN PY8ANAL'
                  stop
            endif
      enddo 
c***************************************************************c
c***************************************************************c 
c     sort good charged leptons by by pT
      if(nrGams.gt.0)   call sortP4ByPT(gamBag,nrGams)
      if(nrLeps.gt.0)   call sortP4ByPT(lepBag,nrLeps)
      if(nrEles.gt.0)   call sortP4ByPT(eleBag,nrEles)
      if(nrPoss.gt.0)   call sortP4ByPT(posBag,nrPoss)
      if(nrMums.gt.0)   call sortP4ByPT(mumBag,nrMums)
      if(nrMups.gt.0)   call sortP4ByPT(mupBag,nrMups)
      if(nrTams.gt.0)   call sortP4ByPT(tamBag,nrTams)
      if(nrTaps.gt.0)   call sortP4ByPT(tapBag,nrTaps)
      if(nrBSMs(1).gt.0)     call sortP4ByPT(bs1Bag,nrBSMs(1))
      if(nrBSMs(2).gt.0)     call sortP4ByPT(bs2Bag,nrBSMs(2))
      if(nrBSMs(3).gt.0)     call sortP4ByPT(bs3Bag,nrBSMs(3))
      if(nrBSMs(4).gt.0)     call sortP4ByPT(bs4Bag,nrBSMs(4))
c Note:       integer,parameter,dimension(1:4) ::
c     &     bsmPID = (/37,-37,38,-38,61,-61/) ! see bsmPID at ~L195            
c***************************************************************c
c     apply cuts
c***************************************************************c
c
c***************************************************************c
c     build observables
c***************************************************************c
      if(nrBSMs(3).gt.0) then 
c 1.-2. first, boost k++ to rest frame of pSys=p(k++k--)
            do jj=0,3
                  pBSMHardFrame(jj) = bs3Bag(jj,1)
            enddo
            call dboost_Sgn(pSys,hardQ,pBSMHardFrame,-1d0) 
c 3. get polar angle of k++ w.r.t. pSys in pSys frame
            cosThetaQkBoost(1) = getCosTheta(pSys,pBSMHardFrame)
c 4.boost again for sanity check later:
            call dboost_Sgn(pBSMHardFrame,getMfromP4(bs3Bag(0,1)),pBSMHardFrame,-1d0)            
cc no do again but for k--
            do jj=0,3
                  pBSMHardFrame(jj) = bs4Bag(jj,1)
            enddo
            call dboost_Sgn(pSys,hardQ,pBSMHardFrame,-1d0) 
            cosThetaQkBoost(2) = getCosTheta(pSys,pBSMHardFrame)
            call dboost_Sgn(pBSMHardFrame,getMfromP4(bs4Bag(0,1)),pBSMHardFrame,-1d0)            
c get polar angle of k++ w.r.t. Q=(had. recoil) in lab frame
            cosThetaQk(1)      = getCosTheta(q,bs3Bag(0,1))
            cosThetaQk(2)      = getCosTheta(q,bs4Bag(0,1))
c get azimuth angle of k++ w.r.t. had. in lab frame            
            dphiKX(1) = DELTA_PHI(pHad,bs3Bag(0,1))
            dphiKX(2) = DELTA_PHI(pHad,bs4Bag(0,1))
c standard (pT,rap) of k++
            pTBSM =  getPTfromP4(bs3Bag(0,1))
            rapBSM = getRapFromP4(bs3Bag(0,1))
      else
            write(*,*)'should not be here'
c            write(*,*)'nrMups,nrMCMups = ',nrMups,nrMCMups
            write(*,*)'nrMCBSMs(i) = ',nrMCBSMs(1),nrMCBSMs(2),nrMCBSMs(3),nrMCBSMs(4)
            write(*,*)'nrBSMs(i) = ',nrBSMs(1),nrBSMs(2),nrBSMs(3),nrBSMs(4)
            stop -3861 ! skip event if there are no k++ or mu+
            goto 999 
      endif
c***************************************************************c
c     jet clustering:                           c
c***************************************************************c
c     call fastjetppgenkt(pp,nn,rfj,sycut,palg,pjet,njet,jet)
c     arguments:
c     pp/pHadFJ = array of hadron 4-momenta
c     nn/nrHads = number of hadrons
c     rfj = jet radius for kT-style clustering
c     sycut/minPTjDef = pT scale above which jets are defined; inclusive below
c     yjmax/maxRapjDef = y scale within which jets are defined; inclusive outside
c     palg = algorithm choice of kT-style clustering
c     pjet/pJetFJ = array of jet 4-momenta, ordered by pTj
c     njet/nrJets = number of clustered jets
c     whichJet(i) = which jet in pJetFJ array that contains parton (i) 
      palg(1) = 1.0d0           ! kT
      palg(2) = 1.0d0           ! kT
      rfj(1)  =  0.4d0          ! jet size R=0.4
      rfj(2)  =  1.0d0          ! jet size R=1.0
      minPTjDef(1)  =  1d0
      minPTjDef(2)  =  1d0
      maxRapjDef(1) =  etaMaxHCAL !5d0
      maxRapjDef(2) =  etaMaxHCAL !5d0
      nrJetsKT0p4 = 0
c     pjet      
c     call fastjetppgenkt(pp,nn,rfj,sycut,palg,pjet,njet,jet)
c     call fastjetppgenkt(pHadFJ,nrHads,rfj,minPTjDef,palg,pJetFJ,nrJets,whichJet)
c***************************************************************c
c***************************************************************c
      call fastjetppgenkt(pHadFJ,nrHads,rfj(1),minPTjDef(1),palg(1),pJetFJKT0p4,nrJetsKT0p4,whichJet)
c***************************************************************c
c***************************************************************c
      allocate( jetBagKT0p4(0:3,nrJetsKT0p4) )
c***************************************************************c
c***************************************************************c
c     transcribe pJetFJ momenta p(1:4) to HELAS convention p(0:3) 
c     and build / collect observables
c***************************************************************c
c     ak-0.4 clusters
      if(nrJetsKT0p4.gt.0) then
         do ii = 1,nrJetsKT0p4
c get pT of jet(ii)            
            call getP4HelasfromP4FJ(jetBagKT0p4(0,ii),pJetFJKT0p4(1,ii))
            tmpPT = pt(jetBagKT0p4(0,ii))
c increment  HT_KT0p4
            HTincl_KT0p4 = HTincl_KT0p4 + tmpPT
c increment counter            
            if(tmpPT.gt.pTjMin) then
               nrKT0p4PT25 = nrKT0p4PT25 + 1
            endif
c update leading pTj1            
            if(tmpPT.gt.pTj1) pTj1 = tmpPT
         enddo
         pTj1KT0p4 = pt(jetBagKT0p4(0,1))
      endif                                 
c***************************************************************c
c***************************************************************c
c***************************************************************c
c     Misc. observables and cuts
c***************************************************************c
c ...
c***************************************************************c
c***************************************************************c
c     sanity:
c***************************************************************c
c ...
c***************************************************************c
c***************************************************************c
c     fill histos
c***************************************************************c
c***************************************************************c
 777  do i=1,1
         l=0+(i-1)*1000
c     global books: 1-100
         call HwU_fill(l+1,var,WWW)
c***************************************************************c
c***************************************************************c
c     local books: 101-200
         call HwU_fill(l+101,pTSys, www)
         call HwU_fill(l+111,hardQ, www)
         if(nrBSMs(3).gt.0) call HwU_fill(l+121,pBSMHardFrame(0)-getMfromP4(bs4Bag(0,1)),www)
c     lepton books: 201-300
c     hadron books: 301-400
         call HwU_fill(l+301,dble(nrJetsKT0p4), WWW)
         call HwU_fill(l+302,dble(nrKT0p4PT25), WWW)
         call HwU_fill(l+311,HTincl_Had,  WWW)
         call HwU_fill(l+312,pTHad,       WWW)
         call HwU_fill(l+321,pTj1,        WWW)
c     lepton-hadron books: 401-500
c     BSM: 501-600
         if(nrBSMs(1).gt.0) call HwU_fill(l+501,getMfromP4(bs1Bag(0,1)),www)
         if(nrBSMs(3).gt.0) call HwU_fill(l+503,getMfromP4(bs3Bag(0,1)),www)
         if(nrBSMs(3).gt.0) call HwU_fill(l+505,pTBSM,www)
         if(nrBSMs(3).gt.0) call HwU_fill(l+506,rapBSM,www)
         if(cosThetaQk(1).gt.-10d0)      call HwU_fill(l+511,cosThetaQk(1),  WWW)
         if(cosThetaQk(2).gt.-10d0)      call HwU_fill(l+531,cosThetaQk(2),  WWW)
         if(cosThetaQkBoost(1).gt.-10d0) call HwU_fill(l+512,cosThetaQkBoost(1),  WWW)
         if(cosThetaQkBoost(2).gt.-10d0) call HwU_fill(l+532,cosThetaQkBoost(2),  WWW)
         if(dphiKX(1).gt.-10d0)          call HwU_fill(l+521,dphiKX(1),  WWW)        
         if(dphiKX(2).gt.-10d0)          call HwU_fill(l+541,dphiKX(2),  WWW)
c     Misc: 601-700
ccc   *************************************************************** c
c Note:       integer,parameter,dimension(1:4) ::
c     &     bsmPID = (/37,-37,38,-38,61,-61/) ! see bsmPID at ~L195            
ccc   *************************************************************** c
ccc   *************************************************************** c
ccc   *************************************************************** c
c     end of books
      enddo
      call HwU_add_points       ! "add points" 
c***************************************************************c
c***************************************************************c
c     end histos
c***************************************************************c
c***************************************************************c
c***************************************************************c
c     memory savior
c***************************************************************c
 778  deallocate( whichJet    )
      deallocate( pHadFJ )
      deallocate( pJetFJKT0p4 )

      deallocate( mcGamBag )
      deallocate( mcLepBag )
      deallocate( mcEleBag )
      deallocate( mcPosBag )
      deallocate( mcMumBag )
      deallocate( mcMupBag )
      deallocate( mcTamBag )
      deallocate( mcTapBag )
      deallocate( mcBS1Bag )
      deallocate( mcBS2Bag )
      deallocate( mcBS3Bag )
      deallocate( mcBS4Bag )

      deallocate( GamBag )
      deallocate( LepBag )
      deallocate( EleBag )
      deallocate( PosBag )
      deallocate( MumBag )
      deallocate( MupBag )
      deallocate( TamBag )
      deallocate( TapBag )
      deallocate( bs1Bag )
      deallocate( bs2Bag )
      deallocate( bs3Bag )
      deallocate( bs4Bag )

      deallocate( jetBagKT0p4 )
c***************************************************************c
c***************************************************************c
 999  END
c***************************************************************c
c***************************************************************c
      subroutine end_of_file()
      implicit none
      end
c***************************************************************c
c***************************************************************c
