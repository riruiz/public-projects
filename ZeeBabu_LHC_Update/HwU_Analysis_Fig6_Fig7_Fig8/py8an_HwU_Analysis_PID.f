c ************************************************* c
c py8an_HwU_Analysis_PID.f
c Particle identifications routines for HwU Analysis
c R. Ruiz
c March 2019
c ************************************************* c

c ************************************************* c
c pass kinematic and fiducial acceptance?
      logical function pid_IsFidKinCand(kDoCuts,kP4,kRapMax,kPTMin)
      implicit none
      logical kDoCuts
      double precision kP4(0:3),kPTMin,kRapMax
c     
      logical tmpFlag
      double precision pid_getPT2,pid_getRap
      external pid_getPT2, pid_getRap
c
      if(.not.kDoCuts) then
            pid_IsFidKinCand = .true.
            return
      endif

      tmpFlag = .true.
      if(pid_getPT2(kP4).lt.kPTMin**2) then
         tmpFlag = .false.
         pid_IsFidKinCand = tmpFlag
         return
      elseif(abs(pid_getRap(kP4)).gt.kRapMax) then
         tmpFlag = .false.
         pid_IsFidKinCand = tmpFlag
         return
      else
         pid_IsFidKinCand = tmpFlag
         return
      endif
      end
c ************************************************* c
c get pT^2 from p4 : (E,px,py,pz)
      double precision function pid_getPT2(kP4)
      implicit none
      double precision kP4(0:3)
c
      double precision tmpPT2
      tmpPT2 = kp4(1)**2 + kp4(2)**2
      if(tmpPT2.lt.0d0) tmpPT2 = 0d0
      pid_getPT2 = tmpPT2
      return
      end
c ************************************************* c
c get pT from p4 : (E,px,py,pz)
      double precision function pid_getPT(kP4)
      implicit none
      double precision kP4(0:3)
c
      double precision tmpPT2
      tmpPT2 = kp4(1)**2 + kp4(2)**2
      if(tmpPT2.lt.0d0) tmpPT2 = 0d0
      pid_getPT = dsqrt(tmpPT2)
      return
      end
c ************************************************* c
c get rapidity from p4 : (E,px,py,pz)
      double precision function pid_getRap(kP4)
      implicit none
      double precision kP4(0:3)
c
      real*8 en,pl,tiny,xplus,xminus,y
      parameter (tiny=1.d-8)
      en = kP4(0)
      pl = kP4(3)
      xplus=en+pl
      xminus=en-pl
      if(xplus.gt.tiny.and.xminus.gt.tiny)then
         if( (xplus/xminus).gt.tiny.and.(xminus/xplus).gt.tiny)then
            y=0.5d0*log( xplus/xminus  )
         else
            y=sign(1.d0,pl)*1.d8
         endif
      else
         y=sign(1.d0,pl)*1.d8
      endif
      pid_getRap=y
      return
      end
c ************************************************* c
