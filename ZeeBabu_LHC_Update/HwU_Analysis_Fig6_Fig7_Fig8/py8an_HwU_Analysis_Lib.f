c ************************************************* c
c py8an_HwU_Analysis_Lib.f
c Basic routines and kinematic functions for
c HwU Analysis
c R. Ruiz
c March 2019
c ************************************************* c
c ************************************************* c
c ************************************************* c
      function getrapidity(en,pl)
      implicit none
      real*8 getrapidity,en,pl,tiny,xplus,xminus,y
      parameter (tiny=1.d-8)
      xplus=en+pl
      xminus=en-pl
      if(xplus.gt.tiny.and.xminus.gt.tiny)then
         if( (xplus/xminus).gt.tiny.and.(xminus/xplus).gt.tiny)then
            y=0.5d0*log( xplus/xminus  )
         else
            y=sign(1.d0,pl)*1.d8
         endif
      else 
         y=sign(1.d0,pl)*1.d8
      endif
      getrapidity=y
      return
      end
c ************************************************* c
c compute square of invariant mass of 4-vector
c ************************************************* c
      double precision function getMfromP4(kP4)
      implicit none
      double precision kP4(0:3)
      double precision getM2fromP4
      external getM2fromP4

      double precision sum
      sum = getM2fromP4(kP4)
      if(sum.gt.0d0) then
            sum = dsqrt(sum)
      else
            sum = dsqrt(-sum)
      endif  

      getMfromP4 = sum
      return
      end
c ************************************************* c
c ************************************************* c
c compute square of invariant mass of 4-vector
c ************************************************* c
      double precision function getM2fromP4(kP4)
      implicit none
      double precision kP4(0:3)

      integer ii
      double precision sum2
      sum2 = kP4(0)*kP4(0)
      do ii=1,3
            sum2 = sum2 - kP4(ii)*kP4(ii)
      enddo
      getM2fromP4 = sum2
      return
      end
c ************************************************* c
c ************************************************* c
c compute square of pT of 4-vector
c ************************************************* c
      double precision function getPT2fromP4(kP4)
      implicit none
      double precision kP4(0:3) ! = (E,px,py,pz)

      double precision tmpPT2
      tmpPT2 = kP4(1)**2 + kP4(2)**2
      getPT2fromP4 = tmpPT2
      return
      end
c ************************************************* c
c ************************************************* c
c compute pT of 4-vector
c ************************************************* c
      double precision function getPTfromP4(kP4)
      implicit none
      double precision kP4(0:3) ! = (E,px,py,pz)
      double precision getPT2fromP4
      external getPT2fromP4

      double precision tmpPT
      tmpPT = getPT2fromP4(kP4)
      if(tmpPT.lt.0d0) then
           tmpPT = -999d0
      else
           tmpPT = dsqrt(tmpPT)
      endif

      getPTfromP4 = tmpPT
      return
      end
c ************************************************* c
c ************************************************* c
c compute rapidity from 4-vector
c identical to getrapidity for compatibility
c ************************************************* c
      double precision function getRapFromP4(kP4)
      implicit none
      double precision kP4(0:3)
c
      real*8 en,pl,tiny,xplus,xminus,y
      parameter (tiny=1.d-8)
      en = kP4(0)
      pl = kP4(3)
      xplus=en+pl
      xminus=en-pl
      if(xplus.gt.tiny.and.xminus.gt.tiny)then
         if( (xplus/xminus).gt.tiny.and.(xminus/xplus).gt.tiny)then
            y=0.5d0*log( xplus/xminus  )
         else
            y=sign(1.d0,pl)*1.d8
         endif
      else
         y=sign(1.d0,pl)*1.d8
      endif
      getRapFromP4=y
      return
      end
c ************************************************* c
c ************************************************* c
c compute rapidity from 5-vector
c identical to getrapidity for compatibility
      double precision function getRapFromP5(kP5)
      implicit none
      double precision kP5(5)
c
      real*8 en,pl,tiny,xplus,xminus,y
      parameter (tiny=1.d-8)
      en = kP5(4)
      pl = kP5(3)
      xplus=en+pl
      xminus=en-pl
      if(xplus.gt.tiny.and.xminus.gt.tiny)then
         if( (xplus/xminus).gt.tiny.and.(xminus/xplus).gt.tiny)then
            y=0.5d0*log( xplus/xminus  )
         else
            y=sign(1.d0,pl)*1.d8
         endif
      else
         y=sign(1.d0,pl)*1.d8
      endif
      getRapFromP5=y
      return
      end
c ************************************************* c
c ************************************************* c
c compute pT^2 from 5-vector
      double precision function getPT2fromP5(kP5)
      implicit none
      double precision kP5(5) ! p5 = (px,py,pz,E,m)
c
      double precision tmpPT2
      tmpPT2 = kp5(1)**2 + kp5(2)**2
      if(tmpPT2.lt.0d0) tmpPT2 = 0d0
      getPT2fromP5 = tmpPT2
      return
      end
c ************************************************* c
c ************************************************* c
c compute pT from 5-vector
      double precision function getPTfromP5(kP5)
      implicit none
      double precision kP5(5) ! p5 = (px,py,pz,E,m)
c
      double precision tmpPT2
      tmpPT2 = kp5(1)**2 + kp5(2)**2
      if(tmpPT2.lt.0d0) tmpPT2 = 0d0
      getPTfromP5 = dsqrt(tmpPT2)
      return
      end
c ************************************************* c
c ************************************************* c
c out: 4-vector, p4 = (E,px,py,pz) from
c  in: 4-vector, p4 = (px,py,pz,E) 
      subroutine getP4HelasfromP4FJ(kP4HEL,kP4FJ)
      implicit none
      double precision kP4HEL(0:3),kP4FJ(4)
c
      integer jj
      kP4HEL(0) = kP4FJ(4)
      do jj=1,3
         kP4HEL(jj) = kP4FJ(jj)
      enddo
      end
c ************************************************* c
c ************************************************* c
c out: 4-vector, p4 = (E,px,py,pz) from
c  in: 5-vector, p5 = (px,py,pz,E,m) 
      subroutine getP4fromP5_Helas(kP4,kP5)
      implicit none
      double precision kP4(0:3),kP5(5)
c
      integer jj
      do jj=1,3
         kP4(jj) = kP5(jj)
      enddo
      kP4(0) = kP5(4)
      end
c ************************************************* c
c ************************************************* c
c out: 4-vector, p4 = (px,py,pz,E) from
c  in: 5-vector, p5 = (px,py,pz,E,m) 
      subroutine getP4fromP5_FJ(kP4,kP5)
      implicit none
      double precision kP4(4),kP5(5)
c
      integer jj
      do jj=1,4
         kP4(jj) = kP5(jj)
      enddo
      end
c ************************************************* c
c ************************************************* c
c out: 4-vector, p4 = (E,px,py,pz) from
c  in: 5-vector, p5 = (px,py,pz,E,m)
      subroutine addP4fromP5_Helas(kP4,kP5)
      implicit none
      double precision kP4(0:3),kP5(5)
c
      integer jj
      kP4(0) = kP4(0) + kP5(4)
      do jj=1,3
         kP4(jj) = kP4(jj) + kP5(jj)
      enddo
      end
c ************************************************* c  
c ************************************************* c
c out: 4-vector, p4 = (E,px,py,pz) from
c  in: 5-vector, p5 = (px,py,pz,E,m)
      subroutine addP4fromP5_Helas_Sgn(kP4,kP5,kSgn)
      implicit none
      double precision kP4(0:3),kP5(5)
      integer kSgn
c
      integer jj
      kP4(0) = kP4(0) + kSgn*kP5(4)
      do jj=1,3
         kP4(jj) = kP4(jj) + kSgn*kP5(jj)
      enddo
      end
c ************************************************* c  
c ************************************************* c
c out: 4-vector, p4 = (E,px,py,pz) from p4
      subroutine addP4fromP4_Helas(kP4O,kP4I)
      implicit none
      double precision kP4O(0:3),kP4I(0:3)
c
      integer jj
      do jj=0,3
         kP4O(jj) = kP4O(jj) + kP4I(jj)
      enddo
      end
c ************************************************* c  
c ************************************************* c
c out: 4-vector, p4 = (E,px,py,pz) from p4
      subroutine addP4fromP4_Helas_Sgn(kP4O,kP4I,kSgn)
      implicit none
      double precision kP4O(0:3),kP4I(0:3)
      integer kSgn
c
      integer jj
      do jj=0,3
         kP4O(jj) = kP4O(jj) + kSgn*kP4I(jj)
      enddo
      end
c ************************************************* c  
c ************************************************* c
c out: 4-vector, p4 = (0,0,0,0)
c in:  4-vector, p4 = (E,px,py,pz)
      subroutine resetP4(kP4)
      implicit none
      double precision kP4(0:3)
c
      integer jj
      do jj=0,3
         kP4(jj) = 0d0
      enddo
      end
c ************************************************* c
c ************************************************* c
c Based on "sort lepton pts" in Template/LO/SubProcesses/cuts.f
c out: array of 4-vector(E,px,py,pz) ordered by pT (1 = hardest; kNrP4 = softest)
c in:  array of 4-vector(E,px,py,pz)
      subroutine sortP4ByPT(kP4Bag,kNrP4)
      implicit none
      integer kNrP4
      double precision kP4Bag(0:3,*)
c
      integer ii,jj,kk
      double precision tmpP4(0:3),tmpPT2ii,tmpPT2jj
c
      do ii=1,kNrP4-1
         do jj=ii+1,kNrP4
            tmpPT2ii = kP4Bag(1,ii)**2 + kP4Bag(2,ii)**2
            tmpPT2jj = kP4Bag(1,jj)**2 + kP4Bag(2,jj)**2
            if(tmpPT2jj>tmpPT2ii) then
c              swap positions
               do kk=0,3
                  tmpP4(kk)     = kP4Bag(kk,ii)
                  kP4Bag(kk,ii) = kP4Bag(kk,jj)
                  kP4Bag(kk,jj) = tmpP4(kk)
               enddo
            endif
         enddo
      enddo
      end      
cc     - sort lepton pts
c         do i=1,nleptons-1
c            do j=i+1,nleptons
c               if(ptlepton(j).gt.ptlepton(i)) then
c                  temp=ptlepton(i)
c                 ptlepton(i)=ptlepton(j)
c                  ptlepton(j)=temp
c               endif
c            enddo
c         enddo
c ************************************************* c
c ************************************************* c
c Based on "sort lepton pts" in Template/LO/SubProcesses/cuts.f
c out: array of 5-vector(px,py,pz,E,m) ordered by pT (1 = hardest; kNrP5 = softest)
c in:  array of 5-vector(px,py,pz,E,m)
      subroutine sortP5ByPT(kP5Bag,kNrP5)
      implicit none
      integer kNrP5
      double precision kP5Bag(5,*)
c
      integer ii,jj,kk
      double precision tmpP5(5),tmpPT2ii,tmpPT2jj
c
      do ii=1,kNrP5-1
         do jj=ii+1,kNrP5
            tmpPT2ii = kP5Bag(1,ii)**2 + kP5Bag(2,ii)**2
            tmpPT2jj = kP5Bag(1,jj)**2 + kP5Bag(2,jj)**2
            if(tmpPT2jj>tmpPT2ii) then
c              swap positions
               do kk=1,5
                  tmpP5(kk)     = kP5Bag(kk,ii)
                  kP5Bag(kk,ii) = kP5Bag(kk,jj)
                  kP5Bag(kk,jj) = tmpP5(kk)
               enddo
            endif
         enddo
      enddo
      end
c ************************************************* c
c ************************************************* c
c ************************************************* c
c in:  4-vector, p4 = (E,px,py,pz)
      double precision function getCosTheta(kP4A,kP4B)
      implicit none
      double precision kP4A(0:3),kP4B(0:3)
c
      double precision magPA,magPB,tiny
      parameter (tiny=1.d-8)
c compute polar angle between pA and pB
c
      magPA = kP4A(1)**2 + kP4A(2)**2 + kP4A(3)**2
      magPB = kP4B(1)**2 + kP4B(2)**2 + kP4B(3)**2
      if(magPA.lt.tiny) then 
         magPA = tiny
      else
         magPA = sqrt(magPA)
      endif
      if(magPB.lt.tiny) then
         magPB = tiny
      else
         magPB = sqrt(magPB)
      endif
      getCosTheta = kP4A(1)*kP4B(1) + kP4A(2)*kP4B(2) + kP4A(3)*kP4B(3)
      getCosTheta = getCosTheta/(magPA*magPB)
      return
      end
c ************************************************* c
c ************************************************* c
c in:  4-vector, kQ4 = (px,py,pz,E)
c                  P = (px,py,pz,E)
        SUBROUTINE DBOOST(P,MP,kQ4)
        implicit none
        double precision P(4),kQ4(4),MP
C ***
C       THIS SUBROUTINE CALCULATES BOOSTED 4-MOMENTA IN DOUBLE PRECISION
C       Q TO FRAME OF PARTICLE P
C       EQ=Q(4)
C       PQ=Q(I) ; I=1,3
C       MP = MASS OF PARTICLE P            -H.BAER-
C ***
        integer ii
        double precision G,VQ,HQ
        G=P(4)/MP
        VQ=(kQ4(1)*P(1)+kQ4(2)*P(2)+kQ4(3)*P(3))/P(4)
        HQ=VQ*G*G/(1.0D0+G)/P(4)+kQ4(4)/MP
        kQ4(4)=G*(kQ4(4)+VQ)
        DO ii=1,3
                kQ4(ii)=kQ4(ii)+HQ*P(ii)
        END DO
        RETURN
        END
c ************************************************* c
c ************************************************* c
c in:  4-vector, kP4 = (E,px,py,pz)
c                kQ4 = (E,px,py,pz)
c Usage example:
c     call dboost_Sgn(pDiTau,mDiTau,pTam,-1d0) ! boost ta- to diTau frame
c     call dboost_Sgn(pDiTau,mDiTau,pTap,-1d0) ! boost ta+ to diTau frame
        SUBROUTINE DBOOST_Sgn(kP4,kMP,kQ4,kSgn)
        implicit none
        double precision kP4(0:3),kQ4(0:3),kMP,kSgn
C ***
C       kSgn=+1: THIS SUBROUTINE CALCULATES BOOSTED 4-MOMENTA 
C                (IN DOUBLE PRECISION) Q TO FRAME OF PARTICLE P
C       kSgn=-1: BOOSTS P TO REST-FRAME OF Q -R. Ruiz-
c
C       EQ=Q(0)
C       PQ=Q(I) ; I=1,3
C       kMP = MASS OF PARTICLE P
C ***
        integer ii
        double precision G,VQ,HQ
        G=kP4(0)/kMP
        VQ=(kQ4(1)*kP4(1)+kQ4(2)*kP4(2)+kQ4(3)*kP4(3))/kP4(0) ! beta*gamma = |p|/m
        HQ=VQ*G*G/(1.0D0+G)/kP4(0)+kSgn*kQ4(0)/kMP
        kQ4(0)=G*(kQ4(0)+kSgn*VQ)
        DO ii=1,3
                kQ4(ii)=kQ4(ii)+HQ*kP4(ii)
        END DO
        RETURN
        END
c ************************************************* c
