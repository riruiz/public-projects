# R. Ruiz
# 2022 June

These are the files needed to generate the kinematic distributions at (N)LO+LL(PS):

- readme.txt
This file

- py8an_HwU_pp_kppkmm_ZeeBabu_public.f
Main analysis code called by shower_card.dat and should be placed in the directory:
/path_to_working_directory/MCatNLO/PY8Analyzer/

For reference, the events and cards directories are:
/path_to_working_directory/Events/
/path_to_working_directory/Cards/

- py8an_HwU_Analysis_Lib.f
Analysis library for py8an_HwU_pp_kppkmm_ZeeBabu_public.f location should match path in shower_card.dat

- py8an_HwU_Analysis_PID.f
Analysis library for py8an_HwU_pp_kppkmm_ZeeBabu_public.f location should match path in shower_card.dat

- shower_card.dat
Shower card with library declarations (bottom). should be placed in /path_to_working_directory/Cards/

- scripts for running mg5amc
./bin/mg5_aMC run_mg5amc_TypeII_DYX_DppDmm_HwU_MXScan_LHCX13_public.dat
./bin/mg5_aMC run_mg5amc_ZeeBabu_DYX_kppkmm_HwU_MXScan_LHCX13_public.dat
