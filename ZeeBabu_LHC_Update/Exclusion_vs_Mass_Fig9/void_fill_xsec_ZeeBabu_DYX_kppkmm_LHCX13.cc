// today is: 2022 6 12
// ////////////////
//  DYX_kppkmm   //
// ////////////////

void fillZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen( double kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[0] = 0.002256;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[1] = 0.0008228;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[2] = 0.0003397;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[3] = 0.0001524;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[4] = 7.264e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[5] = 3.62e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[6] = 1.866e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[7] = 9.867e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[8] = 5.335e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[9] = 2.929e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max( double kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[0] = 0.0023033759999999998;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[1] = 0.0008417244;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[2] = 0.0003478528;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[3] = 0.0001563624;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[4] = 7.467391999999999e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[5] = 3.72498e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[6] = 1.923846e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[7] = 1.0192611e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[8] = 5.521724999999999e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[9] = 3.034444e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min( double kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[0] = 0.002208624;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[1] = 0.0008030528;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[2] = 0.0003305281;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[3] = 0.000147828;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[4] = 7.031551999999999e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[5] = 3.4933e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[6] = 1.796958e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[7] = 9.47232e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[8] = 5.110929999999999e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[9] = 2.800124e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XSec_NLO_LHCX13_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen( double kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[0] = 0.002256;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[1] = 0.0008228;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[2] = 0.0003397;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[3] = 0.0001524;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[4] = 7.264e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[5] = 3.62e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[6] = 1.866e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[7] = 9.867e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[8] = 5.335e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[9] = 2.929e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max( double kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[0] = 0.0023417280000000004;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[1] = 0.0008565348;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[2] = 0.0003546468;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[3] = 0.00015956279999999998;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[4] = 7.634463999999999e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[5] = 3.8190999999999994e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[6] = 1.9779600000000003e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[7] = 1.0518222e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[8] = 5.713784999999999e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[9] = 3.157462e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min( double kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[0] = 0.0021792960000000003;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[1] = 0.0007923564000000001;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[2] = 0.00032645170000000003;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[3] = 0.0001459992;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[4] = 6.944384e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[5] = 3.4498599999999995e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[6] = 1.7727e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[7] = 9.344049e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[8] = 5.0362399999999996e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[9] = 2.7532599999999997e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XPDF_NLO_LHCX13_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen( double kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[0] = 0.001944;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[1] = 0.0007085;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[2] = 0.0002906;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[3] = 0.0001296;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[4] = 6.141e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[5] = 3.039e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[6] = 1.562e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[7] = 8.208e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[8] = 4.399e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[9] = 2.401e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max( double kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[0] = 0.002058696;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[1] = 0.0007595120000000001;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[2] = 0.0003147198;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[3] = 0.0001416528;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[4] = 6.767382e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[5] = 3.3732900000000004e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[6] = 1.7447539999999998e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[7] = 9.225792e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[8] = 4.970869999999999e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[9] = 2.729937e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min( double kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[0] = 0.0018390239999999999;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[1] = 0.0006624475000000001;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[2] = 0.00026938620000000005;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[3] = 0.00011910240000000001;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[4] = 5.606733e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[5] = 2.756373e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[6] = 1.407362e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[7] = 7.354368e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[8] = 3.919509e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[9] = 2.129687e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XSec_XLO_LHCX13_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen( double kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[0] = 0.001944;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[1] = 0.0007085;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[2] = 0.0002906;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[3] = 0.0001296;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[4] = 6.141e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[5] = 3.039e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[6] = 1.562e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[7] = 8.208e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[8] = 4.399e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[9] = 2.401e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max( double kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[0] = 0.0020159279999999997;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[1] = 0.0007368400000000001;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[2] = 0.0003033864;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[3] = 0.0001356912;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[4] = 6.454190999999999e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[5] = 3.2061449999999994e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[6] = 1.65572e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[7] = 8.749728e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[8] = 4.715728e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[9] = 2.588278e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min( double kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[] ){
	// take care to avoid seg fault here due to size of array
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[0] = 0.001877904;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[1] = 0.000682994;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[2] = 0.0002792666;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[3] = 0.0001242864;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[4] = 5.870795999999999e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[5] = 2.896167e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[6] = 1.4838999999999998e-05;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[7] = 7.772976e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[8] = 4.152656e-06;	 // [pb]
	kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[9] = 2.2569399999999998e-06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kZeeBabu_DYX_kppkmm_XPDF_XLO_LHCX13_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
