# R. Ruiz
# 2022 July 1

# These files were used to make some of the figures and tables in the paper
# Doubly Charged Higgs Boson Production at Hadron Colliders II: A Zee-Babu Case Study
# https://arxiv.org/abs/2206.14833

# UFO files are available from the FeynRules database:
# https://feynrules.irmp.ucl.ac.be/wiki/ZeeBabu

# Feel free to write if there are questions!
