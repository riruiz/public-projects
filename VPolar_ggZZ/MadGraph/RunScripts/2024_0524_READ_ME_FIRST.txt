# R. Ruiz (IFJ PAN)
# 2024 May 24

The run scripts in this folder correspond to the following polarization configurations. 

## unpolarized case
## ZPolar_gg_eemm_ZZ_QED4_QCD2_XLO

## includes Z0 + ZA + ZT interference (recovers unpolarized case)
## ZPolar_gg_eemm_XX_QED4_QCD2_XLO

## TT
## ZPolar_gg_eemm_TT_QED4_QCD2_XLO

## AA
## ZPolar_gg_eemm_AA_QED4_QCD2_XLO

## 00
## ZPolar_gg_eemm_00_QED4_QCD2_XLO

## 0T + T0
## ZPolar_gg_eemm_0T_QED4_QCD2_XLO_LoopFilter
