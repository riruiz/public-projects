// today is: 2024 2 13
// //////////////////////////
//  ggZZpolar_gg_eemm_XX   //
// //////////////////////////

void fillggZZpolar_gg_eemm_XX_XSec_XLO_Cen( double kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[0] = 3.38761e-07;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[1] = 9.56049e-06;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[2] = 3.92609e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[3] = 9.12462e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[4] = 0.000164614;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[5] = 0.000362194;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[6] = 0.000480014;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[7] = 0.000751652;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[8] = 0.00122446;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[9] = 0.00140051;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[10] = 0.001947;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[11] = 0.00252914;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[12] = 0.00358987;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[13] = 0.00424025;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[14] = 0.00581784;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[15] = 0.00702779;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[16] = 0.00827695;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[17] = 0.00949881;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[18] = 0.0107109;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[19] = 0.0131797;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[20] = 0.0158033;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[21] = 0.0183521;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[22] = 0.0208384;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[23] = 0.0221895;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[24] = 0.0247503;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_XX_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_XX_XSec_XLO_Max( double kggZZpolar_gg_eemm_XX_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[0] = 5.762324610000001e-07;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[1] = 1.4598868230000002e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[2] = 5.7046087699999995e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[3] = 0.00012829215720000002;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[4] = 0.00022601502199999998;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[5] = 0.00048063143799999996;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[6] = 0.00062881834;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[7] = 0.0009636178640000001;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[8] = 0.001530575;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[9] = 0.0017408339299999998;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[10] = 0.002425962;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[11] = 0.0031892455400000003;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[12] = 0.004598623470000001;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[13] = 0.00547416275;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[14] = 0.00762718824;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[15] = 0.00929073838;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[16] = 0.01103317435;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[17] = 0.01274740302;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[18] = 0.014459715000000001;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[19] = 0.017977110799999998;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[20] = 0.021761144099999997;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[21] = 0.025472714799999997;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[22] = 0.0291112448;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[23] = 0.031087489500000003;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Max[24] = 0.034897923;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_XX_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_XX_XSec_XLO_Min( double kggZZpolar_gg_eemm_XX_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[0] = 2.1037058100000001e-07;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[1] = 6.5202541799999994e-06;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[2] = 2.7914499900000004e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[3] = 6.670097220000001e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[4] = 0.000122802044;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[5] = 0.000278527186;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[6] = 0.00037297087800000004;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[7] = 0.000594556732;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[8] = 0.00098936368;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[9] = 0.00113861463;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[10] = 0.0015751229999999999;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[11] = 0.00201825372;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[12] = 0.0028108682100000002;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[13] = 0.003290434;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[14] = 0.00443319408;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[15] = 0.00530598145;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[16] = 0.0061911586;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[17] = 0.0070481170199999995;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[18] = 0.0078832224;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[19] = 0.009594821600000001;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[20] = 0.011378375999999999;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[21] = 0.013085047300000001;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[22] = 0.0147535872;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[23] = 0.015643597500000002;	 // [pb]
	kggZZpolar_gg_eemm_XX_XSec_XLO_Min[24] = 0.017325209999999997;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_XX_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_XX_XPDF_XLO_Cen( double kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[0] = 3.38761e-07;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[1] = 9.56049e-06;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[2] = 3.92609e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[3] = 9.12462e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[4] = 0.000164614;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[5] = 0.000362194;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[6] = 0.000480014;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[7] = 0.000751652;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[8] = 0.00122446;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[9] = 0.00140051;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[10] = 0.001947;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[11] = 0.00252914;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[12] = 0.00358987;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[13] = 0.00424025;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[14] = 0.00581784;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[15] = 0.00702779;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[16] = 0.00827695;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[17] = 0.00949881;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[18] = 0.0107109;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[19] = 0.0131797;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[20] = 0.0158033;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[21] = 0.0183521;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[22] = 0.0208384;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[23] = 0.0221895;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[24] = 0.0247503;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_XX_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_XX_XPDF_XLO_Max( double kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[0] = 3.648117209e-07;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[1] = 9.833920014e-06;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[2] = 3.998722665e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[3] = 9.25236468e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[4] = 0.0001666058294;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[5] = 0.0003662505728;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[6] = 0.00048553416100000004;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[7] = 0.000760671824;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[8] = 0.0012402555339999998;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[9] = 0.0014189967320000002;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[10] = 0.0019736739;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[11] = 0.002565306702;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[12] = 0.003641923115;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[13] = 0.004301733625;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[14] = 0.00590219868;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[15] = 0.007128287396999999;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[16] = 0.00839448269;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[17] = 0.009630843459;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[18] = 0.01085871042;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[19] = 0.013356307980000003;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[20] = 0.016008742899999998;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[21] = 0.018585171669999997;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[22] = 0.02109679616;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[23] = 0.022460211900000002;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[24] = 0.0250473036;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_XX_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_XX_XPDF_XLO_Min( double kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[0] = 3.1271027910000003e-07;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[1] = 9.287059986e-06;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[2] = 3.8534573350000006e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[3] = 8.99687532e-05;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[4] = 0.0001626221706;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[5] = 0.0003581374272;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[6] = 0.00047449383900000005;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[7] = 0.000742632176;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[8] = 0.0012086644659999998;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[9] = 0.001382023268;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[10] = 0.0019203260999999998;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[11] = 0.002492973298;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[12] = 0.0035378168850000004;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[13] = 0.004178766375;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[14] = 0.005733481320000001;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[15] = 0.0069272926030000005;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[16] = 0.00815941731;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[17] = 0.009366776541;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[18] = 0.01056308958;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[19] = 0.01300309202;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[20] = 0.0155978571;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[21] = 0.01811902833;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[22] = 0.02058000384;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[23] = 0.0219187881;	 // [pb]
	kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[24] = 0.024453296399999998;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_XX_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
