// today is: 2024 2 13
// //////////////////////////
//  ggZZpolar_gg_eemm_00   //
// //////////////////////////

void fillggZZpolar_gg_eemm_00_XSec_XLO_Cen( double kggZZpolar_gg_eemm_00_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[0] = 7.19358e-09;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[1] = 2.06631e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[2] = 8.84986e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[3] = 2.14419e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[4] = 3.98514e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[5] = 9.19532e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[6] = 1.25043e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[7] = 2.0561e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[8] = 3.50725e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[9] = 4.0451e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[10] = 5.82065e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[11] = 7.80639e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[12] = 0.000113735;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[13] = 0.000137076;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[14] = 0.000193969;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[15] = 0.000239383;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[16] = 0.00028506;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[17] = 0.000330157;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[18] = 0.000378857;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[19] = 0.000479878;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[20] = 0.00057553;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[21] = 0.000688167;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[22] = 0.000788931;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[23] = 0.000839386;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Cen[24] = 0.000945453;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_00_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_00_XSec_XLO_Max( double kggZZpolar_gg_eemm_00_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[0] = 1.2128375879999999e-08;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[1] = 3.14285751e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[2] = 1.284114686e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[3] = 3.0190195199999995e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[4] = 5.48753778e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[5] = 1.226655688e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[6] = 1.64806674e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[7] = 2.6564812e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[8] = 4.42965675e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[9] = 5.0806456e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[10] = 7.1943234e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[11] = 9.60966609e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[12] = 0.00014148634;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[13] = 0.00017148207599999998;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[14] = 0.000245952692;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[15] = 0.00030569209099999995;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[16] = 0.00036658716000000004;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[17] = 0.000427223158;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[18] = 0.0004932718140000001;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[19] = 0.0006295999360000001;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[20] = 0.00076200172;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[21] = 0.0009159502770000001;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[22] = 0.0010571675400000001;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[23] = 0.001127295398;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Max[24] = 0.0012763615500000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_00_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_00_XSec_XLO_Min( double kggZZpolar_gg_eemm_00_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[0] = 4.4959875e-09;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[1] = 1.41128973e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[2] = 6.30110032e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[3] = 1.5674028900000001e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[4] = 2.9689292999999996e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[5] = 7.0344198e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[6] = 9.6658239e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[7] = 1.6181507e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[8] = 2.8128145000000002e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[9] = 3.2603506000000004e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[10] = 4.75547105e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[11] = 6.42465897e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[12] = 9.212535e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[13] = 0.00011034617999999999;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[14] = 0.000153623448;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[15] = 0.000188155038;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[16] = 0.00022206174;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[17] = 0.000255541518;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[18] = 0.000291341033;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[19] = 0.000365187158;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[20] = 0.00043394962;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[21] = 0.0005154370830000001;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[22] = 0.000586964664;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[23] = 0.000622824412;	 // [pb]
	kggZZpolar_gg_eemm_00_XSec_XLO_Min[24] = 0.0006967988609999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_00_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_00_XPDF_XLO_Cen( double kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[0] = 7.19358e-09;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[1] = 2.06631e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[2] = 8.84986e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[3] = 2.14419e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[4] = 3.98514e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[5] = 9.19532e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[6] = 1.25043e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[7] = 2.0561e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[8] = 3.50725e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[9] = 4.0451e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[10] = 5.82065e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[11] = 7.80639e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[12] = 0.000113735;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[13] = 0.000137076;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[14] = 0.000193969;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[15] = 0.000239383;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[16] = 0.00028506;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[17] = 0.000330157;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[18] = 0.000378857;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[19] = 0.000479878;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[20] = 0.00057553;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[21] = 0.000688167;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[22] = 0.000788931;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[23] = 0.000839386;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[24] = 0.000945453;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_00_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_00_XPDF_XLO_Max( double kggZZpolar_gg_eemm_00_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[0] = 7.746046944e-09;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[1] = 2.128092669e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[2] = 9.025972214000001e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[3] = 2.176996107e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[4] = 4.036548305999999e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[5] = 9.297388052e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[6] = 1.263934644e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[7] = 2.0783058799999996e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[8] = 3.5468819250000006e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[9] = 4.09121414e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[10] = 5.889915735e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[11] = 7.901627958e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[12] = 0.00011516806099999999;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[13] = 0.0001388168652;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[14] = 0.0001964518032;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[15] = 0.00024244710239999998;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[16] = 0.000288708768;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[17] = 0.00033438300959999994;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[18] = 0.00038366848389999997;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[19] = 0.0004859244628;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[20] = 0.000582666572;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[21] = 0.0006964938207000001;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[22] = 0.0007983192789;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[23] = 0.0008492907548;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Max[24] = 0.0009564202548;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_00_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillggZZpolar_gg_eemm_00_XPDF_XLO_Min( double kggZZpolar_gg_eemm_00_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array or gpbConv conversion factor
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[0] = 6.641113056e-09;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[1] = 2.0045273309999998e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[2] = 8.673747786e-07;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[3] = 2.1113838929999997e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[4] = 3.933731693999999e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[5] = 9.093251948e-06;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[6] = 1.2369253559999999e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[7] = 2.0338941199999998e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[8] = 3.467618075e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[9] = 3.99898586e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[10] = 5.751384265e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[11] = 7.711152042e-05;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[12] = 0.000112301939;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[13] = 0.00013533513479999998;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[14] = 0.0001914861968;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[15] = 0.00023631889759999998;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[16] = 0.000281411232;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[17] = 0.00032593099039999996;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[18] = 0.0003740455161;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[19] = 0.0004738315372;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[20] = 0.000568393428;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[21] = 0.0006798401793;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[22] = 0.0007795427211;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[23] = 0.0008294812452;	 // [pb]
	kggZZpolar_gg_eemm_00_XPDF_XLO_Min[24] = 0.0009344857451999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kggZZpolar_gg_eemm_00_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
