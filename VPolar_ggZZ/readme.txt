# R. Ruiz
# 2024 May 24

For details about this project, please see the paper

%\cite{Javurkova:2024bwa}
\bibitem{Javurkova:2024bwa}
M.~Javurkova, R.~Ruiz, R.~C.~L.~de S\'a and J.~Sandesara,
%``Polarized ZZ pairs in gluon fusion and vector boson fusion at the LHC,''
[arXiv:2401.17365 [hep-ph]].
