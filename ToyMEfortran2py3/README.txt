# R. Ruiz
# March 2025
# version 0.9
#

The following files give an example of plotting a fortran-based function in python3

Files:
- toyF2PY3example.tgz [self-contained package]
- toy_matrix_element.f [matrix element and standalone fortran code]
- makefile [makefile for fortran program and python lib]
- plotToyXSec.py [plotting routine]
- f2py3_example.png [final output]
- this README file

Usage:
$ tar -zxvf toyF2PY3example.tgz
$ cd ./ToyF2PY3example
$ ls 
$ make
$ python ./plotToyXSec.py
$ ls 

The final line will generate f2py3_example.png
