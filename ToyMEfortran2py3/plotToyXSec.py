# R. Ruiz
# March 2025
# plotToyXSec.py
# version 0.9
#
# this program will print the 
# value of a function written in fortran 
# libToyXSec should be compiled with f2py3

from libToyXSec import gettoyme2fixedmom as getToyME2
import matplotlib.pyplot as plt
import numpy as np

print("starting program")


# initialize inputs
smInputs = [1.0,2.0,3.0,4.0,5.0]
xVal = list(np.linspace(1,10,15))
yVal = []

# compute M2 for each input
kk=0
kMax=len(xVal)
for xx in xVal:
    kk = kk+1
    print("calling fortran function (%i/%i)" % (kk,kMax))
    tmp = getToyME2(smInputs)
    yVal.append(tmp)


# plot output
plt.plot(xVal,yVal,linestyle="solid",color="purple")
plt.xlabel(r'virtuality $\sqrt{q^2}$ [GeV]',fontsize=15)
plt.ylabel(r'$\vert\mathcal{M}\vert^2$ [a.u.]',fontsize=15)

# save output to file
print("saving f2py3_example.png to file")
plt.savefig('f2py3_example.png',bbox_inches='tight',dpi=150)

print("done! have a nice day.")
