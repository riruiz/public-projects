c R. Ruiz
c 2025 March
c name: toy_matrix_element.f
c version 0.9
c 
c this program will print the 
c value of a toy cross section 
c "computed" from a matrix element
c***************************************c
c***************************************c
c***************************************c
c***************************************c
	program getToyXSec
	implicit none
c       inputs    
	double precision p4(1:4,1:2)
	double precision smInputs(5)
	double precision getToyME2,getToyME2FixedMom

	double precision tmp
	external getToyME2,getToyME2FixedMom



	p4(:,1) = (/ 1.d0,0.d0,0.d0, 1.d0 /)
	p4(:,2) = (/ 1.d0,0.d0,0.d0,-1.d0 /)
	smInputs(:) = (/ 1,2,3,4,5 /)
	
	tmp = getToyME2(p4,smInputs)
	print*, 'xsec = ',tmp*smInputs(5), ' fb'
	tmp = getToyME2FixedMom(smInputs)
	print*, 'xsec[fixedP4] = ',tmp, ' fb'

	end program getToyXSec
c***************************************c
c***************************************c
c  toy function for ME^2 for fixed argument
	double precision function getToyME2FixedMom(kInputs)
	implicit none
	double precision kInputs(*)
	double precision getToyME2
	external getToyME2

	double precision p4(1:4,2)
	double precision tmp
	p4(:,1) = (/ 1.d0,0.d0,0.d0, 1.d0 /)
	p4(:,2) = (/ 1.d0,0.d0,0.d0,-1.d0 /)
	tmp = getToyME2(p4,kInputs)
	getToyME2FixedMom = tmp*2.d0
	return 
	end
c***************************************c
c***************************************c
c       toy function for ME^2
	double precision function getToyME2(kp4,kInputs)
	implicit none

c       inputs
	double precision kp4(1:4,*)
	double precision kInputs(*)
	complex getToyMatrixElement
	external getToyMatrixElement

	complex ctmp
	double precision tmp2

	ctmp = getToyMatrixElement(kp4,kInputs)
	getToyME2 = cabs(ctmp)**2
	return
	end
c***************************************c
c***************************************c
c***************************************c
c***************************************c
c       toy function for ME
	complex function getToyMatrixElement(kp4,kInputs)
	implicit none

c       inputs
	double precision kp4(1:4,*)
	double precision kInputs(*)

	complex ctmp

	ctmp = (1.d0,1.d0)
	getToyMatrixElement = ctmp
	return
	end
c***************************************c
c***************************************c
c end of file
c***************************************c
c***************************************c
