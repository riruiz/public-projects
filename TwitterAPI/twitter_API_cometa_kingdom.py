# S. Delorme & 
# R. Ruiz
# 2024 March 15
# The author(s) would like to acknowledge the contribution of the COST Action CA22130.
# This software is solely for educational, academic/scientific, and outreach purposes; it is not for commercial usage
#
# replace XXX with actual tokens from https://developer.twitter.com/
# the '' are important

class apiTwitterCometa(object):
    BEARER_TOKEN='XXX'
    ACCESS_TOKEN='XXX'
    ACCESS_TOKEN_SECRET='XXX'
    CONSUMER_KEY='XXX'
    CONSUMER_SECRET='XXX'