#!/usr/bin/bash
# S. Delorme & 
# R. Ruiz
# 2024 March 15
# The author(s) would like to acknowledge the contribution of the COST Action CA22130.
# This software is solely for educational, academic/scientific, and outreach purposes; it is not for commercial usage


project=COMETA
echo running twitter API for $project

apiPath=/.../
apiScript=twitter_API_cometa_Map.py
python $apiPath/$apiScript

echo "done! have a nice day"