# S. Delorme & 
# R. Ruiz
# 2024 March 15
# The author(s) would like to acknowledge the contribution of the COST Action CA22130.
# This software is solely for educational, academic/scientific, and outreach purposes; it is not for commercial usage

This file briefly outlines how COMETA's X/Twitter API operates.
In short, a cron job runs driver.sh once a day, everyday at 16:05. 
The driver script runs the python-based API script. 
The API script can be run by itself, i.e., without cron and driver.sh, but requires the keys from kingdom.py.

File interface:
- cron command calls driver
- driver calls API
- API calls kingdom
- API interfaces with (pulls from) ArXiv and then (pushes to) X/Twitter
- various log files are written for checks
