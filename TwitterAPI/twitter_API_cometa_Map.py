# S. Delorme & 
# R. Ruiz
# 2024 March 15
# The author(s) would like to acknowledge the contribution of the COST Action CA22130.
# This software is solely for educational, academic/scientific, and outreach purposes; it is not for commercial usage
#
# This script will interface first with ArXiv then with X/Twitter
# This script pulls from the ArXiv and pushes to X/Twitter

import urllib
import feedparser
import time
from datetime import date,timedelta,datetime
#import base64
#import hashlib
import os
#import re
#import requests
import tweepy
from requests_oauthlib import OAuth2Session
from twitter_API_cometa_kingdom import apiTwitterCometa as kingdom

#
isAPIdebug=False

# Base api query url
base_url = 'http://export.arxiv.org/api/query?'

# Search parameters

search_query = 'rn:COMETA' #Basic search query using the report number
#search_query = 'cat:hep-ph' #Search query by category if needed 
start = 0             
max_results = 3


query = 'search_query=%s&start=%i&max_results=%i&sortBy=lastUpdatedDate&sortOrder=descending' % (search_query,start,max_results)

# perform a GET request using the base_url and query
response = urllib.request.urlopen(base_url+query).read()

# # parse the response using feedparser
feed = feedparser.parse(response)

# what date are we checking: default is 7
#Use to check for articles from exactly 7 days ago    
dateOffset=7
if(isAPIdebug):
    dateOffset=45
b = (date.today() - timedelta(days=dateOffset)).timetuple() 


# prepare log for log file
outLog = "\n#### today is " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " ####\n"

for i in range(len(feed.entries)):

    a = feed.entries[i].updated_parsed

    #Check on the year, month and day, because the complete date uses also the time of the day, which can break the routine
    if (a[0] == b[0]) and (a[1] == b[1]) and (a[2] == b[2]): 

        # extract author information
        author = feed.entries[i].authors[0].name
        authorEtAl = ""
        if(len(feed.entries[i].authors) > 1):
            authorEtAl = ", et. al."
        
        # extract article data
        id = feed.entries[i].id
        title = feed.entries[i].title

        # build first line
        tweetBase = "🚨 new #COMETA paper! 🚨\n"
        if(isAPIdebug):
            tweetBase = "twitter API test!\n" + tweetBase
        
        # build tweet
        tweet = tweetBase + author + authorEtAl + "\n" + title + "\n" + "☄️ " + id + " ☄️"

        # shorten tweet by cutting down title
        if len(tweet) > 280:
            print("failed to shorten tweet! (trying to cut title)")
            diff = len(tweet) - 280
            if(len(title) is (diff+3)):
                title = feed.entries[i].title[0:len(title)-diff-3]
            else:
                title = feed.entries[i].title[0:len(title)-diff-3] + "..."
            tweet = tweetBase + author + authorEtAl + "\n" + title + "\n" + "☄️ " + id + " ☄️"

        # short tweet by cutting down author names
        if len(tweet) > 280:
            print("failed to shorten tweet! (removing authors)")
            author = ""
            authorEtAl = ""
            title = ""
            tweet = tweetBase + author + authorEtAl + "\n" + title + "☄️☄️ " + id + " ☄️☄️"
            

        print(tweet,"\n")
        outLog = outLog + tweet + "\n"


# define client library; inherit keys from kingdom
client = tweepy.Client(
    consumer_key=kingdom.CONSUMER_KEY,
    consumer_secret=kingdom.CONSUMER_SECRET,
    access_token=kingdom.ACCESS_TOKEN,
    access_token_secret=kingdom.ACCESS_TOKEN_SECRET
)

if(isAPIdebug):
    tweet="twitter API test!\n" + "\n#### today is " + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "####\n"

# push tweet
client.create_tweet(text=tweet)

# push to log file
logFile = open("twitter_API_cometa_tweet.log","a")
logFile.write(outLog)
logFile.close
