# R. Ruiz
# Aug 2024
# plot_0vBBSandbox.py
# 
# Summary: generate abs(mee) vs lightest neutrino mass for NO and IO
# Usage: $ python ./plot_0vBBSandbox.py
# output: 0vBBSandbox_mElEl_vs_mass.png and .pdf

# Structure:
# 1. define NO and IO input classes
# 2. define functions (calculate m_ee)
# 3. main program (plot)
# 4. write to file


import math
import cmath
from   matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import fmin as spyci_min

print("starting the sandbox")

# ############################################
# 1. define NO and IO input classes (pt 1/2) #
# ############################################
class nuInputsNO:
    """neutrino oscillation inputs NormalOrdering m1 < m2 < m3 (NuFitv5.3 -- March 2024)"""
    
    order = "NO" # NormalOrdering
    convhbarc = 0.1973269804 * 1e18 * 1e-9 # GeV-fm * (km/fm) * (eV/GeV)
    convGeV2eV  = 1e-9
    convRad2Deg = 180. / math.pi
    convDeg2Rad = math.pi / 180.

    # degrees
    th12_deg_cen = 33.67
    th12_deg_max = th12_deg_cen+0.73
    th12_deg_min = th12_deg_cen-0.71

    th23_deg_cen = 42.3
    th23_deg_max = th23_deg_cen + 1.1
    th23_deg_min = th23_deg_cen - 0.9

    th13_deg_cen = 8.58
    th13_deg_max = th13_deg_cen + 0.11
    th13_deg_min = th13_deg_cen - 0.11

    dxCP_deg_cen = 232
    dxCP_deg_max = dxCP_deg_cen + 39
    dxCP_deg_min = dxCP_deg_cen - 25

    # radians
    th12_cen = th12_deg_cen * convDeg2Rad
    th12_max = th12_deg_max * convDeg2Rad
    th12_min = th12_deg_min * convDeg2Rad

    th23_cen = th23_deg_cen * convDeg2Rad
    th23_max = th23_deg_max * convDeg2Rad
    th23_min = th23_deg_min * convDeg2Rad

    th13_cen = th13_deg_cen * convDeg2Rad
    th13_max = th13_deg_max * convDeg2Rad
    th13_min = th13_deg_min * convDeg2Rad

    dxCP_cen = dxCP_deg_cen * convDeg2Rad
    dxCP_max = dxCP_deg_max * convDeg2Rad
    dxCP_min = dxCP_deg_min * convDeg2Rad

    # eV^2
    dm21Sq_cen = 7.41e-5
    dm21Sq_max = dm21Sq_cen + 0.21e-5
    dm21Sq_min = dm21Sq_cen - 0.20e-5

    dm31Sq_cen = 2.505e-3
    dm31Sq_max = dm31Sq_cen + 0.024e-3
    dm31Sq_min = dm31Sq_cen - 0.026e-3


# ############################################
# 1. define NO and IO input classes (pt 2/2) #
# ############################################

class nuInputsIO:
    """neutrino oscillation inputs InverseOrdering m3 < m1 < m2 (NuFitv5.3 -- March 2024)"""

    order = "IO" # InverseOrdering
    convhbarc = 0.1973269804 * 1e18 * 1e-9 # GeV-fm * (km/fm) * (eV/GeV)
    convGeV2eV  = 1e-9
    convRad2Deg = 180. / math.pi
    convDeg2Rad = math.pi / 180.

    # degrees
    th12_deg_cen = 33.67
    th12_deg_max = th12_deg_cen+0.73
    th12_deg_min = th12_deg_cen-0.71

    th23_deg_cen = 48.9
    th23_deg_max = th23_deg_cen + 0.9
    th23_deg_min = th23_deg_cen - 1.2

    th13_deg_cen = 8.57
    th13_deg_max = th13_deg_cen + 0.13
    th13_deg_min = th13_deg_cen - 0.11

    dxCP_deg_cen = 273
    dxCP_deg_max = dxCP_deg_cen + 24
    dxCP_deg_min = dxCP_deg_cen - 26

    # radians
    th12_cen = th12_deg_cen * convDeg2Rad
    th12_max = th12_deg_max * convDeg2Rad
    th12_min = th12_deg_min * convDeg2Rad

    th23_cen = th23_deg_cen * convDeg2Rad
    th23_max = th23_deg_max * convDeg2Rad
    th23_min = th23_deg_min * convDeg2Rad

    th13_cen = th13_deg_cen * convDeg2Rad
    th13_max = th13_deg_max * convDeg2Rad
    th13_min = th13_deg_min * convDeg2Rad

    dxCP_cen = dxCP_deg_cen * convDeg2Rad
    dxCP_max = dxCP_deg_max * convDeg2Rad
    dxCP_min = dxCP_deg_min * convDeg2Rad

    # eV^2
    dm21Sq_cen = 7.41e-5
    dm21Sq_max = dm21Sq_cen + 0.21e-5
    dm21Sq_min = dm21Sq_cen - 0.20e-5

    dm32Sq_cen = -2.487e-3
    dm32Sq_max = dm32Sq_cen + 0.027e-3
    dm32Sq_min = dm32Sq_cen - 0.024e-3

# #####################
# 2. define functions #
# #####################

def getBoundMelel(kIsLowerBound=True):
    """return limits on |melel| in [eV] from GERDA [arXiv:2009.06079]"""

    if(kIsLowerBound):
            return 79e-3 
    else:
            return 180e-3

def getNuMasses_from_mvlightest(kmv=0,kIsNO=True):
    """compute nu masses starting from mv_lightest; returns 3d array"""
    
    if(kIsNO is True):
        nuInputs = nuInputsNO # m1 < m2 < m3
        mv1 = kmv #lightest
        mv2 = math.sqrt(mv1**2 + nuInputs.dm21Sq_cen) # = 1 + (2-1)
        mv3 = math.sqrt(mv1**2 + nuInputs.dm31Sq_cen) # = 1 + (3-1)
    else:
        nuInputs = nuInputsIO # m3 < m1 < m2
        mv3 = kmv #lightest
        mv2 = math.sqrt(mv3**2 - nuInputs.dm32Sq_cen) # = 3 - (3-2)
        mv1 = math.sqrt(mv2**2 - nuInputs.dm21Sq_cen) # = 3 - (3-2) - (2-1)

    nuMasses = [mv1,mv2,mv3]
    return nuMasses

def calcAbsMelel(kEta,kmv=0,kSgn=1,kIsNO=True):
    """compute abs(m_ee) starting from mv_lightest, phases """

    eta1=kEta[0]
    eta2=kEta[1]
    sign= 1 if (kSgn>0) else -1 # used only for finding maximum
    # get nuInputs
    nuInputs = nuInputsNO
    if(kIsNO is False):
        nuInputs = nuInputsIO
    
    # get nuMasses
    nuMasses = getNuMasses_from_mvlightest(kmv,kIsNO)

    # get sin/cos
    mix_c12 = math.cos(nuInputs.th12_cen)
    mix_s12 = math.sin(nuInputs.th12_cen)
    mix_c13 = math.cos(nuInputs.th13_cen)
    mix_s13 = math.sin(nuInputs.th13_cen)
    mix_dCP = nuInputs.dxCP_cen

    # get Ue1
    mix_Ue1 = mix_c12*mix_c13*cmath.exp(complex(0,eta1))
    mix_Ue2 = mix_s12*mix_c13*cmath.exp(complex(0,eta2))
    mix_Ue3 = mix_s13*cmath.exp(complex(0,-mix_dCP))

    mll = mix_Ue1**2 *nuMasses[0] \
        + mix_Ue2**2 *nuMasses[1] \
        + mix_Ue3**2 *nuMasses[2] 

    return sign*abs(mll) # max(f) = -min(-f)

def getAbsMelelMin(kmv=0,kIsNO=True):
    """compute minimum of abs(m_ee) starting from mv_lightest assuming unknown Majorana phases """

    # set min/max condition
    sign=+1

    # set precision
    tiny=1e-10

    # build test guess
    piover2 = math.pi/2
    etaInit=[piover2,piover2]

    # minimize
    output=spyci_min(calcAbsMelel,x0=etaInit,args=(kmv,sign,kIsNO),xtol=tiny,ftol=tiny,full_output=True,disp=False)

    return sign*output[1]

def getAbsMelelMax(kmv=0,kIsNO=True):
    """compute maximum of abs(m_ee) starting from mv_lightest assuming unknown Majorana phases """

    # set min/max condition
    sign=-1 
    
    # set precision
    tiny=1e-10

    # build test guess
    piover2 = math.pi/2
    etaInit=[piover2,piover2]

    # minimize
    output=spyci_min(calcAbsMelel,x0=etaInit,args=(kmv,sign,kIsNO),xtol=tiny,ftol=tiny,full_output=True,disp=False)

    return sign*output[1] 

def lighten_color(color, amount=0.5):
    # https://stackoverflow.com/questions/37765197/darken-or-lighten-a-color-in-matplotlib
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])

# ########################
# 3. main program / plot #
# ########################
print("plotting mBB vs m_light")

# aesthetics (global)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "font.serif" : ['Computer Modern']
})
plt.tick_params(which="major",top=True,bottom=True,left=True,right=True,direction="in")
plt.tick_params(which="minor",top=True,bottom=True,left=True,right=True,direction="in")
#plt.title(r"$...$ words",fontsize=15)

# generate (x,y) values
#xVal = list(np.logspace(-4.75,0.5,100)) # reduced for CPU consumption
xVal = list(np.logspace(-4.75,0.5,1000)) # use this to reproduce plot

yValMElEl_NO_max = list(getAbsMelelMax(xx,True)  for xx in xVal)
yValMElEl_NO_min = list(getAbsMelelMin(xx,True)  for xx in xVal)
yValMElEl_IO_max = list(getAbsMelelMax(xx,False) for xx in xVal)
yValMElEl_IO_min = list(getAbsMelelMin(xx,False) for xx in xVal)
yValMElEl_XUnityX = list(10 for  xx in xVal) 
yValMElEl_Bnd_max = list(getBoundMelel(False) for  xx in xVal) 
yValMElEl_Bnd_min = list(getBoundMelel(True) for  xx in xVal) 


# colors
colorPink=(245/255, 169/255, 184/255) # RGB
colorTeal=(91/255, 206/255, 250/255) # RGB

# plot
plt.plot(xVal,yValMElEl_NO_max, color=lighten_color(colorPink,1.5))
plt.plot(xVal,yValMElEl_NO_min, color=lighten_color(colorPink,1.5))
plt.plot(xVal,yValMElEl_IO_max, color=lighten_color(colorTeal,1.5))
plt.plot(xVal,yValMElEl_IO_min, color=lighten_color(colorTeal,1.5))

# fill
plt.fill_between(xVal,yValMElEl_NO_max,yValMElEl_NO_min,color=colorPink,alpha=.3)
plt.fill_between(xVal,yValMElEl_IO_max,yValMElEl_IO_min,color=colorTeal,alpha=.3)

# add limits from GERDA
plt.plot(xVal,yValMElEl_Bnd_max,color="gray") 
plt.plot(xVal,yValMElEl_Bnd_min,color="gray") 
plt.fill_between(xVal,yValMElEl_Bnd_max,yValMElEl_Bnd_min,color="gray",alpha=.2)
plt.fill_between(xVal,yValMElEl_XUnityX,yValMElEl_Bnd_max,color="gray",alpha=.4)

# aesthetics (x-axis)
plt.xlabel(r'$m_{\rm lightest}$ [eV]',fontsize=15)
plt.xlim(6e-5,1.5) # set range of x-axis
plt.xscale('log')
plt.xticks(fontsize=11)

# aesthetics (y-axis)
plt.ylabel(r"$\vert m_{ee}\vert$ [eV]",fontsize=15)
plt.ylim(1e-4,.75) # set range of y-axis
plt.yscale('log')
plt.yticks(fontsize=11)

# aesthetics (inserts)
plt.text(7e-5,3e-4,r"Normal Ordering",fontsize=15,color=plt.gca().lines[0].get_color())
plt.text(10e-5,1.1e-2,r"Inverse Ordering",fontsize=15,color=plt.gca().lines[2].get_color())
plt.text(1e-2,2.5e-4,r'$\delta_{\rm CP}\ \approx\ 232^\circ\ (273)^\circ$ for NO (IH)',fontsize=12)
plt.text(1e-2,1.5e-4,r'$\eta_1,\ \eta_2\ \in\ [0,\pi]$',fontsize=12)

plt.text(1.5e-2,2e-3,r"$m_{ee}\ =\ \sum_{k=1}^3 U_{ek} m_k U_{ek}$",fontsize=15)
plt.text(2.5e-2,1e-3,r"via standard mechanism",fontsize=13)
plt.text(3.75e-2,.6e-3,r"(Weinberg operator)",fontsize=13)

plt.text(9e-5,.95e-1,r"GERDA (2020) 90\% C.L. {\tiny [PRL125, 252502 (2020)]}",fontsize=11)

# ###########################
# 4. output / write to file #
# ###########################
print("generating 0vBBSandbox_mElEl_vs_mass.png at dpi=450")
plt.savefig('0vBBSandbox_mElEl_vs_mass.png',bbox_inches='tight',dpi=450)
print("generating 0vBBSandbox_mElEl_vs_mass.pdf")
plt.savefig('0vBBSandbox_mElEl_vs_mass.pdf',bbox_inches='tight',dpi=450)

print("done! have a nice day.")






