# R. Ruiz
# Aug 2024
# plot_nuOscSandbox.py
# 
# Summary: generate 2-flavor neutrino oscillation plots
# Usage: $ python ./plot_nuOscSandbox.py
# output: nuOscSandbox_2FlavorProb_NO.png and .pdf

# Structure:
# 1. define NO and IO input classes
# 2. define functions (oscillation probabilities)
# 3. main program (plot)
# 4. write to file


import math
from   matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np

print("starting the sandbox")

# ############################################
# 1. define NO and IO input classes (pt 1/2) #
# ############################################
class nuInputsNO:
    """neutrino oscillation inputs NormalOrdering m1 < m2 < m3 (NuFitv5.3 -- March 2024)"""
    
    order = "NO" # NormalOrdering
    convhbarc = 0.1973269804 * 1e18 * 1e-9 # GeV-fm * (km/fm) * (eV/GeV)
    convGeV2eV  = 1e-9
    convRad2Deg = 180. / math.pi
    convDeg2Rad = math.pi / 180.

    # degrees
    th12_deg_cen = 33.67
    th12_deg_max = th12_deg_cen+0.73
    th12_deg_min = th12_deg_cen-0.71

    th23_deg_cen = 42.3
    th23_deg_max = th23_deg_cen + 1.1
    th23_deg_min = th23_deg_cen - 0.9

    th13_deg_cen = 8.58
    th13_deg_max = th13_deg_cen + 0.11
    th13_deg_min = th13_deg_cen - 0.11

    dxCP_deg_cen = 232
    dxCP_deg_max = dxCP_deg_cen + 39
    dxCP_deg_min = dxCP_deg_cen - 25

    # radians
    th12_cen = th12_deg_cen * convDeg2Rad
    th12_max = th12_deg_max * convDeg2Rad
    th12_min = th12_deg_min * convDeg2Rad

    th23_cen = th23_deg_cen * convDeg2Rad
    th23_max = th23_deg_max * convDeg2Rad
    th23_min = th23_deg_min * convDeg2Rad

    th13_cen = th13_deg_cen * convDeg2Rad
    th13_max = th13_deg_max * convDeg2Rad
    th13_min = th13_deg_min * convDeg2Rad

    dxCP_cen = dxCP_deg_cen * convDeg2Rad
    dxCP_max = dxCP_deg_max * convDeg2Rad
    dxCP_min = dxCP_deg_min * convDeg2Rad

    # eV^2
    dm21Sq_cen = 7.41e-5
    dm21Sq_max = dm21Sq_cen + 0.21e-5
    dm21Sq_min = dm21Sq_cen - 0.20e-5

    dm31Sq_cen = 2.505e-3
    dm31Sq_max = dm31Sq_cen + 0.024e-3
    dm31Sq_min = dm31Sq_cen - 0.026e-3


# ############################################
# 1. define NO and IO input classes (pt 2/2) #
# ############################################

class nuInputsIO:
    """neutrino oscillation inputs InverseOrdering m3 < m1 < m2 (NuFitv5.3 -- March 2024)"""

    order = "IO" # InverseOrdering
    convhbarc = 0.1973269804 * 1e18 * 1e-9 # GeV-fm * (km/fm) * (eV/GeV)
    convGeV2eV  = 1e-9
    convRad2Deg = 180. / math.pi
    convDeg2Rad = math.pi / 180.

    # degrees
    th12_deg_cen = 33.67
    th12_deg_max = th12_deg_cen+0.73
    th12_deg_min = th12_deg_cen-0.71

    th23_deg_cen = 48.9
    th23_deg_max = th23_deg_cen + 0.9
    th23_deg_min = th23_deg_cen - 1.2

    th13_deg_cen = 8.57
    th13_deg_max = th13_deg_cen + 0.13
    th13_deg_min = th13_deg_cen - 0.11

    dxCP_deg_cen = 273
    dxCP_deg_max = dxCP_deg_cen + 24
    dxCP_deg_min = dxCP_deg_cen - 26

    # radians
    th12_cen = th12_deg_cen * convDeg2Rad
    th12_max = th12_deg_max * convDeg2Rad
    th12_min = th12_deg_min * convDeg2Rad

    th23_cen = th23_deg_cen * convDeg2Rad
    th23_max = th23_deg_max * convDeg2Rad
    th23_min = th23_deg_min * convDeg2Rad

    th13_cen = th13_deg_cen * convDeg2Rad
    th13_max = th13_deg_max * convDeg2Rad
    th13_min = th13_deg_min * convDeg2Rad

    dxCP_cen = dxCP_deg_cen * convDeg2Rad
    dxCP_max = dxCP_deg_max * convDeg2Rad
    dxCP_min = dxCP_deg_min * convDeg2Rad

    # eV^2
    dm21Sq_cen = 7.41e-5
    dm21Sq_max = dm21Sq_cen + 0.21e-5
    dm21Sq_min = dm21Sq_cen - 0.20e-5

    dm32Sq_cen = -2.487e-3
    dm32Sq_max = dm32Sq_cen + 0.027e-3
    dm32Sq_min = dm32Sq_cen - 0.024e-3

# #####################
# 2. define functions #
# #####################
def prob_el_to_el_2flav(kLKM,kEGeV,kIsNO=True):
    """probability for el -> el transition at distance kLKM and energy of kEGeV"""
    nuInputs = nuInputsNO
    if(kIsNO is False):
        nuInputs = nuInputsIO

    # max amplitude term
    sineArg = 2*nuInputs.th12_cen
    sineTerm = math.sin(sineArg)**2

    # L/E term
    loeArg = (4 * kEGeV * nuInputs.convhbarc * nuInputs.convGeV2eV)
    loeArg = nuInputs.dm21Sq_cen * kLKM / loeArg
    loeTerm = math.sin(loeArg)**2

    return 1.0 - sineTerm*loeTerm
    
def prob_mu_to_mu_2flav(kLKM,kEGeV,kIsNO=True):
    """probability for mu -> mu transition at distance kLKM and energy of kEGeV"""
    return prob_el_to_el_2flav(kLKM,kEGeV,kIsNO)

def prob_el_to_mu_2flav(kLKM,kEGeV,kIsNO=True):
    """probability for el -> mu transition at distance kLKM and energy of kEGeV"""
    return 1.0 - prob_el_to_el_2flav(kLKM,kEGeV,kIsNO)

def maxProb_el_to_mu_2flav(kIsNO=True):
    """max probability for el -> mu transition at distance kLKM and energy of kEGeV"""
    nuInputs = nuInputsNO
    if(kIsNO is False):
        nuInputs = nuInputsIO

    # max amplitude term
    sineArg = 2*nuInputs.th12_cen
    sineTerm = math.sin(sineArg)**2

    # L/E term
    loeTerm = 1.0

    return sineTerm*loeTerm

def prob_mu_to_el_2flav(kLKM,kEGeV,kIsNO=True):
    """probability for mu -> el transition for at distance kLKM and energy of kEGeV"""
    return prob_el_to_mu_2flav(kLKM,kEGeV,kIsNO=True)


# ########################
# 3. main program / plot #
# ########################
print("plotting el -> el transition with NO (2-flavor scenario)")

# aesthetics (global)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "font.serif" : ['Computer Modern']
})
plt.tick_params(which="major",top=True,bottom=True,left=True,right=True,direction="in")
plt.tick_params(which="minor",top=True,bottom=True,left=True,right=True,direction="in")
plt.title(r"$\nu_{\ell}\ \to\ \nu_{\ell'}$ transition probability in 2-flavor scheme",fontsize=15)

    
# generate (x,y) values
xVal = list(np.logspace(2,5,1000))
yValMu2Mu = list(prob_el_to_el_2flav(xx,1) for xx in xVal)
yValMu2El = list(prob_el_to_mu_2flav(xx,1) for xx in xVal)
yValMaxPr = list(maxProb_el_to_mu_2flav()  for xx in xVal)

# plot
plt.plot(xVal,yValMu2Mu,linestyle="solid")
plt.plot(xVal,yValMu2El,linestyle="dashed")
plt.plot(xVal,yValMaxPr, color="black")

# aesthetics (x-axis)
plt.xlabel(r'$L\ /\ E_{\nu}\ $ [km/GeV]',fontsize=15)
plt.xlim(xVal[0],xVal[-1]) # set range of x-axis
plt.xscale('log')
plt.xticks(fontsize=11)

# aesthetics (y-axis)
plt.ylabel(r"Prob$(\nu_{\ell}\ \to\ \nu_{\ell'})$",fontsize=15)
plt.yticks(fontsize=11)

# aesthetics (inserts)
plt.text(150,0.95,r"$\nu_\mu\to\nu_e$",fontsize=15,color=plt.gca().lines[0].get_color())
plt.text(150,0.05,r"$\nu_e\to\nu_\mu$",fontsize=15,color=plt.gca().lines[1].get_color())
plt.text(5000, 1.025*maxProb_el_to_mu_2flav(),r'$\sin^2(2\theta_{12})\approx0.85$',fontsize=12)

plt.text(150,0.8,r"Normal Ordering",fontsize=12)
plt.text(150,0.725,r"$\theta_{12}\ \approx\ 33.7^\circ$",fontsize=12)
plt.text(150,0.650,r"$\Delta m_{21}^2\ \approx\ 7.41\times10^{-5}\ {\rm eV}^{2}$",fontsize=12)

# ###########################
# 4. output / write to file #
# ###########################
print("generating nuOscSandbox_2FlavorProb_NO.png at dpi=450")
plt.savefig('nuOscSandbox_2FlavorProb_NO.png',bbox_inches='tight',dpi=450)
print("generating nuOscSandbox_2FlavorProb_NO.pdf")
plt.savefig('nuOscSandbox_2FlavorProb_NO.pdf',bbox_inches='tight',dpi=450)

print("done! have a nice day.")
