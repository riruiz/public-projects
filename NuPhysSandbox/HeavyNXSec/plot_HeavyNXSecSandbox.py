# R. Ruiz
# October 2024
# plot_HeavyNXSecSandbox.py
# 
# Summary: calculate leading order cross section vs heavy neutrino mass at LHC14
#          for charged current (CC) and neutral current (NC) Drell-Yan (DY) process
#          in phenomenological Type I Seesaw
# Usage: $ python ./plot_HeavyNXSecSandbox.py
# output: heavyNSandbox_DYX_vs_mass_LHCX14.png and .pdf


# Note 1: # to import lhapdf, lhapdf.so should be in PYTHONPATH, e.g., 
# $ echo $PYTHONPATH
# :/usr/local/lib/python3.11/dist-packages
# $ ls /usr/local/lib/python3.11/dist-packages
# lhapdf.so
# Note 2: lhapdf.so is *NOT* /usr/local/lib/libLHAPDF.so


# Code structure:
# 1. define SM, BSM, and Monte Carlo input classes
# 2. define functions (partonic, integrand, hadronic/integral)
# 3. main program (plot)
# 4. write to file

import math
import cmath
import lhapdf
from   matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np
#from scipy.optimize import fmin as spyci_min
from scipy.integrate import nquad as spyci_int
from math import pi as pi

print("starting the sandbox")


# #####################################
# 1.1 define SM and BSM input classes #
# #####################################

class smInputs:
    """SM inputs"""
    hbarc2 = 0.3893793721e9 # pb GeV^2
    #hbarc2 = 0.3893793721e12 # fb GeV^2
    MW = 7.995123e+01 # GeV
    WW = 2.085000e+00 # GeV
    MZ = 9.118760e+01 # GeV
    WZ = 2.495200e+00 # GeV
    MW2 = MW*MW
    MZ2 = MZ*MZ
    aEM = 1./1.279400e+02
    GF = 1.174560e-05 # GeV^-2
    aS = 1.184000e-01
    NC = 3
    sW2 = 1.0 - (MW2/MZ2)
    cW2 = 1.0 - sW2
    pdfName = "cteq6l1"

# ####################################
# 1.2 define SM and BSM input class  #
# ####################################

class nuInputs:
     """heavy neutrino inputs"""
     mN = 100 # GeV
     VelN = 1.0 # ve-N mixing
     VmuN = 0.0 # vm-N mixing
     VtaN = 0.0 # vt-N mixing
     VlN2 = VelN**2 + VmuN**2 + VtaN**2

# #####################################
# 1.3 define Monte Carlo input class  #
# #####################################

class mcInputs:
     """Monte Carlo Integration Inputs"""     
     EpsAbs = 1e-4
     EpsRel = 1e-3
     IsDebug = False
     xValSrt = 100 # [GeV]
     xValEnd = 1550 # [GeV]
     nrBins  = 100
     sqrtS   = 14e3 # [GeV]

# #############################################
# 2.1 define functions (partonic expressions) #
# #############################################

def getParton_CCDY_XSec(kNuInputs,kSqrtSHat):
    """partonic cross section for charged current Drell-Yan (CCDY) production 
    of heavy neutrino in pheno. Type I Seesaw: 
    qqbar > W+ > ell+ N (not summed over charges)"""
    if (kNuInputs.mN) > kSqrtSHat:
         print(f"{kNuInputs.mN=}")
         print(f"{kSqrtSHat=}")
         return -1
    
    sm = smInputs
    sHat = kSqrtSHat**2
    mN2 = (kNuInputs.mN)**2
    VlN2 = kNuInputs.VlN2
    rN = mN2 / sHat
    chargeMultiplicity = 1
    
    preFact = (sm.GF)**2 * (sm.MW2)**2 * VlN2 \
            / (12 * sm.NC * pi)
    numer = sHat * (1-rN)**2 * (2.+rN)
    denom = (sHat-sm.MW2)**2 + (sm.WW*sm.MW)**2

    tmpXSec = chargeMultiplicity*preFact*numer/denom
    return tmpXSec

def getParton_NCDY_XSec(kNuInputs,kSqrtSHat,kIsUpType=True):
    """partonic cross section for neutral current Drell-Yan (NCDY) production 
    of heavy neutrino in pheno. Type I Seesaw: 
    qqbar > Z > nu_ell N + \overline{\nu_\ell} N (summed over nu and antinu)"""
    if (kNuInputs.mN) > kSqrtSHat:
         print(f"{kNuInputs.mN=}")
         print(f"{kSqrtSHat=}")
         return -1
    
    sm = smInputs
    sHat = kSqrtSHat**2
    mN2 = (kNuInputs.mN)**2
    VlN2 = kNuInputs.VlN2
    rN = mN2 / sHat
    chargeMultiplicity = 2
    T3=0
    Qf=0
    if(kIsUpType):
         T3=0.5
         Qf=+2./3.
    else:
         T3=-0.5
         Qf=-1./3.

    gqL=(0.5*T3 - Qf*sm.sW2) - (-0.5*T3)
    gqR=(0.5*T3 - Qf*sm.sW2) + (-0.5*T3)
    
    preFact = (sm.GF)**2 * (sm.MZ2)**2 * VlN2 \
            * (gqL**2 + gqR**2) \
            / (2**2 * 3 * sm.NC * pi)
    numer = sHat * (1-rN)**2 * (2.+rN)
    denom = (sHat-sm.MZ2)**2 + (sm.WZ*sm.MZ)**2

    tmpXSec = chargeMultiplicity*preFact*numer/denom
    return tmpXSec


# ###################################
# 2.2 define functions (integrands) #
# ###################################

def getHadron_CCDY_XSec_Int(ky1,ky2,kNuInputs,kSqrtS,kPDF):
     """hadronic cross section (integrand) for heavy neutrino production via CCDY"""

     tauMin = kNuInputs.mN**2 / kSqrtS**2
     xi1 = (1.0-tauMin)*ky1 + tauMin
     xi2 = (1.0-tauMin/xi1)*ky2 + tauMin/xi1
     tau = xi1*xi2
     sHat = kSqrtS*kSqrtS*tau
     sqrtSHat = math.sqrt(sHat)
     jacob = (1.0-tauMin)*(1.0-tauMin/xi1)
     fScale = sqrtSHat

     pdf = kPDF 
     gx1 = pdf.xfxQ( 0,xi1,fScale) / xi1
     dx1 = pdf.xfxQ( 1,xi1,fScale) / xi1
     ux1 = pdf.xfxQ( 2,xi1,fScale) / xi1
     sx1 = pdf.xfxQ( 3,xi1,fScale) / xi1
     cx1 = pdf.xfxQ( 4,xi1,fScale) / xi1
     bx1 = pdf.xfxQ( 5,xi1,fScale) / xi1
     tx1 = pdf.xfxQ( 6,xi1,fScale) / xi1
     db1 = pdf.xfxQ(-1,xi1,fScale) / xi1
     ub1 = pdf.xfxQ(-2,xi1,fScale) / xi1
     sb1 = pdf.xfxQ(-3,xi1,fScale) / xi1
     cb1 = pdf.xfxQ(-4,xi1,fScale) / xi1
     bb1 = pdf.xfxQ(-5,xi1,fScale) / xi1
     tb1 = pdf.xfxQ(-6,xi1,fScale) / xi1

     gx2 = pdf.xfxQ( 0,xi2,fScale) / xi2
     dx2 = pdf.xfxQ( 1,xi2,fScale) / xi2
     ux2 = pdf.xfxQ( 2,xi2,fScale) / xi2
     sx2 = pdf.xfxQ( 3,xi2,fScale) / xi2
     cx2 = pdf.xfxQ( 4,xi2,fScale) / xi2
     bx2 = pdf.xfxQ( 5,xi2,fScale) / xi2
     tx2 = pdf.xfxQ( 6,xi2,fScale) / xi2
     db2 = pdf.xfxQ(-1,xi2,fScale) / xi2
     ub2 = pdf.xfxQ(-2,xi2,fScale) / xi2
     sb2 = pdf.xfxQ(-3,xi2,fScale) / xi2
     cb2 = pdf.xfxQ(-4,xi2,fScale) / xi2
     bb2 = pdf.xfxQ(-5,xi2,fScale) / xi2
     tb2 = pdf.xfxQ(-6,xi2,fScale) / xi2

     pdfWgt = [0.0 for kk in range(4)] 
     pdfWgt[0] = ux1*db2 + cx1*sb2 #+ tx1*bb2 # u1 db2
     pdfWgt[1] = dx1*ub2 + sx1*cb2 #+ bx1*tb2 # d1 ub2
     pdfWgt[2] = ux2*db1 + cx2*sb1 #+ tx2*bb1 # u2 db1
     pdfWgt[3] = dx2*ub1 + sx2*cb1 #+ bx2*tb1 # d2 ub1


     tmpdSigma = [0.0 for i in range(4)] 
     dSigma = 0
     for kk  in range(4):
          tmpdSigma[kk] = smInputs.hbarc2 * jacob \
                        * pdfWgt[kk] * getParton_CCDY_XSec(kNuInputs,sqrtSHat) 
          dSigma       += tmpdSigma[kk]  
    
     return dSigma

def getHadron_NCDY_XSec_Int(ky1,ky2,kNuInputs,kSqrtS,kPDF):
     """hadronic cross section (integrand) for heavy neutrino production via NCDY"""

     tauMin = kNuInputs.mN**2 / kSqrtS**2
     xi1 = (1.0-tauMin)*ky1 + tauMin
     xi2 = (1.0-tauMin/xi1)*ky2 + tauMin/xi1
     tau = xi1*xi2
     sHat = kSqrtS*kSqrtS*tau
     sqrtSHat = math.sqrt(sHat)
     jacob = (1.0-tauMin)*(1.0-tauMin/xi1)
     fScale = sqrtSHat

     pdf = kPDF 
     gx1 = pdf.xfxQ( 0,xi1,fScale) / xi1
     dx1 = pdf.xfxQ( 1,xi1,fScale) / xi1
     ux1 = pdf.xfxQ( 2,xi1,fScale) / xi1
     sx1 = pdf.xfxQ( 3,xi1,fScale) / xi1
     cx1 = pdf.xfxQ( 4,xi1,fScale) / xi1
     bx1 = pdf.xfxQ( 5,xi1,fScale) / xi1
     tx1 = pdf.xfxQ( 6,xi1,fScale) / xi1
     db1 = pdf.xfxQ(-1,xi1,fScale) / xi1
     ub1 = pdf.xfxQ(-2,xi1,fScale) / xi1
     sb1 = pdf.xfxQ(-3,xi1,fScale) / xi1
     cb1 = pdf.xfxQ(-4,xi1,fScale) / xi1
     bb1 = pdf.xfxQ(-5,xi1,fScale) / xi1
     tb1 = pdf.xfxQ(-6,xi1,fScale) / xi1

     gx2 = pdf.xfxQ( 0,xi2,fScale) / xi2
     dx2 = pdf.xfxQ( 1,xi2,fScale) / xi2
     ux2 = pdf.xfxQ( 2,xi2,fScale) / xi2
     sx2 = pdf.xfxQ( 3,xi2,fScale) / xi2
     cx2 = pdf.xfxQ( 4,xi2,fScale) / xi2
     bx2 = pdf.xfxQ( 5,xi2,fScale) / xi2
     tx2 = pdf.xfxQ( 6,xi2,fScale) / xi2
     db2 = pdf.xfxQ(-1,xi2,fScale) / xi2
     ub2 = pdf.xfxQ(-2,xi2,fScale) / xi2
     sb2 = pdf.xfxQ(-3,xi2,fScale) / xi2
     cb2 = pdf.xfxQ(-4,xi2,fScale) / xi2
     bb2 = pdf.xfxQ(-5,xi2,fScale) / xi2
     tb2 = pdf.xfxQ(-6,xi2,fScale) / xi2

     pdfWgt = [0.0 for kk in range(4)] 
     pdfWgt[0] = ux1*ub2 + cx1*cb2
     pdfWgt[1] = dx1*db2 + sx1*sb2 + bx1*bb2
     pdfWgt[2] = ux2*ub1 + cx2*cb1
     pdfWgt[3] = dx2*db1 + sx2*sb1 + bx2*bb1


     tmpdSigma = [0.0 for i in range(4)] 
     tmpdSigma[0] = pdfWgt[0] * getParton_NCDY_XSec(kNuInputs,sqrtSHat,True) 
     tmpdSigma[1] = pdfWgt[1] * getParton_NCDY_XSec(kNuInputs,sqrtSHat,False) 
     tmpdSigma[2] = pdfWgt[2] * getParton_NCDY_XSec(kNuInputs,sqrtSHat,True) 
     tmpdSigma[3] = pdfWgt[3] * getParton_NCDY_XSec(kNuInputs,sqrtSHat,False) 

     dSigma = 0
     for kk  in range(4):
          tmpdSigma[kk] *= smInputs.hbarc2 * jacob
          dSigma += tmpdSigma[kk]
    
     return dSigma


# ##################################
# 2.3 define functions (integrals) #
# ##################################

def getHadron_CCDY_XSec(kNuInputs,kSqrtS,kPDF):
     """hadronic cross section (integral) for heavy neutrino production via CCDY"""
     
     mc = mcInputs
     dPSVol=[[0,1],[0,1]]
     sigma = spyci_int(getHadron_CCDY_XSec_Int,dPSVol,args=(kNuInputs,kSqrtS,kPDF),
                       opts=dict(epsabs=mc.EpsAbs,epsrel=mc.EpsRel),full_output=True)
     return sigma

def getHadron_NCDY_XSec(kNuInputs,kSqrtS,kPDF):
     """hadronic cross section (integral) for heavy neutrino production via NCDY"""

     mc = mcInputs
     dPSVol=[[0,1],[0,1]]
     sigma = spyci_int(getHadron_NCDY_XSec_Int,dPSVol,args=(kNuInputs,kSqrtS,kPDF),
                       opts=dict(epsabs=mc.EpsAbs,epsrel=mc.EpsRel),full_output=True)
     return sigma



# #############################
# 2.4 define functions (misc) #
# #############################

def lighten_color(color, amount=0.5):
    # https://stackoverflow.com/questions/37765197/darken-or-lighten-a-color-in-matplotlib
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])



# ########################
# 3. main program / plot #
# ########################
print("computing xsec for CCDY and NCDY production of heavy neutrinos")

nuParam = nuInputs
mc = mcInputs
print("collider center-of-mass energy set to %i [GeV]" % int(mc.sqrtS))
print("loading PDFs")
pdf = lhapdf.mkPDF(smInputs.pdfName,0) # 0 = central member number

#xVal = list(np.logspace(1,3,5)) # generate masses in GeV
#xVal = [100,1600]
xVal = np.linspace(mc.xValSrt,mc.xValEnd,mc.nrBins)
if(mcInputs.IsDebug):
     print(f"{xVal=}")
yValCCDY = []
yValNCDY = []
yValCCoN = []
yValOneX = list(1.0 for xx in xVal)

binNr=0
for xx in xVal:
     binNr = binNr+1
     nuParam.mN = xx
     print("computing xsec for %f [GeV] (%i/%i)" % (xx,binNr,len(xVal)) )
     tmpCC = getHadron_CCDY_XSec(nuParam,mc.sqrtS,pdf)
     tmpNC = getHadron_NCDY_XSec(nuParam,mc.sqrtS,pdf)
     yValCCDY.append(tmpCC[0])
     yValNCDY.append(tmpNC[0])
     yValCCoN.append(tmpCC[0]/tmpNC[0])

if(mcInputs.IsDebug):
     print(f"{yValCCDY=}")
     print(f"{yValNCDY=}")


# ########################
# 3. main program / plot #
# ########################
print("plotting xsec vs mN")

# aesthetics (global)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "font.serif" : ['Computer Modern Roman']
})
#fontUpper = {'family':'serif','size':15}
#fontLower = {'family':'serif','size':13}
fontUpper = {'size':15}
fontLower = {'size':13}

fig = plt.figure()
gs = fig.add_gridspec(2, hspace=0,height_ratios=[4, 1])
ax = gs.subplots(sharex=True, sharey=False)

# upper panel
plotCCDY, = ax[0].plot(xVal,yValCCDY,linestyle='solid')
plotNCDY, = ax[0].plot(xVal,yValNCDY,linestyle='dashdot')
#lower panel
plotCCoN, = ax[1].plot(xVal,yValCCoN,linestyle='solid')
plotOneX, = ax[1].plot(xVal,yValOneX,linestyle='solid',color='black')

for axis in ax:
    axis.tick_params(which="major",top=True,bottom=True,left=True,right=True,direction="in")
    axis.tick_params(which="minor",top=True,bottom=True,left=True,right=True,direction="in")
    axis.minorticks_on()


# aesthetics (x-axis - global)
plt.xlabel(r'heavy neutrino mass $m_N$ [GeV]',fontsize=15)
plt.xlim(xVal[0],xVal[-1]) # set range of x-axis
plt.xticks(fontsize=11)

# aesthetics (y-axis - global and by subplot)
plt.yticks(fontsize=11)
ax[0].set_ylabel(r"$\sigma(pp\ \to\ NX)$ [pb]",fontdict=fontUpper)
ax[0].set(ylim=(0.80e-4,100),yscale='log')
ax[1].set_ylabel(r'ratio',fontdict=fontLower)
ax[1].set(ylim=(0.0,3.3))

# aesthetics (inserts)
ax[0].text(300,1,r"$q\overline{q'}\to N\ell^\pm$",
     rotation=0,rotation_mode='anchor',fontsize=15,
     color=ax[0].get_lines()[0].get_color())
ax[0].text(200,3e-3,r"$q\overline{q}\to N\nu_\ell+N\overline{\nu_\ell}$",
     rotation=0,rotation_mode='anchor',fontsize=15,
     color=ax[0].get_lines()[1].get_color())
ax[0].text(900,10,r"LHC $\sqrt{s}=14$ TeV",fontsize=15)
ax[0].text(950,2,r"CTEQ6L1, $\mu^2 = Q^2$",fontsize=13)

ax[1].text(300,2.25,r"$\sigma_{\rm CCDY}\ /\ \sigma_{\rm NCDY}$",fontsize=15)


# ###########################
# 4. output / write to file #
# ###########################
print("generating heavyNSandbox_DYX_vs_mass_LHCX14.png at dpi=450")
plt.savefig('heavyNSandbox_DYX_vs_mass_LHCX14.png',bbox_inches='tight',dpi=450)
print("generating heavyNSandbox_DYX_vs_mass_LHCX14.pdf")
plt.savefig('heavyNSandbox_DYX_vs_mass_LHCX14.pdf',bbox_inches='tight',dpi=450)

print("done! have a nice day.")
