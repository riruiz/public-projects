////////////////////////////////////////////
//  nuWidthBase.cc                        //
//                                        //
// Richard Ruiz                           //
// 2023 April                             //
// This library supports the calculation  //
// of the inclusive hadron ratio for      //
// Neutrino > Lepton/Neutrino + Hadrons   //
//                                        //
// Please cite: ...                       //
//                                        //
//                                        //
// Example usage:                         //
// $ root -l                              //
// [0] .L nuWidthBase.cc                  //
// [1] NuWidth::getInclHadRatio_CC_NNLO(2,.1)//
// (double) 2.1700684                     //
// [2] NuWidth::getInclHadRatio_CC_NNLO(2,0)//
// (double) 2.5495416                     //
//                                        //
////////////////////////////////////////////

/* Note 1: 
Everything is contained in the class ``NuWidth''.
All functions and data members are declared ``static'', 
and therefore can be called without initializing 
an instance of the class. 

See demos in demoNuWidth.cc.

Note 2:
These libraries use LHAPDF for running alpha_s(mu).
It may be helpful to add
gSystem->Load("/usr/local/lib/libLHAPDF.so");

or something similar to your rootlogon.C file.
*/

#include "LHAPDF/LHAPDF.h"
#include "TMath.h"
#include <cmath>
#include <complex>
#include <cstdio>

// /////////////////////////////////////////
// There are four levels to this library
// 1. global constants
// 2. getFunctions - calls ret and cal
// 3. calFunctions - calls ret 
// 4. retFunctions - returns
// /////////////////////////////////////////


#ifndef NuWidth_Base
#define NuWidth_Base

class NuWidth{

  public:
    // THESE ARE SHORT CUTS THAT CALL calInclHadRatio
    static double getInclHadRatio_CC_NNLO(double kMParent,double kMChild=0);
    static double getInclHadRatio_CC_XNLO(double kMParent,double kMChild=0);
    static double getInclHadRatio_CC_XXLO(double kMParent,double kMChild=0);
    static double getInclHadRatio_NC_NNLO(double kMParent,double kMChild=0);
    static double getInclHadRatio_NC_XNLO(double kMParent,double kMChild=0);
    static double getInclHadRatio_NC_XXLO(double kMParent,double kMChild=0);

    // THIS IS THE MAIN WORKHORSE FUNCTION
    static double calInclHadRatio(double kMParent, double kMChild=0,unsigned int kOrder=2,bool kIsCC=kTRUE,bool kIsDEBUG=kFALSE);

    // THIS IS A BONUS FUNCTION
    static double calInclHadRatio_Tau_NP(double kMass=pdg_ml,double kUnc=1.0);

    // THESE ARE HELPER FUNCTIONS THAT RET[URN] SOMETHING
    static double retAlphasFromLHAPDF(double kScale,unsigned int kOrder=2);
    static double retAdHocPSFact(double kMParent, double kMChild=0, double kPow=0);
    static double retPSFuncH1(double kMParent, double kMChild=0); // H1 fn
    static double retPSFuncH2(double kMParent, double kMChild=0); // H2 fn
    static double retPSFuncH3(double kMParent, double kMChild=0); // H3 fn
    static double retGZL2_by_PID(int kPID); // Z coup to LH currents
    static double retGZR2_by_PID(int kPID); // Z coup to RH currents
    static double retB0(unsigned int kNf=3); // QCD beta0
    static double retB1(unsigned int kNf=3); // QCD beta1
    static double retVud2(); // return CKM |V|^2
    static double retVus2(); // return CKM |V|^2
    static double retVub2(); // return CKM |V|^2
    static double retVcd2(); // return CKM |V|^2
    static double retVcs2(); // return CKM |V|^2
    static double retVcb2(); // return CKM |V|^2

    // SPECIAL //
    // this is the power of the meson-suppression phase space factor
    constexpr static double mesonPower = 0.5;   

    // //////////////////////////////
    // DECLARE MANY MASSES/INPUTS  //
    // Ref: PDG, Prog. Theor. Exp. //
    // Phys. 2022, 083C01 (2022)   //
    // ParticleDataGroup:2022pth   //
    // //////////////////////////////

    // fermion masses for alpha_s(mu) running with LHAPDF
    constexpr static double pdg_md = 0.00469; // pid=1 [GeV]
    constexpr static double pdg_mu = 0.00220; // pid=2[GeV]
    constexpr static double pdg_ms = 0.09310; // pid=3 [GeV]
    constexpr static double pdg_mc = 1.2800;  // pid=4 [GeV]
    constexpr static double pdg_mb = 4.1800;  // pid=5 [GeV]
    constexpr static double pdg_mt = 172.69;  // pid=6 [GeV]
    constexpr static double pdg_me = 0.00051099895; // pid=11 [GeV]
    constexpr static double pdg_mm = 0.1056583755;  // pid=13 [GeV]
    constexpr static double pdg_ml = 1.77686; // pid=15 [GeV]
    // meson masses
    constexpr static double pdg_mpip = 0.13957039; // pi^+ [GeV]
    constexpr static double pdg_mkxp = 0.493677; // K^+ [GeV]
    constexpr static double pdg_mdxp = 1.86966;  // D^+ [GeV]
    constexpr static double pdg_mdsp = 1.96835;  // Ds^+ [GeV]
    constexpr static double pdg_mbxp = 5.27934;  // B^+ [GeV]
    constexpr static double pdg_mbcp = 6.27447;   // Bc^+ [GeV]
    constexpr static double pdg_mpi0 = 0.1349768; // pi^0 [GeV]
    constexpr static double pdg_meta = 0.547862; // eta [GeV]
    constexpr static double pdg_metp = 0.95778; // eta' [GeV]
    constexpr static double pdg_metc = 2.9839; // eta_c [GeV]
    constexpr static double pdg_metb = 9.3987; // eta_b [GeV]
    // Lambda_QCD (needed for LHAPDF)
    constexpr static double pdg_Lambda_Nf3 = 0.339; // Nf=3 [GeV]
    constexpr static double pdg_Lambda_Nf4 = 0.296; // Nf=4 [GeV]
    constexpr static double pdg_Lambda_Nf5 = 0.213; // Nf=5 [GeV]
    // CKM elements [2022 PDG]
    constexpr static double pdg_CKM_cabi  = 2.2500-01; // [rad]
    constexpr static double pdg_CKM_th13  = 3.6900-03; // [rad]
    constexpr static double pdg_CKM_th23  = 4.1820-02; // [rad]
    constexpr static double pdg_CKM_del13 = 1.1440+00; // [rad]
    // EW constants and charges
    constexpr static double pdg_hbar  = 6.582119569e-25; // GeV-s
    constexpr static double pdg_hbarc = 0.1973269804; // GeV-fm
    inline static double pdg_hbarcM = pdg_hbarc*1e-15; // GeV-m
    constexpr static double pdg_TL3_u = +0.5; // weak isospin
    constexpr static double pdg_TL3_d = -0.5; // weak isospin
    constexpr static double pdg_TL3_v = +0.5; // weak isospin
    constexpr static double pdg_TL3_l = -0.5; // weak isospin
    constexpr static double pdg_Qem_u = +2./3.; // QED charge
    constexpr static double pdg_Qem_d = -1./3.; // QED charge
    constexpr static double pdg_Qem_v = 0.;     // QED charge
    constexpr static double pdg_Qem_l = -1.;    // QED charge
    // EW inputs assume sW2, MZ, alphaEM
    constexpr static double pdg_aEM = 1./127.951; // alphaEM(MZ)
    constexpr static double pdg_sW2 = 0.23122;    // sin^2(weinberg)
    inline static double pdg_cw2 = 1. - pdg_sW2;  // cos^2(weinberg)
    inline static double pdg_mz2 = TMath::Power(91.1876,2);
    inline static double pdg_mw2 = pdg_mz2 * pdg_cw2;
    inline static double pdg_ee2 = 4.*TMath::Pi()*pdg_aEM; 
    inline static double pdg_gW  = TMath::Sqrt(pdg_ee2/pdg_sW2);
    inline static double pdg_vev = 2.*TMath::Sqrt(pdg_mw2)/pdg_gW;
    inline static double pdg_Gf  = 1./TMath::Sqrt2()/pdg_vev/pdg_vev;
    //inline static double pdg_cw2 = pdg_mw2 / pdg_mz2; // cos^2(weinberg)
    //inline static double pdg_sW2 = 1. - pdg_cw2; // sin^2(weinberg)
    // QCD group theory factors
    constexpr static double pdg_Nc = 3.;
    constexpr static double pdg_Cf = 4./3.;
    constexpr static double pdg_Tf = 0.5; 
};

// //////////////////////
// get-Level Functions //
// //////////////////////

// -----------------------------------------------------------
// gets InclHadRatio for charged current (or neutral current)
// at a NNLO (NLO) [LO] given mass of parent and child
// calInclHadRatio(double kMParent, double kMChild=0,unsigned int kOrder=2,
//                 bool kIsCC=kTRUE,bool kIsDEBUG=kFALSE);
double NuWidth::getInclHadRatio_CC_NNLO(double kMParent,double kMChild/*=0*/){
  return calInclHadRatio(kMParent,kMChild,2,kTRUE);
}
double NuWidth::getInclHadRatio_CC_XNLO(double kMParent,double kMChild/*=0*/){
  return calInclHadRatio(kMParent,kMChild,1,kTRUE);
}
double NuWidth::getInclHadRatio_CC_XXLO(double kMParent,double kMChild/*=0*/){
  return calInclHadRatio(kMParent,kMChild,0,kTRUE);
}
double NuWidth::getInclHadRatio_NC_NNLO(double kMParent,double kMChild/*=0*/){
  return calInclHadRatio(kMParent,kMChild,2,kFALSE);
}
double NuWidth::getInclHadRatio_NC_XNLO(double kMParent,double kMChild/*=0*/){
  return calInclHadRatio(kMParent,kMChild,1,kFALSE);
}
double NuWidth::getInclHadRatio_NC_XXLO(double kMParent,double kMChild/*=0*/){
  return calInclHadRatio(kMParent,kMChild,0,kFALSE);
}


// //////////////////////
// calc-Level Functions //
// //////////////////////

// -----------------------------------------------------------
// calculates non-pert. contribution to incluside hadron decay ratio
// w.r.t. tau > v + had in the SM
// Eq 4 of E. Braaten, Phys. Rev. Lett. 60, 1606 (1988)
double NuWidth::calInclHadRatio_Tau_NP(double kMass/*=pdg_ml*/,double kUnc/*=1.0*/){

  double gluonRef =  0.002*kUnc; // muR=m_tau
  double fermiRef = -0.015*kUnc; // muR=m_tau
  double massXRef = -0.013*kUnc; // muR=m_tau

  double asPieTau = NuWidth::retAlphasFromLHAPDF(pdg_ml);
  double asPieNew = NuWidth::retAlphasFromLHAPDF(kMass);

  double tmp = gluonRef*(asPieNew/asPieTau) + fermiRef*(asPieNew/asPieTau) + massXRef;
  return tmp;
}	


// -----------------------------------------------------------
// calculates pert. contribution to inclusive hadron decay ratio
double NuWidth::calInclHadRatio(double kMParent, double kMChild/*=0*/,
  unsigned int kOrder/*=2*/,bool kIsCC/*=kTRUE*/,bool kIsDEBUG/*=kFALSE*/){

  // virtuality scale (running of alpha_s)
  double scale = kMParent - kMChild;
  if(scale<0){ std::cout << "Error: kMParent < kMChild in " << __func__ << std::endl; return -1;}
  if(scale<0.7 && !kIsDEBUG){std::cout << "Warning: (kMParent - kMChild) too low (below 700 MeV!) in " << __func__ << std::endl; return -1;}

  // this sets power of meson-suppression phase space factor
  double psPower = mesonPower; // see NuWidth::mesonPower
  if(psPower<0){ std::cout << "Error: psPower < 0. psPower = " << psPower << std::endl; return -1;}

  // set dynamic flavor factors by scale
  double flavFact = 0.;
  double nrFlav = 0.;
  
  if(kIsDEBUG){
    flavFact = 1;
    nrFlav = 3;
  }else{
    if(kIsCC){
    // charged current
    if(scale>pdg_mpip){ flavFact += retVud2()*retAdHocPSFact(scale,pdg_mpip,psPower); nrFlav++; nrFlav++;}
    if(scale>pdg_mkxp){ flavFact += retVus2()*retAdHocPSFact(scale,pdg_mkxp,psPower); nrFlav++;}
    if(scale>pdg_mbxp){ flavFact += retVub2()*retAdHocPSFact(scale,pdg_mbxp,psPower); nrFlav++;}
    if(scale>pdg_mdxp){ flavFact += retVcd2()*retAdHocPSFact(scale,pdg_mdxp,psPower); nrFlav++;}
    if(scale>pdg_mdsp){ flavFact += retVcs2()*retAdHocPSFact(scale,pdg_mdsp,psPower); }
    if(scale>pdg_mbcp){ flavFact += retVcb2()*retAdHocPSFact(scale,pdg_mbcp,psPower); }
    }else{
    // neutral current
    if(scale>pdg_mpi0){ flavFact += (retGZL2_by_PID(1)+retGZR2_by_PID(1))*retAdHocPSFact(scale,pdg_mpi0,psPower);  nrFlav++;}
    if(scale>pdg_mpi0){ flavFact += (retGZL2_by_PID(2)+retGZR2_by_PID(2))*retAdHocPSFact(scale,pdg_mpi0,psPower);  nrFlav++;}
    if(scale>pdg_meta){ flavFact += (retGZL2_by_PID(1)+retGZR2_by_PID(1))*retAdHocPSFact(scale,pdg_meta,psPower);  nrFlav++;}
    if(scale>pdg_metc){ flavFact += (retGZL2_by_PID(2)+retGZR2_by_PID(2))*retAdHocPSFact(scale,pdg_metc,psPower);  nrFlav++;}
    if(scale>pdg_metb){ flavFact += (retGZL2_by_PID(1)+retGZR2_by_PID(1))*retAdHocPSFact(scale,pdg_metb,psPower);  nrFlav++;}
    flavFact = flavFact / (retGZL2_by_PID(11)+retGZR2_by_PID(11)); // ratio w.r.t. electron
    }
  }

  // set alpha_s
  double asPie = retAlphasFromLHAPDF(scale,kOrder+1)/TMath::Pi();

  // set r coefficients
  double rr[3];
  rr[0] = 1.;
  rr[1] = 1.9857-0.1153*nrFlav;
  rr[2] = -6.6368-1.2001*nrFlav-0.0052*nrFlav*nrFlav;

  // set d coefficients
  double deltaD[3] = {0.,0.,0.};
  double tmpH1 = retPSFuncH1(kMParent,kMChild);
  double tmpH2 = (kOrder>0) ? retPSFuncH2(kMParent,kMChild) : 0.;
  double tmpH3 = (kOrder>1) ? retPSFuncH3(kMParent,kMChild) : 0.;
  
  switch (kOrder)	{
  case 0: // LO
    deltaD[0] =      rr[0]*tmpH1;
    break;
  case 1: // NLO
    deltaD[0] =      rr[0]*tmpH1;
    deltaD[1] =      rr[1]*tmpH1 
      + (rr[0]*retB0()/4.)*tmpH2;
    break;
  default: // NNLO
    deltaD[0] =       rr[0]*tmpH1;
    deltaD[1] =       rr[1]*tmpH1
      +  (rr[0]*retB0()/4.)*tmpH2;
    deltaD[2] =       rr[2]*tmpH1
      + (rr[1]*retB0()/ 2.)*tmpH2
      + (rr[0]*retB1()/16.)*tmpH2
      + (rr[0]*retB0()*retB0()/8.)*tmpH3;
  }

  // sum all terms
  double ratio = pdg_Nc*flavFact*(
    deltaD[0]*TMath::Power(asPie,0)+ // LO
    deltaD[0]*TMath::Power(asPie,1)+ // LO
    deltaD[1]*TMath::Power(asPie,2)+ // NLO
    deltaD[2]*TMath::Power(asPie,3)  // NNLO
    );

  if(kIsDEBUG){
    for(unsigned int dNr=0; dNr<3; dNr++){
      std::cout << "DEBUG: deltaD["<<dNr<<"] =\t" << deltaD[dNr] << std::endl;
    }

  }

  return ratio;
}

// /////////////////////////
// Return-Level Functions //
// /////////////////////////

// -----------------------------------------------------------
// returns square of CKM matrix elements
double NuWidth::retVud2(){
  double tmpV = TMath::Cos(pdg_CKM_cabi)*TMath::Cos(pdg_CKM_th13);
  return TMath::Power(tmpV,2);
}
double NuWidth::retVus2(){
  double tmpV = TMath::Sin(pdg_CKM_cabi)*TMath::Cos(pdg_CKM_th13);
  return TMath::Power(tmpV,2);
}
double NuWidth::retVub2(){
  std::complex<double> cmplxExpo(TMath::Cos(-pdg_CKM_del13),TMath::Sin(-pdg_CKM_del13)); //  = TMath::Exp(-ii*pdg_CKM_del13)
  std::complex<double> tmpV = TMath::Sin(pdg_CKM_th13)*cmplxExpo;
  return TMath::Power(std::abs(tmpV),2);
}
double NuWidth::retVcd2(){
  std::complex<double> cmplxExpo(TMath::Cos(pdg_CKM_del13),TMath::Sin(pdg_CKM_del13)); //  = TMath::Exp(+ii*pdg_CKM_del13)
  std::complex<double> tmpV = -TMath::Sin(pdg_CKM_cabi)*TMath::Cos(pdg_CKM_th23)
  - TMath::Cos(pdg_CKM_cabi)*TMath::Sin(pdg_CKM_th23)*TMath::Sin(pdg_CKM_th13)*cmplxExpo;
  return TMath::Power(std::abs(tmpV),2);
}
double NuWidth::retVcs2(){
  std::complex<double> cmplxExpo(TMath::Cos(pdg_CKM_del13),TMath::Sin(pdg_CKM_del13)); //  = TMath::Exp(+ii*pdg_CKM_del13)
  std::complex<double> tmpV = TMath::Cos(pdg_CKM_cabi)*TMath::Cos(pdg_CKM_th23)
  - TMath::Sin(pdg_CKM_cabi)*TMath::Sin(pdg_CKM_th23)*TMath::Sin(pdg_CKM_th13)*cmplxExpo;
  return TMath::Power(std::abs(tmpV),2);
}
double NuWidth::retVcb2(){
  double tmpV = TMath::Sin(pdg_CKM_th23)*TMath::Cos(pdg_CKM_th13);
  return TMath::Power(tmpV,2);
}

// -----------------------------------------------------------
// returns QCD beta function coefficients
double NuWidth::retB0(unsigned int kNf/*=3*/){ return (11.*pdg_Nc - 4.*pdg_Tf*kNf)/3.; }
double NuWidth::retB1(unsigned int kNf/*=3*/){ return (34.*pdg_Nc*pdg_Nc/3.) - (20.*pdg_Nc*pdg_Tf*kNf/3.) - (4.*pdg_Cf*pdg_Tf*kNf); }

// -----------------------------------------------------------
// returns square of LH gauge coupling to Z boson of fermion by kPID
double NuWidth::retGZL2_by_PID(int kPID){
  double TL3,Qem,coup;
  switch (abs(kPID)) {
  case 1: // d-type
    TL3 = pdg_TL3_d;
    Qem = pdg_Qem_d;
    break;
  case 2: // u-type
    TL3 = pdg_TL3_u;
    Qem = pdg_Qem_u;
    break;
  case 11: // l-type
    TL3 = pdg_TL3_l;
    Qem = pdg_Qem_l;
    break;
  default: // v-type
    TL3 = pdg_TL3_v;
    Qem = pdg_Qem_v;
  }
  coup = 0.5*TL3-Qem*pdg_sW2 - (-0.5)*TL3;
  return coup*coup;
}

// -----------------------------------------------------------
// returns square of RH gauge coupling to Z boson of fermion by kPID
double NuWidth::retGZR2_by_PID(int kPID){
  double TL3,Qem,coup;
  switch (abs(kPID)){
  case 1: // d-type
    TL3 = pdg_TL3_d;
    Qem = pdg_Qem_d;
    break;
  case 2: // u-type
    TL3 = pdg_TL3_u;
    Qem = pdg_Qem_u;
    break;
  case 11: // l-type
    TL3 = pdg_TL3_l;
    Qem = pdg_Qem_l;
    break;
  default: // v-type
    TL3 = pdg_TL3_v;
    Qem = pdg_Qem_v;  
  }
  coup = 0.5*TL3-Qem*pdg_sW2 + (-0.5)*TL3;
  return coup*coup;
}

// -----------------------------------------------------------
// return phase space integral h1 [defined in arXiv:]
// for parent of mass kMParent and child mass of kMChild 
double NuWidth::retPSFuncH1(double kMParent, double kMChild/*=0*/){
  double rat2 = (kMParent-kMChild)/kMParent;
  if(rat2<0){std::cout  << "Error: kMParent < kMChild in " << __func__ << std::endl; return -1;}
  rat2 = TMath::Power(rat2,2); 

  double sum= rat2 - TMath::Power(rat2,3) + TMath::Power(rat2,4)/2.;
  sum *= 2.;
  return sum;
}

// -----------------------------------------------------------
// return phase space integral h2 [defined in arXiv:]
// for parent of mass kMParent and child mass of kMChild 
double NuWidth::retPSFuncH2(double kMParent, double kMChild/*=0*/){
  double rat2 = (kMParent-kMChild)/kMParent;
  if(rat2<0){std::cout  << "Error: kMParent < kMChild in " << __func__ << std::endl; return -1;}
  rat2 = TMath::Power(rat2,2); 

  double sum= rat2 - TMath::Power(rat2,3)/3. + TMath::Power(rat2,4)/8.;
  sum *= 2.;
  return sum;
}

// -----------------------------------------------------------
// return phase space integral h3 [defined in arXiv:]
// for parent of mass kMParent and child mass of kMChild 
double NuWidth::retPSFuncH3(double kMParent, double kMChild/*=0*/){
  double rat2 = (kMParent-kMChild)/kMParent;
  if(rat2<0){std::cout  << "Error: kMParent < kMChild in " << __func__ << std::endl; return -1;}
  rat2 = TMath::Power(rat2,2); 

  double sum= rat2 - TMath::Power(rat2,3)/9. + TMath::Power(rat2,4)/32.;
  sum *= 2.;
  return sum;
}

// -----------------------------------------------------------
// return ad hoc phase space suppression factor
double NuWidth::retAdHocPSFact(double kMParent, double kMChild/*=0*/, double kPow/*=0*/){
  double psFact = 1. - kMChild*kMChild/kMParent/kMParent;
  return TMath::Power(psFact,kPow);
}

// -----------------------------------------------------------
// return alpha_s(mu) up to kOrder loops with LHAPDF
// [Default:] kOrder set to two loops (NNLO)
double NuWidth::retAlphasFromLHAPDF(double kScale,unsigned int kOrder/*=2*/){
  LHAPDF::AlphaS_Analytic *kalpha = new LHAPDF::AlphaS_Analytic();
  // set order
  kalpha->setOrderQCD(kOrder);
  // Set quark masses for evolution (both transition points and gradients by default)
  kalpha->setQuarkMass(1, pdg_md);
  kalpha->setQuarkMass(2, pdg_mu);
  kalpha->setQuarkMass(3, pdg_ms);
  kalpha->setQuarkMass(4, pdg_mc);
  kalpha->setQuarkMass(5, pdg_mb);
  kalpha->setQuarkMass(6, pdg_mt);
  kalpha->setLambda(3, pdg_Lambda_Nf3);
  kalpha->setLambda(4, pdg_Lambda_Nf4);
  kalpha->setLambda(5, pdg_Lambda_Nf5);

  return kalpha->alphasQ(kScale);
}

#endif
