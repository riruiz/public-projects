////////////////////////////////////////////
//  demoNuWidth.cc                        //
//                                        //
// Richard Ruiz                           //
// 2023 April                             //
// This program demos the calculation     //
// of the inclusive hadron ratio for      //
// Neutrino > Lepton/Neutrino + Hadrons   //
// using the NuWidth++ library.           //
//                                        //
// Please cite: ...                       //
//                                        //
//                                        //
// Example usage:                         //
// $ g++ -o demoNuWidth.bin demoNuWidth.cc\   //
//          $(root-config --cflags --libs)\   //
//          $(lhapdf-config --cflags --libs)\ //
//          -std=gnu++17                      //
//
// $ ./demoNuWidth.bin                    //
//                                        //
////////////////////////////////////////////


#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>
#include "nuWidthBase.cc"

enum {
    nuNrBins=10
};


void getDEBUG_InclHadRatio_CC_tau_DEBUG(); // see bottom of file
void getInclHadRatio_CC_HeavyN_MassScan(); // see bottom of file 


int main(){

    printf("Starting NuWidth++ demonstration (%s) #1/3 with %i mass bins.\n\n", __FILE__,nuNrBins);

    std::ofstream outFile; // output file containing masses and R
    std::string outName = "demoOutput.csv";
    outFile.open(outName);
    outFile << "#" << outName << ": " << __DATE__ << std::endl;
    // explain format
    outFile << "#Entry No.,\t mN [GeV],\t R^NC(mN,mv=0 GeV) at NNLO,\t R^CC(mN,ml=0 GeV) at NNLO" << std::endl;

    double xVal[nuNrBins], tmp[2];
    for(unsigned binNr=0; binNr<nuNrBins; binNr++){
        // set mN
        xVal[binNr] = 1.0 + 0.1*binNr; // [GeV]

        // get R^NC
        tmp[0] = NuWidth::getInclHadRatio_NC_NNLO(xVal[binNr]);
        printf("For mN = %f GeV, at NNLO R^NC(mN,mv=0GeV) = %f\n",
            xVal[binNr], tmp[0]);

        // get R^CC
        tmp[1] = NuWidth::getInclHadRatio_CC_NNLO(xVal[binNr]);

        // write to file
        outFile << binNr << ",\t" << xVal[binNr] << ",\t" << tmp[0] << ",\t" << tmp[1] << std::endl;

    }
    outFile.close();
    printf("\nOutput file %s written to file.\n\n",outName.c_str());


    printf("\n\nStarting demonstration #2/3.\n");
    printf("calling getInclHadRatio_CC_HeavyN_MassScan()");
    getInclHadRatio_CC_HeavyN_MassScan();


    printf("\n\nStarting demonstration #3/3.\n");
    printf("calling getDEBUG_InclHadRatio_CC_tau_DEBUG()");
    getDEBUG_InclHadRatio_CC_tau_DEBUG();

    printf("\n\nDone! Have a nice day!\n");

    return 1;
}


// -----------------------------------------------------------
// gets InclHadRatio for N > mu + Had

void getInclHadRatio_CC_HeavyN_MassScan(){
  unsigned int nrBins=7;
  unsigned int pQCD=2; // NNLO
  double xVal[7] = {0.05,0.50,0.75,0.85,1.00,1.25,1.50}; // mN [GeV]
  double rVal[7];
  double mass_mu = NuWidth::pdg_mm;
  double mass_me = NuWidth::pdg_me;

  printf("\nCalculating R ratio for N > mu + hadrons. mMu = %f\n",mass_mu);
  printf("Note: errors and warnings below are intentionally triggered.\n\n");

  for(unsigned int ii=0; ii<nrBins; ii++){
    // calInclHadRatio(double kMParent, double kMChild=0,unsigned int kOrder=2,bool kIsCC=kTRUE,bool kIsDEBUG=kFALSE);
    rVal[ii] = NuWidth::calInclHadRatio(xVal[ii],mass_mu,pQCD,kTRUE);

    printf("For mN = %f GeV, at NNLO R^CC(mN,mMu) = %f\n",xVal[ii], rVal[ii]);
  }

  printf("\n\nChanging to N > e + hadrons. mEl = %f\n",mass_me);
  for(unsigned int ii=0; ii<nrBins; ii++){
    rVal[ii] = NuWidth::calInclHadRatio(xVal[ii],mass_me,pQCD,kTRUE);

    printf("For mN = %f GeV, at NNLO R^CC(mN,mEl) = %f\n",xVal[ii], rVal[ii]);
  }

}


// -----------------------------------------------------------
// gets InclHadRatio for tau > v + Had in the SM
void getDEBUG_InclHadRatio_CC_tau_DEBUG(){
  double mass_tau = NuWidth::pdg_ml;
  
  std::cout << std::endl << "Running with DEBUG option set to kTRUE" << std::endl;
  unsigned int order=2;
  double rNNLO = NuWidth::calInclHadRatio(mass_tau,0,order,kTRUE,kTRUE);
  std::cout << "R^CC(m_tau,mv=0GeV) at NNLO\t = " << rNNLO << std::endl;
  order=1;
  double rXNLO = NuWidth::calInclHadRatio(mass_tau,0,order,kTRUE,kTRUE);
  std::cout << "R^CC(m_tau,mv=0GeV) at XNLO\t = " << rXNLO << std::endl;
  order=0;
  double rXXLO = NuWidth::calInclHadRatio(mass_tau,0,order,kTRUE,kTRUE);
  std::cout << "R^CC(m_tau,mv=0GeV) at XXLO\t = " << rXXLO << std::endl;
}
