# Team Ions@NLO (AS)
# 2023 Oct 30
# 2023 Dec 8 (added photons - RR)

# ######################
#Total collider energy #
# ######################
minE=1   #TeV variable not used since first step is written explicitly
maxE=110 #TeV
step=2 #energy step       
#procList=['pp_Wpxx', 'pp_Wmxx', 'pp_Wxxx', 'pp_Zxxx'] #singleBoson
#procList=['pp_WpWm', 'pp_WxZx', 'pp_WpZx', 'pp_WmZx', 'pp_ZZxx'] #doubleBoson
#procList=['pp_WxHx', 'pp_WpHx', 'pp_WmHx', 'pp_ZHxx'] #higgs - weak boson
#procList=['pp_WxAx', 'pp_WpAx', 'pp_WmAx', 'pp_ZAxx'] #photon - weak boson
procList=['pp_Ajxx', 'pp_AAxx', 'pp_AAAx'] # photon - X
#procList=[  'pp_WWWx','pp_WWAx','pp_WWHx','pp_WWZx',
#            'pp_ZZWx','pp_ZZAx','pp_ZZHx','pp_ZZZx',
#            'pp_WAAx','pp_ZAAx','pp_WHHx','pp_ZHHx',
#            'pp_WZAx','pp_WAHx','pp_WZHx','pp_ZAHx']
#procList=['pp_WWWW','pp_WWWZ','pp_WWWH','pp_ZZZZ','pp_ZZZW','pp_ZZZH','pp_WWZZ','pp_WWZH','pp_WWHH'] #four-boson
#procList=['pp_ttxx', 'pp_ttZx', 'pp_ttWx'] #top
atomList=['Hx','Cx','Xe', 'Pb']
QCD=['NLO', 'LO']
#procType=procList[3] #every proces
#modeType=atomList[0] #every atom on list
energy=[]

tag1='IonsNLO_'
tag2='_NLOQCD'
tag3='mg5amc_run_'

for i in range(step+minE,maxE+1, step):
    energy.append(i)

proc=0
while proc<len(procList):
    procType=procList[proc]
    Atom=0
    while Atom<len(atomList):
        modeType=atomList[Atom]
        if modeType=='Hx':
            lhapdfid='14400'
        elif modeType=='Cx':
            lhapdfid='990012006'
        elif modeType=='Xe':
            lhapdfid='990131054'
        elif modeType=='Pb':
            lhapdfid='990208082'
        
        procName=procType+'_'+modeType
        #tmpText =procName+'.dat'
        tmpText =tag3+tag1+procName+'.dat'
        oFile = open(tmpText,'w')

        tempText='launch '+tag1+procType+tag2+'\n'\
        +'order=NLO\n'\
        +'fixed_order=ON\n'\
        +'set MT 172.9\n'\
        +'set MH 125.1\n'\
        +'set req_acc_FO 0.002\n'\
        +'set LHC 1\n'\
        +'set pdlabel lhapdf\n'\
        +'set reweight_scale true\n'\
        +'set reweight_PDF true\n'\
        +'set no_parton_cut\n'\
        +'set jetalgo -1\n'\
        +'set jetradius 0.4\n'\
        +'set ickkw 0\n'\
        +'set ptgmin 150\n'\
        +'set etagamma 2.4\n'\
        +'set r0gamma 0.4\n'\
        +'set xn 1.0\n'\
        +'set epsgamma 1.0\n'\
        +'set lhaid '+lhapdfid+' #pdf '+modeType+'\n'\
        +'done\n\n'

        j=0
        while j <len(QCD):
            energyBinNr=0
            while energyBinNr<len(energy):
                Out='launch '+tag1+procType+tag2+'\n'\
                +'order='+QCD[j]+'\n'\
                +'fixed_order=ON\n'\
                +'set LHC '+str(int(energy[energyBinNr]))+'\n'\
                +'done\n\n'
                tempText=tempText+Out+"\n"
                energyBinNr=energyBinNr+1
            energy.insert(0, 1)
            j=j+1
        del energy[0:2]   
        oFile.write(tempText)
        oFile.close()
            
        Atom=Atom+1
    proc=proc+1
