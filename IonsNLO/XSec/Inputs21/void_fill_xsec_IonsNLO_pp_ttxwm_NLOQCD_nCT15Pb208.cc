// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxwm_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[0] = 1.19e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[1] = 0.0007516;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[2] = 0.005827;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[3] = 0.01709;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[4] = 0.0342;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[5] = 0.083;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[6] = 0.1134;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[7] = 0.1845;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[8] = 0.3123;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[9] = 0.3595;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[10] = 0.4104;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[11] = 0.6908;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[12] = 1.017;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[13] = 1.382;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[14] = 2.672;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[15] = 3.139;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[16] = 4.162;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[17] = 5.923;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[18] = 7.692;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[19] = 9.682;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[20] = 11.74;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[0] = 1.43514e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[1] = 0.0008417920000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[2] = 0.0063747380000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[3] = 0.018525560000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[4] = 0.036936000000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[5] = 0.089723;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[6] = 0.12269880000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[7] = 0.200736;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[8] = 0.34228080000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[9] = 0.394731;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[10] = 0.4518504;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[11] = 0.7681696;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[12] = 1.13904;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[13] = 1.5561319999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[14] = 3.0353920000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[15] = 3.5690429999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[16] = 4.740518;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[17] = 6.781835;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[18] = 8.830416000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[19] = 11.202074000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[20] = 13.677100000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[0] = 9.579499999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[1] = 0.0006493823999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[2] = 0.005168549;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[3] = 0.01534682;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[4] = 0.030951000000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[5] = 0.07561300000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[6] = 0.10342080000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[7] = 0.168264;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[8] = 0.2845053;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[9] = 0.3275045;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[10] = 0.3730536;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[11] = 0.6258648;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[12] = 0.9183509999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[13] = 1.2438;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[14] = 2.3914400000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[15] = 2.796849;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[16] = 3.670884;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[17] = 5.164856;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[18] = 6.638196;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[19] = 8.287792;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[20] = 9.99074;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[0] = 1.19e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[1] = 0.0007516;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[2] = 0.005827;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[3] = 0.01709;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[4] = 0.0342;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[5] = 0.083;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[6] = 0.1134;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[7] = 0.1845;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[8] = 0.3123;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[9] = 0.3595;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[10] = 0.4104;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[11] = 0.6908;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[12] = 1.017;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[13] = 1.382;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[14] = 2.672;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[15] = 3.139;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[16] = 4.162;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[17] = 5.923;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[18] = 7.692;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[19] = 9.682;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[20] = 11.74;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[0] = 1.6160200000000002e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[1] = 0.0008778687999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[2] = 0.006590337;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[3] = 0.019004080000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[4] = 0.0375858;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[5] = 0.089723;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[6] = 0.12179160000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[7] = 0.19612349999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[8] = 0.3282273;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[9] = 0.376756;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[10] = 0.42886799999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[11] = 0.7156688;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[12] = 1.049544;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[13] = 1.42346;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[14] = 2.760176;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[15] = 3.2488649999999994;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[16] = 4.32848;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[17] = 6.177689;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[18] = 8.068908;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[19] = 10.195146;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[20] = 12.40918;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[0] = 7.639800000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[1] = 0.0006253312;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[2] = 0.0050636629999999995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[3] = 0.01517592;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[4] = 0.030814200000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[5] = 0.07627700000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[6] = 0.10500839999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[7] = 0.17287650000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[8] = 0.2963727;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[9] = 0.342244;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[10] = 0.39193199999999995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[11] = 0.6659312;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[12] = 0.9844559999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[13] = 1.3405399999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[14] = 2.583824;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[15] = 3.0291349999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[16] = 3.99552;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[17] = 5.668311;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[18] = 7.315092;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[19] = 9.168854;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[20] = 11.07082;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[0] = 6.688e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[1] = 0.0004828;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[2] = 0.003894;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[3] = 0.01159;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[4] = 0.02342;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[5] = 0.05602;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[6] = 0.07584;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[7] = 0.1212;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[8] = 0.1964;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[9] = 0.2226;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[10] = 0.2514;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[11] = 0.3976;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[12] = 0.5529;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[13] = 0.7172;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[14] = 1.232;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[15] = 1.416;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[16] = 1.775;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[17] = 2.33;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[18] = 2.896;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[19] = 3.458;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[20] = 4.039;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[0] = 1.0319584e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[1] = 0.0006821964000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[2] = 0.005284158;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[3] = 0.01534516;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[4] = 0.030446;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[5] = 0.07103336;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[6] = 0.09525504000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[7] = 0.1499244;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[8] = 0.2388224;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[9] = 0.269346;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[10] = 0.302937;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[11] = 0.4723488;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[12] = 0.6623741999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[13] = 0.8670947999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[14] = 1.5190560000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[15] = 1.753008;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[16] = 2.216975;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[17] = 2.9381300000000006;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[18] = 3.683712;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[19] = 4.42624;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[20] = 5.198192999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[0] = 4.527776e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[1] = 0.0003538924;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[2] = 0.00295944;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[3] = 0.00899384;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[4] = 0.01847838;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[5] = 0.045152120000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[6] = 0.06165792;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[7] = 0.0998688;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[8] = 0.16419040000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[9] = 0.1867614;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[10] = 0.2119302;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[11] = 0.3403456;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[12] = 0.46720049999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[13] = 0.5995792;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[14] = 1.007776;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[15] = 1.1512079999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[16] = 1.428875;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[17] = 1.85468;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[18] = 2.284944;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[19] = 2.7076140000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[20] = 3.1423419999999997;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[0] = 6.688e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[1] = 0.0004828;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[2] = 0.003894;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[3] = 0.01159;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[4] = 0.02342;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[5] = 0.05602;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[6] = 0.07584;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[7] = 0.1212;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[8] = 0.1964;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[9] = 0.2226;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[10] = 0.2514;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[11] = 0.3976;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[12] = 0.5529;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[13] = 0.7172;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[14] = 1.232;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[15] = 1.416;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[16] = 1.775;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[17] = 2.33;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[18] = 2.896;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[19] = 3.458;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[20] = 4.039;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[0] = 9.16256e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[1] = 0.0005653588;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[2] = 0.004408008000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[3] = 0.012888080000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[4] = 0.02571516;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[5] = 0.06044558;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[6] = 0.08122464;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[7] = 0.128472;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[8] = 0.2058272;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[9] = 0.23261699999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[10] = 0.2622102;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[11] = 0.4131064;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[12] = 0.575016;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[13] = 0.7487568;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[14] = 1.3022239999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[15] = 1.502376;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[16] = 1.8939249999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[17] = 2.50708;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[18] = 3.1363679999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[19] = 3.7623040000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[20] = 4.414626999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[0] = 4.21344e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[1] = 0.0004002412;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[2] = 0.003379992;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[3] = 0.01029192;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[4] = 0.02112484;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[5] = 0.05159442;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[6] = 0.07045536000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[7] = 0.113928;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[8] = 0.1869728;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[9] = 0.212583;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[10] = 0.2405898;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[11] = 0.3820936;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[12] = 0.5307839999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[13] = 0.6856431999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[14] = 1.161776;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[15] = 1.329624;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[16] = 1.656075;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[17] = 2.1529200000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[18] = 2.655632;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[19] = 3.1536960000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[20] = 3.663373;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Pb208_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
