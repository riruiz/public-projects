// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxh_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[0] = 2.964e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[1] = 0.0003542;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[2] = 0.003329;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[3] = 0.01133;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[4] = 0.02564;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[5] = 0.07742;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[6] = 0.1158;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[7] = 0.2194;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[8] = 0.4465;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[9] = 0.5402;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[10] = 0.6433;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[11] = 1.301;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[12] = 2.185;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[13] = 3.252;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[14] = 7.503;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[15] = 9.201;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[16] = 13.2;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[17] = 20.47;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[18] = 27.42;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[19] = 35.76;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[20] = 45.39;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[0] = 3.4619519999999997e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[1] = 0.0003747436;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[2] = 0.003475476;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[3] = 0.01179453;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[4] = 0.026639959999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[5] = 0.0805168;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[6] = 0.1207794;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[7] = 0.22993120000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[8] = 0.469718;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[9] = 0.5693708000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[10] = 0.6761082999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[11] = 1.377759;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[12] = 2.309545;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[13] = 3.4503719999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[14] = 7.945677;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[15] = 9.753060000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[16] = 14.0448;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[17] = 22.128069999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[18] = 29.2023;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[19] = 37.97712;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[20] = 48.385740000000006;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[0] = 2.3919479999999995e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[1] = 0.0003127586;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[2] = 0.003009416;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[3] = 0.01033296;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[4] = 0.0234606;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[5] = 0.07083930000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[6] = 0.10595700000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[7] = 0.200751;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[8] = 0.408994;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[9] = 0.4948232;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[10] = 0.5905494;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[11] = 1.194318;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[12] = 2.01457;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[13] = 2.998344;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[14] = 6.97779;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[15] = 8.55693;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[16] = 12.2892;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[17] = 18.91428;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[18] = 25.637700000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[19] = 33.57864;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[20] = 42.57582;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[0] = 2.964e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[1] = 0.0003542;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[2] = 0.003329;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[3] = 0.01133;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[4] = 0.02564;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[5] = 0.07742;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[6] = 0.1158;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[7] = 0.2194;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[8] = 0.4465;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[9] = 0.5402;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[10] = 0.6433;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[11] = 1.301;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[12] = 2.185;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[13] = 3.252;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[14] = 7.503;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[15] = 9.201;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[16] = 13.2;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[17] = 20.47;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[18] = 27.42;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[19] = 35.76;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[20] = 45.39;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[0] = 4.285944e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[1] = 0.0004328324;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[2] = 0.003991471;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[3] = 0.01350536;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[4] = 0.03040904;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[5] = 0.09058139999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[6] = 0.1344438;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[7] = 0.251213;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[8] = 0.500973;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[9] = 0.6028632;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[10] = 0.7134197;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[11] = 1.410284;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[12] = 2.3292100000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[13] = 3.4211039999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[14] = 7.720586999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[15] = 9.440226000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[16] = 13.543199999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[17] = 20.940809999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[18] = 28.46196;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[19] = 37.40496;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[20] = 47.70489;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[0] = 1.642056e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[1] = 0.0002755676;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[2] = 0.002666529;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[3] = 0.00915464;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[4] = 0.020870959999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[5] = 0.0642586;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[6] = 0.0971562;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[7] = 0.187587;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[8] = 0.392027;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[9] = 0.47753680000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[10] = 0.5731803;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[11] = 1.191716;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[12] = 2.04079;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[13] = 3.082896;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[14] = 7.285413;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[15] = 8.961774;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[16] = 12.8568;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[17] = 19.99919;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[18] = 26.378040000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[19] = 34.11503999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[20] = 43.075109999999995;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[0] = 1.83e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[1] = 0.0002609;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[2] = 0.002585;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[3] = 0.008853;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[4] = 0.02017;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[5] = 0.05998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[6] = 0.08901;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[7] = 0.1671;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[8] = 0.3358;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[9] = 0.405;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[10] = 0.4837;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[11] = 0.9607;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[12] = 1.592;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[13] = 2.369;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[14] = 5.369;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[15] = 6.684;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[16] = 9.421;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[17] = 14.05;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[18] = 19.34;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[19] = 25.13;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[20] = 31.39;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[0] = 2.91153e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[1] = 0.0003751742;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[2] = 0.0035879799999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[3] = 0.012057786000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[4] = 0.02714882;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[5] = 0.07959345999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[6] = 0.11740419;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[7] = 0.21839969999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[8] = 0.4338536;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[9] = 0.52164;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[10] = 0.6210708;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[11] = 1.2162462;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[12] = 1.991592;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[13] = 2.935191;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[14] = 6.49649;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[15] = 8.047536;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[16] = 11.220411;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[17] = 16.6071;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[18] = 22.93724;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[19] = 30.030350000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[20] = 37.73078;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[0] = 1.20597e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[1] = 0.00018810890000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[2] = 0.00192324;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[3] = 0.006692868;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[4] = 0.01540988;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[5] = 0.04636454;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[6] = 0.06916077000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[7] = 0.1308393;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[8] = 0.265282;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[9] = 0.32076000000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[10] = 0.38405780000000006;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[11] = 0.7724028000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[12] = 1.292704;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[13] = 1.9402110000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[14] = 4.483115;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[15] = 5.607876;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[16] = 7.9795869999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[17] = 12.0268;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[18] = 16.40032;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[19] = 21.13433;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[20] = 26.21065;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[0] = 1.83e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[1] = 0.0002609;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[2] = 0.002585;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[3] = 0.008853;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[4] = 0.02017;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[5] = 0.05998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[6] = 0.08901;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[7] = 0.1671;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[8] = 0.3358;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[9] = 0.405;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[10] = 0.4837;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[11] = 0.9607;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[12] = 1.592;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[13] = 2.369;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[14] = 5.369;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[15] = 6.684;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[16] = 9.421;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[17] = 14.05;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[18] = 19.34;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[19] = 25.13;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[20] = 31.39;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[0] = 2.6315400000000003e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[1] = 0.00031594990000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[2] = 0.003063225;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[3] = 0.010419981;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[4] = 0.023619070000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[5] = 0.06951682000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[6] = 0.10245051000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[7] = 0.1899927;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[8] = 0.3750886;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[9] = 0.44995500000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[10] = 0.5349722000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[11] = 1.0394774;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[12] = 1.6938880000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[13] = 2.4874500000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[14] = 5.5193319999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[15] = 6.8577840000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[16] = 9.675366999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[17] = 14.527700000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[18] = 20.171619999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[19] = 26.3865;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[20] = 33.11645;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[0] = 1.0284600000000002e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[1] = 0.00020585009999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[2] = 0.002106775;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[3] = 0.0072860189999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[4] = 0.01672093;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[5] = 0.05044318;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[6] = 0.07556949;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[7] = 0.1442073;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[8] = 0.2965114;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[9] = 0.360045;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[10] = 0.43242780000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[11] = 0.8819226;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[12] = 1.4901119999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[13] = 2.25055;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[14] = 5.218668;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[15] = 6.510216;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[16] = 9.166633;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[17] = 13.5723;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[18] = 18.50838;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[19] = 23.873499999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[20] = 29.66355;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Pb208_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
