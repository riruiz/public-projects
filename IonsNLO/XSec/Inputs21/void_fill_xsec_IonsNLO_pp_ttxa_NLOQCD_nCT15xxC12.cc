// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxa_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[0] = 1.043e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[1] = 4.577e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[2] = 0.0006555;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[3] = 0.002767;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[4] = 0.007215;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[5] = 0.02523;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[6] = 0.03939;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[7] = 0.08017;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[8] = 0.1725;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[9] = 0.2109;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[10] = 0.2577;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[11] = 0.5319;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[12] = 0.9107;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[13] = 1.391;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[14] = 3.245;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[15] = 4.023;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[16] = 5.788;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[17] = 8.735;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[18] = 11.78;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[19] = 15.84;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[20] = 19.89;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[0] = 1.3882329999999999e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[1] = 5.387129e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[2] = 0.0007492365000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[3] = 0.003140545;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[4] = 0.008174595;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[5] = 0.02835852;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[6] = 0.044156190000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[7] = 0.08963006000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[8] = 0.192165;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[9] = 0.23452080000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[10] = 0.2875932;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[11] = 0.5882814000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[12] = 1.00177;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[13] = 1.5301000000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[14] = 3.540295;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[15] = 4.377024;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[16] = 6.303132;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[17] = 9.48621;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[18] = 12.734179999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[19] = 17.13888;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[20] = 21.52098;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[0] = 7.686909999999999e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[1] = 3.7668709999999995e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[2] = 0.0005552085;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[3] = 0.002365785;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[4] = 0.006197685;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[5] = 0.021899639999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[6] = 0.0342693;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[7] = 0.07006858;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[8] = 0.1516275;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[9] = 0.1860138;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[10] = 0.22677599999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[11] = 0.47285910000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[12] = 0.8150765;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[13] = 1.247727;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[14] = 2.9432150000000004;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[15] = 3.66093;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[16] = 5.278656000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[17] = 8.00126;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[18] = 10.86116;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[19] = 14.620320000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[20] = 18.37836;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[0] = 1.043e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[1] = 4.577e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[2] = 0.0006555;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[3] = 0.002767;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[4] = 0.007215;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[5] = 0.02523;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[6] = 0.03939;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[7] = 0.08017;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[8] = 0.1725;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[9] = 0.2109;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[10] = 0.2577;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[11] = 0.5319;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[12] = 0.9107;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[13] = 1.391;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[14] = 3.245;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[15] = 4.023;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[16] = 5.788;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[17] = 8.735;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[18] = 11.78;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[19] = 15.84;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[20] = 19.89;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[0] = 1.247428e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[1] = 5.0758929999999995e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[2] = 0.0007171170000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[3] = 0.003010496;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[4] = 0.007842705;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[5] = 0.02732409;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[6] = 0.04258059;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[7] = 0.08626292;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[8] = 0.18423;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[9] = 0.22481940000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[10] = 0.2741928;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[11] = 0.5606226000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[12] = 0.9516814999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[13] = 1.443858;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[14] = 3.319635;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[15] = 4.099436999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[16] = 5.869032000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[17] = 8.831084999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[18] = 11.933139999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[19] = 16.09344;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[20] = 20.267909999999997;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[0] = 8.38572e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[1] = 4.078107e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[2] = 0.000593883;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[3] = 0.002523504;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[4] = 0.006587295;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[5] = 0.02313591;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[6] = 0.03619941;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[7] = 0.07407708;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[8] = 0.16076999999999997;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[9] = 0.19698059999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[10] = 0.24120719999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[11] = 0.5031774;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[12] = 0.8697185;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[13] = 1.338142;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[14] = 3.170365;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[15] = 3.946563;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[16] = 5.706968;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[17] = 8.638914999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[18] = 11.626859999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[19] = 15.58656;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[20] = 19.51209;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[0] = 5.027e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[1] = 2.683e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[2] = 0.000398;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[3] = 0.001696;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[4] = 0.004461;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[5] = 0.01556;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[6] = 0.02448;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[7] = 0.04957;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[8] = 0.1073;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[9] = 0.131;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[10] = 0.1572;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[11] = 0.3396;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[12] = 0.5783;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[13] = 0.877;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[14] = 2.055;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[15] = 2.524;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[16] = 3.667;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[17] = 5.547;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[18] = 7.663;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[19] = 9.964;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[20] = 12.34;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[0] = 8.425252e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[1] = 3.97084e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[2] = 0.000566354;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[3] = 0.002364224;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[4] = 0.006133875;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[5] = 0.021021559999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[6] = 0.03285216;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[7] = 0.06582895999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[8] = 0.14056300000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[9] = 0.170955;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[10] = 0.2045172;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[11] = 0.4353672;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[12] = 0.7315495000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[13] = 1.098881;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[14] = 2.51532;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[15] = 3.071708;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[16] = 4.415068;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[17] = 6.600929999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[18] = 9.027014;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[19] = 11.717664;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[20] = 14.598220000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[0] = 3.172037e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[1] = 1.883466e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[2] = 0.000288948;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[3] = 0.001253344;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[4] = 0.0033368279999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[5] = 0.011810039999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[6] = 0.01867824;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[7] = 0.038168900000000006;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[8] = 0.08358670000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[9] = 0.102442;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[10] = 0.12324480000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[11] = 0.2696424;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[12] = 0.46437490000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[13] = 0.7103700000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[14] = 1.6974300000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[15] = 2.09492;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[16] = 3.0729459999999995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[17] = 4.698308999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[18] = 6.551865;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[19] = 8.529184;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[20] = 10.488999999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[0] = 5.027e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[1] = 2.683e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[2] = 0.000398;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[3] = 0.001696;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[4] = 0.004461;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[5] = 0.01556;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[6] = 0.02448;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[7] = 0.04957;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[8] = 0.1073;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[9] = 0.131;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[10] = 0.1572;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[11] = 0.3396;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[12] = 0.5783;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[13] = 0.877;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[14] = 2.055;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[15] = 2.524;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[16] = 3.667;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[17] = 5.547;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[18] = 7.663;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[19] = 9.964;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[20] = 12.34;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[0] = 6.017319e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[1] = 2.9727640000000003e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[2] = 0.000434218;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[3] = 0.0018418560000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[4] = 0.004835724;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[5] = 0.01682036;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[6] = 0.0264384;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[7] = 0.05333732000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[8] = 0.1147037;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[9] = 0.139777;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[10] = 0.167418;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[11] = 0.358278;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[12] = 0.6054801;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[13] = 0.911203;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[14] = 2.1043200000000004;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[15] = 2.57448;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[16] = 3.7220049999999993;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[17] = 5.608016999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[18] = 7.762619;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[19] = 10.133388;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[20] = 12.599139999999998;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[0] = 4.036681e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[1] = 2.393236e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[2] = 0.00036178200000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[3] = 0.001550144;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[4] = 0.004086276;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[5] = 0.01429964;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[6] = 0.0225216;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[7] = 0.045802680000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[8] = 0.09989630000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[9] = 0.12222300000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[10] = 0.146982;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[11] = 0.320922;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[12] = 0.5511199;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[13] = 0.842797;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[14] = 2.00568;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[15] = 2.47352;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[16] = 3.611995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[17] = 5.485983;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[18] = 7.563381000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[19] = 9.794612;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[20] = 12.08086;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15xxC12_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
