// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxwp_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[0] = 3.053e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[1] = 0.001231;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[2] = 0.00863;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[3] = 0.02398;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[4] = 0.04675;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[5] = 0.1095;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[6] = 0.1486;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[7] = 0.2366;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[8] = 0.3909;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[9] = 0.4492;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[10] = 0.5089;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[11] = 0.8426;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[12] = 1.217;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[13] = 1.638;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[14] = 3.096;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[15] = 3.621;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[16] = 4.796;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[17] = 6.696;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[18] = 8.713;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[19] = 10.9;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[20] = 13.21;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[0] = 3.712448e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[1] = 0.0013897990000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[2] = 0.009544780000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[3] = 0.026234120000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[4] = 0.0511445;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[5] = 0.11979300000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[6] = 0.16316280000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[7] = 0.2604966;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[8] = 0.4327263;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[9] = 0.4995104;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[10] = 0.5658968000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[11] = 0.9462398;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[12] = 1.373993;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[13] = 1.85913;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[14] = 3.5480159999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[15] = 4.149666;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[16] = 5.520196;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[17] = 7.747272;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[18] = 10.159358;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[19] = 12.8184;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[20] = 15.627430000000002;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[0] = 2.414923e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[1] = 0.0010475810000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[2] = 0.007533990000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[3] = 0.021222300000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[4] = 0.04156075;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[5] = 0.0980025;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[6] = 0.1331456;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[7] = 0.2122302;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[8] = 0.35063730000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[9] = 0.402034;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[10] = 0.4559744;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[11] = 0.7515992;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[12] = 1.0831300000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[13] = 1.4545439999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[14] = 2.730672;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[15] = 3.175617;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[16] = 4.167724;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[17] = 5.751863999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[18] = 7.414762999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[19] = 9.1996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[20] = 11.08319;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[0] = 3.053e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[1] = 0.001231;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[2] = 0.00863;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[3] = 0.02398;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[4] = 0.04675;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[5] = 0.1095;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[6] = 0.1486;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[7] = 0.2366;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[8] = 0.3909;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[9] = 0.4492;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[10] = 0.5089;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[11] = 0.8426;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[12] = 1.217;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[13] = 1.638;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[14] = 3.096;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[15] = 3.621;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[16] = 4.796;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[17] = 6.696;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[18] = 8.713;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[19] = 10.9;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[20] = 13.21;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[0] = 4.988602e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[1] = 0.0015079750000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[2] = 0.009777790000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[3] = 0.026282080000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[4] = 0.05034975;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[5] = 0.1157415;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[6] = 0.15632720000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[7] = 0.24701040000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[8] = 0.4053633;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[9] = 0.4653712;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[10] = 0.5262026000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[11] = 0.8661928;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[12] = 1.247425;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[13] = 1.6756739999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[14] = 3.1548239999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[15] = 3.686178;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[16] = 4.8775319999999995;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[17] = 6.803135999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[18] = 8.852407999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[19] = 11.0744;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[20] = 13.421360000000002;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[0] = 1.8775949999999999e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[1] = 0.001013113;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[2] = 0.00760303;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[3] = 0.021773840000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[4] = 0.04315025;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[5] = 0.102711;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[6] = 0.1401298;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[7] = 0.22477;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[8] = 0.3737004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[9] = 0.43078279999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[10] = 0.488544;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[11] = 0.8139516;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[12] = 1.18049;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[13] = 1.5937739999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[14] = 3.027888;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[15] = 3.544959;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[16] = 4.70008;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[17] = 6.5754719999999995;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[18] = 8.556166;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[19] = 10.7038;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[20] = 12.97222;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[0] = 1.72e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[1] = 0.0008;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[2] = 0.005807;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[3] = 0.01656;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[4] = 0.03233;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[5] = 0.07421;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[6] = 0.09923;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[7] = 0.1551;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[8] = 0.2495;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[9] = 0.2811;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[10] = 0.3145;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[11] = 0.4951;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[12] = 0.6778;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[13] = 0.8736;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[14] = 1.497;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[15] = 1.697;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[16] = 2.116;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[17] = 2.774;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[18] = 3.444;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[19] = 4.109;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[20] = 4.813;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[0] = 2.6746e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[1] = 0.0011352;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[2] = 0.007932362;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[3] = 0.022107599999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[4] = 0.04241696;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[5] = 0.09521142999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[6] = 0.12622056;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[7] = 0.19465049999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[8] = 0.3081325;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[9] = 0.34575300000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[10] = 0.3852625;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[11] = 0.5970905999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[12] = 0.816749;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[13] = 1.0614240000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[14] = 1.850292;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[15] = 2.1076740000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[16] = 2.649232;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[17] = 3.5035619999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[18] = 4.384212000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[19] = 5.263629000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[20] = 6.194330999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[0] = 1.1489599999999999e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[1] = 0.0005808;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[2] = 0.004361057;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[3] = 0.012701519999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[4] = 0.02518507;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[5] = 0.05899695;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[6] = 0.07948323;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[7] = 0.125631;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[8] = 0.205089;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[9] = 0.2319075;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[10] = 0.260406;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[11] = 0.41538889999999995;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[12] = 0.5686741999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[13] = 0.7268352;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[14] = 1.2200550000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[15] = 1.3762670000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[16] = 1.701264;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[17] = 2.208104;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[18] = 2.717316;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[19] = 3.2214560000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[20] = 3.749327;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[0] = 1.72e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[1] = 0.0008;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[2] = 0.005807;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[3] = 0.01656;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[4] = 0.03233;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[5] = 0.07421;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[6] = 0.09923;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[7] = 0.1551;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[8] = 0.2495;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[9] = 0.2811;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[10] = 0.3145;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[11] = 0.4951;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[12] = 0.6778;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[13] = 0.8736;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[14] = 1.497;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[15] = 1.697;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[16] = 2.116;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[17] = 2.774;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[18] = 3.444;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[19] = 4.109;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[20] = 4.813;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[0] = 2.81564e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[1] = 0.0009832;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[2] = 0.006608365999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[3] = 0.018216;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[4] = 0.03494873;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[5] = 0.07873680999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[6] = 0.10478688;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[7] = 0.16269989999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[8] = 0.2602285;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[9] = 0.29262509999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[10] = 0.32708000000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[11] = 0.5124285;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[12] = 0.7001673999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[13] = 0.9006816;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[14] = 1.5389160000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[15] = 1.7445160000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[16] = 2.173132;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[17] = 2.8488979999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[18] = 3.533544;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[19] = 4.215834;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[20] = 4.942950999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[0] = 1.06296e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[1] = 0.0006576;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[2] = 0.005098546;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[3] = 0.0149868;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[4] = 0.029711269999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[5] = 0.06931214;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[6] = 0.09307773999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[7] = 0.14625929999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[8] = 0.2367755;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[9] = 0.267045;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[10] = 0.299404;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[11] = 0.4738107;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[12] = 0.6506879999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[13] = 0.8404032;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[14] = 1.4446050000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[15] = 1.639302;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[16] = 2.046172;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[17] = 2.685232;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[18] = 3.337236;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[19] = 3.981621;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[20] = 4.663797;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_CT18nloxH1_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
