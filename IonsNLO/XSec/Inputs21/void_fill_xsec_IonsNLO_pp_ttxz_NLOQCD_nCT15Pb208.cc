// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxz_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[0] = 1.055e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[1] = 0.0007378;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[2] = 0.006355;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[3] = 0.02077;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[4] = 0.04619;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[5] = 0.1352;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[6] = 0.2029;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[7] = 0.3845;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[8] = 0.7669;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[9] = 0.9404;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[10] = 1.111;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[11] = 2.245;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[12] = 3.744;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[13] = 5.584;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[14] = 12.92;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[15] = 15.97;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[16] = 22.53;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[17] = 33.83;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[18] = 47.83;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[19] = 61.65;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[20] = 77.32;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[0] = 1.2733850000000002e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[1] = 0.0008233848000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[2] = 0.006946015000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[3] = 0.022556220000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[4] = 0.05011615;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[5] = 0.146016;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[6] = 0.2201465;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[7] = 0.41910500000000006;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[8] = 0.8328534000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[9] = 1.0240955999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[10] = 1.207657;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[11] = 2.4425600000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[12] = 4.0697280000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[13] = 6.053056;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[14] = 13.953600000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[15] = 17.23163;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[16] = 24.174690000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[17] = 36.26576;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[18] = 51.51291;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[19] = 66.15044999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[20] = 82.80971999999998;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[0] = 8.440000000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[1] = 0.0006359836;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[2] = 0.005598755;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[3] = 0.01846453;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[4] = 0.041201480000000006;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[5] = 0.12127439999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[6] = 0.1815955;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[7] = 0.34412750000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[8] = 0.6894431000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[9] = 0.8444792;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[10] = 0.998789;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[11] = 2.0249900000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[12] = 3.38832;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[13] = 5.075856;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[14] = 11.83472;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[15] = 14.66046;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[16] = 20.81772;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[17] = 31.36041;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[18] = 44.29058;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[19] = 57.33449999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[20] = 72.13956;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[0] = 1.055e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[1] = 0.0007378;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[2] = 0.006355;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[3] = 0.02077;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[4] = 0.04619;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[5] = 0.1352;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[6] = 0.2029;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[7] = 0.3845;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[8] = 0.7669;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[9] = 0.9404;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[10] = 1.111;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[11] = 2.245;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[12] = 3.744;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[13] = 5.584;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[14] = 12.92;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[15] = 15.97;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[16] = 22.53;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[17] = 33.83;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[18] = 47.83;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[19] = 61.65;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[20] = 77.32;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[0] = 1.4326900000000002e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[1] = 0.0008801954000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[2] = 0.007454415000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[3] = 0.02423859;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[4] = 0.053718970000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[5] = 0.15561519999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[6] = 0.23292920000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[7] = 0.4364075;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[8] = 0.8566273;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[9] = 1.0447844;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[10] = 1.228766;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[11] = 2.4313350000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[12] = 3.9911040000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[13] = 5.885536;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[14] = 13.29468;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[15] = 16.40119;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[16] = 23.093249999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[17] = 34.8449;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[18] = 49.45622;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[19] = 64.30095;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[20] = 81.18599999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[0] = 6.7731e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[1] = 0.0005954046;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[2] = 0.005255585;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[3] = 0.01730141;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[4] = 0.03866103;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[5] = 0.11478479999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[6] = 0.1728708;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[7] = 0.3325925;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[8] = 0.6771727000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[9] = 0.8360156;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[10] = 0.9932340000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[11] = 2.058665;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[12] = 3.496896;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[13] = 5.282463999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[14] = 12.54532;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[15] = 15.53881;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[16] = 21.96675;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[17] = 32.815099999999994;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[18] = 46.203779999999995;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[19] = 58.99905;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[20] = 73.454;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[0] = 5.961e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[1] = 0.000481;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[2] = 0.00429;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[3] = 0.01425;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[4] = 0.03191;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[5] = 0.09309;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[6] = 0.1384;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[7] = 0.2577;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[8] = 0.5173;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[9] = 0.623;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[10] = 0.7444;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[11] = 1.489;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[12] = 2.48;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[13] = 3.711;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[14] = 8.588;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[15] = 10.54;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[16] = 15.03;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[17] = 22.48;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[18] = 31.35;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[19] = 40.53;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[20] = 50.71;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[0] = 9.311082000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[1] = 0.000686387;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[2] = 0.0059202000000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[3] = 0.01930875;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[4] = 0.0427594;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[5] = 0.12306498000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[6] = 0.1821344;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[7] = 0.33629849999999994;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[8] = 0.6683516;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[9] = 0.803047;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[10] = 0.9565539999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[11] = 1.888052;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[12] = 3.1123999999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[13] = 4.616484;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[14] = 10.451596;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[15] = 12.74286;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[16] = 17.99091;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[17] = 26.59384;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[18] = 37.0557;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[19] = 48.27123;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[20] = 60.75058;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[0] = 3.9938699999999995e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[1] = 0.00034920599999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[2] = 0.00320892;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[3] = 0.010830000000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[4] = 0.02450688;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[5] = 0.07223784000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[6] = 0.1078136;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[7] = 0.2022945;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[8] = 0.4091843;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[9] = 0.493416;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[10] = 0.5910536;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[11] = 1.1956670000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[12] = 2.0088;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[13] = 3.028176;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[14] = 7.136627999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[15] = 8.81144;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[16] = 12.67029;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[17] = 19.15296;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[18] = 26.7102;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[19] = 34.24785;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[20] = 42.49498;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[0] = 5.961e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[1] = 0.000481;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[2] = 0.00429;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[3] = 0.01425;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[4] = 0.03191;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[5] = 0.09309;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[6] = 0.1384;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[7] = 0.2577;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[8] = 0.5173;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[9] = 0.623;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[10] = 0.7444;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[11] = 1.489;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[12] = 2.48;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[13] = 3.711;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[14] = 8.588;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[15] = 10.54;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[16] = 15.03;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[17] = 22.48;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[18] = 31.35;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[19] = 40.53;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[20] = 50.71;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[0] = 8.130803999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[1] = 0.00057239;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[2] = 0.00500214;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[3] = 0.016515750000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[4] = 0.03685605;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[5] = 0.10677423000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[6] = 0.1580528;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[7] = 0.2917164;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[8] = 0.5773068;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[9] = 0.692153;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[10] = 0.8233064;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[11] = 1.6155650000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[12] = 2.6486400000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[13] = 3.9151049999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[14] = 8.854227999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[15] = 10.824579999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[16] = 15.405749999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[17] = 23.154400000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[18] = 32.5413;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[19] = 42.35385;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[20] = 53.296209999999995;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[0] = 3.791196e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[1] = 0.00038961000000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[2] = 0.00357786;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[3] = 0.01198425;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[4] = 0.02696395;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[5] = 0.07940577;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[6] = 0.1187472;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[7] = 0.22368359999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[8] = 0.4572932;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[9] = 0.553847;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[10] = 0.6654936;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[11] = 1.362435;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[12] = 2.3113599999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[13] = 3.5068949999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[14] = 8.321772;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[15] = 10.255419999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[16] = 14.65425;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[17] = 21.8056;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[18] = 30.1587;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[19] = 38.70615;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[20] = 48.12379;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Pb208_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
