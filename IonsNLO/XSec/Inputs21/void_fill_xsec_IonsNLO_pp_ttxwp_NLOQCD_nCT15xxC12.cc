// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxwp_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[0] = 1.514e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[1] = 0.000753;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[2] = 0.005565;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[3] = 0.01611;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[4] = 0.0321;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[5] = 0.0784;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[6] = 0.1062;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[7] = 0.1727;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[8] = 0.2909;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[9] = 0.3361;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[10] = 0.383;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[11] = 0.6455;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[12] = 0.9541;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[13] = 1.296;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[14] = 2.532;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[15] = 2.979;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[16] = 3.955;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[17] = 5.569;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[18] = 7.334;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[19] = 9.228;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[20] = 11.12;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[0] = 1.8168e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[1] = 0.000842607;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[2] = 0.00608811;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[3] = 0.017447129999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[4] = 0.0347643;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[5] = 0.0852992;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[6] = 0.11533320000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[7] = 0.1884157;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[8] = 0.3182446;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[9] = 0.3690378;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[10] = 0.42130000000000006;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[11] = 0.7165050000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[12] = 1.0666838;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[13] = 1.4541119999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[14] = 2.8738200000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[15] = 3.3841440000000005;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[16] = 4.500789999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[17] = 6.348660000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[18] = 8.419432;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[19] = 10.667568;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[20] = 12.943679999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[0] = 1.2233120000000001e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[1] = 0.0006520979999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[2] = 0.00494172;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[3] = 0.014499;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[4] = 0.029018399999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[5] = 0.0711088;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[6] = 0.096642;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[7] = 0.157157;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[8] = 0.2653008;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[9] = 0.30585100000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[10] = 0.34853;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[11] = 0.5854685;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[12] = 0.8625064;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[13] = 1.168992;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[14] = 2.26614;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[15] = 2.6542890000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[16] = 3.492265;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[17] = 4.861737;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[18] = 6.336576;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[19] = 7.917624;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[20] = 9.47424;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[0] = 1.514e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[1] = 0.000753;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[2] = 0.005565;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[3] = 0.01611;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[4] = 0.0321;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[5] = 0.0784;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[6] = 0.1062;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[7] = 0.1727;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[8] = 0.2909;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[9] = 0.3361;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[10] = 0.383;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[11] = 0.6455;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[12] = 0.9541;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[13] = 1.296;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[14] = 2.532;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[15] = 2.979;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[16] = 3.955;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[17] = 5.569;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[18] = 7.334;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[19] = 9.228;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[20] = 11.12;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[0] = 1.739586e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[1] = 0.000822276;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[2] = 0.0059489849999999995;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[3] = 0.01699605;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[4] = 0.0335766;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[5] = 0.0812224;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[6] = 0.1097046;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[7] = 0.1775356;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[8] = 0.2975907;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[9] = 0.3434942;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[10] = 0.391043;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[11] = 0.6564734999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[12] = 0.9684114999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[13] = 1.314144;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[14] = 2.567448;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[15] = 3.023685;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[16] = 4.022234999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[17] = 5.68038;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[18] = 7.502681999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[19] = 9.458699999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[20] = 11.43136;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[0] = 1.288414e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[1] = 0.000683724;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[2] = 0.005181015;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[3] = 0.015223949999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[4] = 0.030623399999999995;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[5] = 0.0755776;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[6] = 0.1026954;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[7] = 0.1678644;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[8] = 0.2842093;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[9] = 0.3287058;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[10] = 0.374957;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[11] = 0.6345265;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[12] = 0.9397884999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[13] = 1.277856;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[14] = 2.496552;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[15] = 2.9343150000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[16] = 3.887765;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[17] = 5.4576199999999995;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[18] = 7.165317999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[19] = 8.9973;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[20] = 10.808639999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[0] = 8.541e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[1] = 0.0004856;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[2] = 0.003738;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[3] = 0.01094;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[4] = 0.0218;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[5] = 0.05223;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[6] = 0.07058;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[7] = 0.112;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[8] = 0.1826;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[9] = 0.2068;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[10] = 0.233;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[11] = 0.373;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[12] = 0.5219;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[13] = 0.6749;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[14] = 1.172;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[15] = 1.343;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[16] = 1.7;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[17] = 2.235;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[18] = 2.781;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[19] = 3.341;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[20] = 3.903;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[0] = 1.3118976e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[1] = 0.0006827536;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[2] = 0.005053776;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[3] = 0.0144408;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[4] = 0.028274599999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[5] = 0.06612318;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[6] = 0.08850732;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[7] = 0.138432;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[8] = 0.2220416;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[9] = 0.2504348;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[10] = 0.280998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[11] = 0.44312399999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[12] = 0.6241924;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[13] = 0.8146043000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[14] = 1.44156;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[15] = 1.659948;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[16] = 2.1182;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[17] = 2.8093950000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[18] = 3.523527;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[19] = 4.259775;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[20] = 5.003646;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[0] = 5.80788e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[1] = 0.00035740159999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[2] = 0.0028483560000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[3] = 0.008511320000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[4] = 0.017222;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[5] = 0.04214961;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[6] = 0.05745212;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[7] = 0.092288;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[8] = 0.15265360000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[9] = 0.173712;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[10] = 0.196419;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[11] = 0.318915;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[12] = 0.44204930000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[13] = 0.5655662;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[14] = 0.96104;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[15] = 1.095888;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[16] = 1.3736000000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[17] = 1.788;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[18] = 2.2053330000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[19] = 2.6293670000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[20] = 3.052146;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[0] = 8.541e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[1] = 0.0004856;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[2] = 0.003738;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[3] = 0.01094;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[4] = 0.0218;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[5] = 0.05223;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[6] = 0.07058;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[7] = 0.112;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[8] = 0.1826;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[9] = 0.2068;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[10] = 0.233;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[11] = 0.373;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[12] = 0.5219;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[13] = 0.6749;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[14] = 1.172;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[15] = 1.343;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[16] = 1.7;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[17] = 2.235;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[18] = 2.781;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[19] = 3.341;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[20] = 3.903;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[0] = 9.822149999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[1] = 0.0005302752;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[2] = 0.00399966;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[3] = 0.011552640000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[4] = 0.022824599999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[5] = 0.054110280000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[6] = 0.07283856000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[7] = 0.11502399999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[8] = 0.1867998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[9] = 0.21134960000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[10] = 0.237893;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[11] = 0.38046;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[12] = 0.5328598999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[13] = 0.6904227;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[14] = 1.20716;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[15] = 1.385976;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[16] = 1.7612;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[17] = 2.3266349999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[18] = 2.906145;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[19] = 3.5013680000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[20] = 4.102053;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[0] = 7.25985e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[1] = 0.0004409248;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[2] = 0.0034763399999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[3] = 0.010327359999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[4] = 0.0207754;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[5] = 0.050349719999999994;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[6] = 0.06832144;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[7] = 0.108976;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[8] = 0.1784002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[9] = 0.2022504;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[10] = 0.228107;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[11] = 0.36554;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[12] = 0.5109401;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[13] = 0.6593773;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[14] = 1.1368399999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[15] = 1.3000239999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[16] = 1.6387999999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[17] = 2.1433649999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[18] = 2.655855;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[19] = 3.180632;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[20] = 3.703947;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15xxC12_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
