// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxwp_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[0] = 1.119e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[1] = 0.0005875;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[2] = 0.004449;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[3] = 0.01313;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[4] = 0.02662;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[5] = 0.06607;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[6] = 0.09098;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[7] = 0.1503;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[8] = 0.2594;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[9] = 0.3001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[10] = 0.3433;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[11] = 0.5886;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[12] = 0.8775;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[13] = 1.203;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[14] = 2.366;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[15] = 2.778;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[16] = 3.749;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[17] = 5.308;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[18] = 7.041;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[19] = 8.78;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[20] = 10.63;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[0] = 1.350633e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[1] = 0.0006568250000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[2] = 0.004862757;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[3] = 0.01423292;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[4] = 0.02877622;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[5] = 0.07148774000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[6] = 0.09853134000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[7] = 0.1635264;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[8] = 0.28456180000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[9] = 0.3298099;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[10] = 0.3783166;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[11] = 0.6545232000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[12] = 0.9828;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[13] = 1.3545779999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[14] = 2.6877760000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[15] = 3.15303;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[16] = 4.277609;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[17] = 6.067044;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[18] = 8.090109;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[19] = 10.15846;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[20] = 12.38395;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[0] = 8.99676e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[1] = 0.0005081875000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[2] = 0.003946263;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[3] = 0.01180387;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[4] = 0.0240911;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[5] = 0.0601237;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[6] = 0.08297376000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[7] = 0.1370736;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[8] = 0.23631340000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[9] = 0.273091;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[10] = 0.3120597;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[11] = 0.5332716000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[12] = 0.7923825;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[13] = 1.0827;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[14] = 2.11757;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[15] = 2.4751980000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[16] = 3.3066180000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[17] = 4.628576;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[18] = 6.076383;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[19] = 7.51568;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[20] = 9.0355;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[0] = 1.119e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[1] = 0.0005875;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[2] = 0.004449;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[3] = 0.01313;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[4] = 0.02662;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[5] = 0.06607;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[6] = 0.09098;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[7] = 0.1503;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[8] = 0.2594;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[9] = 0.3001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[10] = 0.3433;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[11] = 0.5886;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[12] = 0.8775;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[13] = 1.203;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[14] = 2.366;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[15] = 2.778;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[16] = 3.749;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[17] = 5.308;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[18] = 7.041;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[19] = 8.78;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[20] = 10.63;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[0] = 1.4535809999999998e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[1] = 0.0006832625000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[2] = 0.005040717;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[3] = 0.014666209999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[4] = 0.0294151;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[5] = 0.07195023;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[6] = 0.09844036000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[7] = 0.16097129999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[8] = 0.27444520000000006;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[9] = 0.3166055;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[10] = 0.3611516;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[11] = 0.612144;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[12] = 0.9064574999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[13] = 1.23909;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[14] = 2.43698;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[15] = 2.869674;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[16] = 3.8802149999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[17] = 5.525627999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[18] = 7.357845;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[19] = 9.22778;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[20] = 11.225280000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[0] = 7.844190000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[1] = 0.0004917375;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[2] = 0.003857283;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[3] = 0.01159379;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[4] = 0.023824900000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[5] = 0.060189770000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[6] = 0.08351964;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[7] = 0.1396287;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[8] = 0.2443548;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[9] = 0.28359449999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[10] = 0.32544839999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[11] = 0.565056;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[12] = 0.8485425;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[13] = 1.1669100000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[14] = 2.29502;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[15] = 2.6863259999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[16] = 3.617785;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[17] = 5.0903719999999995;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[18] = 6.724155;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[19] = 8.33222;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[20] = 10.03472;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[0] = 6.287e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[1] = 0.0003761;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[2] = 0.002986;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[3] = 0.008931;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[4] = 0.01811;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[5] = 0.04424;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[6] = 0.06062;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[7] = 0.0981;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[8] = 0.1633;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[9] = 0.1862;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[10] = 0.2104;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[11] = 0.3371;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[12] = 0.478;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[13] = 0.6236;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[14] = 1.099;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[15] = 1.256;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[16] = 1.594;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[17] = 2.114;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[18] = 2.635;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[19] = 3.157;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[20] = 3.729;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[0] = 9.719702e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[1] = 0.0005299249;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[2] = 0.00404603;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[3] = 0.011806782;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[4] = 0.02352489;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[5] = 0.056096320000000005;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[6] = 0.07613872000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[7] = 0.12134970000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[8] = 0.1985728;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[9] = 0.225302;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[10] = 0.25353200000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[11] = 0.4004748;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[12] = 0.5726439999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[13] = 0.7539324000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[14] = 1.355067;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[15] = 1.5561839999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[16] = 1.9909060000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[17] = 2.667868;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[18] = 3.35172;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[19] = 4.044117000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[20] = 4.802952;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[0] = 4.2500119999999997e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[1] = 0.0002764335;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[2] = 0.002272346;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[3] = 0.006939387;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[4] = 0.014288789999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[5] = 0.035657440000000006;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[6] = 0.04928406;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[7] = 0.0808344;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[8] = 0.13651880000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[9] = 0.1562218;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[10] = 0.1771568;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[11] = 0.2882205;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[12] = 0.40391;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[13] = 0.5213296000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[14] = 0.8989820000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[15] = 1.021128;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[16] = 1.28317;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[17] = 1.682744;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[18] = 2.07638;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[19] = 2.4687740000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[20] = 2.8974330000000004;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[0] = 6.287e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[1] = 0.0003761;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[2] = 0.002986;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[3] = 0.008931;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[4] = 0.01811;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[5] = 0.04424;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[6] = 0.06062;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[7] = 0.0981;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[8] = 0.1633;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[9] = 0.1862;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[10] = 0.2104;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[11] = 0.3371;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[12] = 0.478;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[13] = 0.6236;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[14] = 1.099;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[15] = 1.256;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[16] = 1.594;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[17] = 2.114;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[18] = 2.635;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[19] = 3.157;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[20] = 3.729;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[0] = 8.229683e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[1] = 0.00043853259999999993;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[2] = 0.0033861239999999995;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[3] = 0.009966996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[4] = 0.01999344;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[5] = 0.0480004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[6] = 0.06534836000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[7] = 0.10457460000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[8] = 0.1721182;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[9] = 0.19551000000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[10] = 0.2204992;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[11] = 0.350584;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[12] = 0.4966419999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[13] = 0.6497912;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[14] = 1.158346;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[15] = 1.328848;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[16] = 1.69761;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[17] = 2.270436;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[18] = 2.85107;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[19] = 3.431659;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[20] = 4.072068000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[0] = 4.3443170000000003e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[1] = 0.0003136674;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[2] = 0.002585876;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[3] = 0.007895004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[4] = 0.01622656;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[5] = 0.040479600000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[6] = 0.05589164;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[7] = 0.0916254;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[8] = 0.1544818;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[9] = 0.17689;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[10] = 0.2003008;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[11] = 0.323616;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[12] = 0.459358;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[13] = 0.5974088000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[14] = 1.0396539999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[15] = 1.183152;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[16] = 1.49039;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[17] = 1.9575639999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[18] = 2.41893;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[19] = 2.8823410000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[20] = 3.3859320000000004;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Pb208_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
