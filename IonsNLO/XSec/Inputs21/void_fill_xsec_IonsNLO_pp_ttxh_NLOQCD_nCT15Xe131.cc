// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxh_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[0] = 3.375e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[1] = 0.0003765;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[2] = 0.003525;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[3] = 0.01193;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[4] = 0.02689;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[5] = 0.0809;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[6] = 0.1201;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[7] = 0.2264;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[8] = 0.4535;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[9] = 0.5527;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[10] = 0.6586;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[11] = 1.313;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[12] = 2.174;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[13] = 3.267;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[14] = 7.508;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[15] = 9.181;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[16] = 13.22;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[17] = 19.66;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[18] = 27.79;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[19] = 35.56;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[20] = 44.66;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[0] = 3.9689999999999996e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[1] = 0.000398337;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[2] = 0.00368715;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[3] = 0.01243106;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[4] = 0.02799249;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[5] = 0.08437869999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[6] = 0.1250241;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[7] = 0.2368144;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[8] = 0.476175;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[9] = 0.5814404;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[10] = 0.6941644;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[11] = 1.390467;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[12] = 2.3000920000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[13] = 3.4793549999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[14] = 7.9810039999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[15] = 9.741041;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[16] = 14.092520000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[17] = 20.91824;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[18] = 29.735300000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[19] = 37.80028;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[20] = 47.69688;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[0] = 2.710125e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[1] = 0.000332073;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[2] = 0.003183075;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[3] = 0.0108563;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[4] = 0.02455057;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[5] = 0.0739426;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[6] = 0.1100116;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[7] = 0.2073824;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[8] = 0.41631300000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[9] = 0.5068258999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[10] = 0.6039362;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[11] = 1.206647;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[12] = 2.004428;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[13] = 3.008907;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[14] = 6.967423999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[15] = 8.547511;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[16] = 12.307820000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[17] = 18.36244;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[18] = 25.90028;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[19] = 33.35528;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[20] = 41.84642;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[0] = 3.375e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[1] = 0.0003765;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[2] = 0.003525;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[3] = 0.01193;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[4] = 0.02689;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[5] = 0.0809;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[6] = 0.1201;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[7] = 0.2264;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[8] = 0.4535;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[9] = 0.5527;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[10] = 0.6586;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[11] = 1.313;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[12] = 2.174;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[13] = 3.267;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[14] = 7.508;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[15] = 9.181;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[16] = 13.22;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[17] = 19.66;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[18] = 27.79;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[19] = 35.56;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[20] = 44.66;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[0] = 4.606875e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[1] = 0.00044427;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[2] = 0.004078425;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[3] = 0.01370757;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[4] = 0.030762160000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[5] = 0.09149790000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[6] = 0.1351125;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[7] = 0.2517568;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[8] = 0.4965825;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[9] = 0.6029956999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[10] = 0.7152396;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[11] = 1.400971;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[12] = 2.287048;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[13] = 3.4074809999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[14] = 7.6956999999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[15] = 9.392162999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[16] = 13.510840000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[17] = 20.19082;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[18] = 28.56812;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[19] = 36.911280000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[20] = 46.58037999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[0] = 2.143125e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[1] = 0.00030873;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[2] = 0.0029715749999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[3] = 0.010152429999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[4] = 0.02301784;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[5] = 0.07030209999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[6] = 0.1050875;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[7] = 0.2010432;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[8] = 0.41041750000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[9] = 0.5024043;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[10] = 0.6019604;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[11] = 1.225029;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[12] = 2.060952;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[13] = 3.1265189999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[14] = 7.3203;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[15] = 8.969836999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[16] = 12.92916;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[17] = 19.129179999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[18] = 27.011879999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[19] = 34.20872;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[20] = 42.739619999999995;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[0] = 2.056e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[1] = 0.0002773;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[2] = 0.00273;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[3] = 0.009317;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[4] = 0.02109;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[5] = 0.06208;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[6] = 0.09242;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[7] = 0.1726;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[8] = 0.343;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[9] = 0.4136;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[10] = 0.4916;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[11] = 0.9685;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[12] = 1.601;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[13] = 2.371;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[14] = 5.392;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[15] = 6.571;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[16] = 9.321;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[17] = 13.81;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[18] = 19.16;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[19] = 24.71;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[20] = 30.67;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[0] = 3.287544e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[1] = 0.0003998666;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[2] = 0.0037947000000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[3] = 0.012708388;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[4] = 0.028429319999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[5] = 0.08238016000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[6] = 0.12190198;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[7] = 0.22541560000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[8] = 0.44247000000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[9] = 0.5318896000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[10] = 0.6302312;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[11] = 1.222247;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[12] = 1.998048;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[13] = 2.9281849999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[14] = 6.508144000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[15] = 7.878629;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[16] = 11.073348;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[17] = 16.309610000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[18] = 22.762079999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[19] = 29.577870000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[20] = 36.957350000000005;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[0] = 1.350792e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[1] = 0.0001993787;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[2] = 0.0020283899999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[3] = 0.007034335000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[4] = 0.016091670000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[5] = 0.047987840000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[6] = 0.07181034;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[7] = 0.1351458;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[8] = 0.271313;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[9] = 0.3279848;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[10] = 0.390822;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[11] = 0.7796425;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[12] = 1.301613;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[13] = 1.94422;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[14] = 4.513104;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[15] = 5.526211;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[16] = 7.904208;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[17] = 11.79374;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[18] = 16.20936;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[19] = 20.73169;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[20] = 25.54811;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[0] = 2.056e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[1] = 0.0002773;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[2] = 0.00273;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[3] = 0.009317;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[4] = 0.02109;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[5] = 0.06208;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[6] = 0.09242;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[7] = 0.1726;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[8] = 0.343;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[9] = 0.4136;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[10] = 0.4916;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[11] = 0.9685;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[12] = 1.601;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[13] = 2.371;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[14] = 5.392;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[15] = 6.571;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[16] = 9.321;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[17] = 13.81;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[18] = 19.16;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[19] = 24.71;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[20] = 30.67;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[0] = 2.794104e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[1] = 0.0003252729;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[2] = 0.00313131;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[3] = 0.010602746;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[4] = 0.02389497;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[5] = 0.06971584;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[6] = 0.10323314;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[7] = 0.1910682;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[8] = 0.37455600000000006;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[9] = 0.4495832;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[10] = 0.5324028;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[11] = 1.0314524999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[12] = 1.682651;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[13] = 2.4682109999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[14] = 5.521408;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[15] = 6.715562;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[16] = 9.535383;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[17] = 14.21049;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[18] = 19.830599999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[19] = 25.74782;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[20] = 32.111489999999996;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[0] = 1.317896e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[1] = 0.00022932710000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[2] = 0.0023286899999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[3] = 0.008031254;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[4] = 0.01828503;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[5] = 0.054444160000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[6] = 0.08160686;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[7] = 0.1541318;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[8] = 0.31144400000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[9] = 0.37761680000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[10] = 0.4507972;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[11] = 0.9055475000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[12] = 1.5193489999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[13] = 2.273789;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[14] = 5.262592000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[15] = 6.426437999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[16] = 9.106617;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[17] = 13.409510000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[18] = 18.4894;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[19] = 23.67218;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[20] = 29.22851;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15Xe131_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
