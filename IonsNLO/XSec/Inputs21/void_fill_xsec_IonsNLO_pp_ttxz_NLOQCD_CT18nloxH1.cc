// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxz_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[0] = 1.027e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[1] = 0.0006435;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[2] = 0.005731;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[3] = 0.01931;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[4] = 0.04377;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[5] = 0.1313;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[6] = 0.1966;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[7] = 0.3708;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[8] = 0.7469;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[9] = 0.9009;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[10] = 1.077;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[11] = 2.172;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[12] = 3.569;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[13] = 5.327;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[14] = 12.56;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[15] = 15.12;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[16] = 21.58;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[17] = 32.25;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[18] = 45.21;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[19] = 58.92;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[20] = 74.39;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[0] = 1.259102e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[1] = 0.000731016;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[2] = 0.0063957960000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[3] = 0.02133755;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[4] = 0.04819077;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[5] = 0.1442987;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[6] = 0.215277;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[7] = 0.4067676;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[8] = 0.8178555;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[9] = 0.9837828000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[10] = 1.179315;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[11] = 2.3848560000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[12] = 3.8795029999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[13] = 5.795776;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[14] = 13.715520000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[15] = 16.374959999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[16] = 23.327979999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[17] = 34.7655;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[18] = 48.736380000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[19] = 63.397920000000006;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[20] = 80.11803;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[0] = 8.041410000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[1] = 0.0005418269999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[2] = 0.00492866;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[3] = 0.0167997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[4] = 0.038298750000000006;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[5] = 0.1156753;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[6] = 0.1737944;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[7] = 0.3288996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[8] = 0.6654879;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[9] = 0.8054046;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[10] = 0.961761;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[11] = 1.9439400000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[12] = 3.226376;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[13] = 4.826262;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[14] = 11.41704;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[15] = 13.8348;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[16] = 19.81044;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[17] = 29.766750000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[18] = 41.63841;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[19] = 54.14748;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[20] = 68.14124;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[0] = 1.027e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[1] = 0.0006435;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[2] = 0.005731;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[3] = 0.01931;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[4] = 0.04377;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[5] = 0.1313;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[6] = 0.1966;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[7] = 0.3708;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[8] = 0.7469;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[9] = 0.9009;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[10] = 1.077;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[11] = 2.172;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[12] = 3.569;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[13] = 5.327;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[14] = 12.56;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[15] = 15.12;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[16] = 21.58;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[17] = 32.25;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[18] = 45.21;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[19] = 58.92;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[20] = 74.39;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[0] = 1.5774720000000002e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[1] = 0.0007535385;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[2] = 0.0063212929999999995;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[3] = 0.02083549;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[4] = 0.046658820000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[5] = 0.13839020000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[6] = 0.20623339999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[7] = 0.3867444;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[8] = 0.7745352999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[9] = 0.9324315;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[10] = 1.113618;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[11] = 2.234988;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[12] = 3.658225;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[13] = 5.449521;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[14] = 12.811200000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[15] = 15.4224;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[16] = 21.990019999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[17] = 32.86275;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[18] = 46.06899;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[19] = 60.03948;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[20] = 75.87780000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[0] = 6.613879999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[1] = 0.000548262;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[2] = 0.005163631;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[3] = 0.017823130000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[4] = 0.04096872;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[5] = 0.1246037;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[6] = 0.18755639999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[7] = 0.3555972;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[8] = 0.7200116;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[9] = 0.8702694;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[10] = 1.041459;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[11] = 2.109012;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[12] = 3.476206;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[13] = 5.199152;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[14] = 12.296240000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[15] = 14.80248;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[16] = 21.12682;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[17] = 31.605;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[18] = 44.26059;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[19] = 57.68268;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[20] = 72.82781;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[0] = 5.785e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[1] = 0.000417;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[2] = 0.003822;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[3] = 0.01308;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[4] = 0.02982;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[5] = 0.08965;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[6] = 0.1333;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[7] = 0.2508;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[8] = 0.5039;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[9] = 0.6064;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[10] = 0.7214;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[11] = 1.432;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[12] = 2.394;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[13] = 3.561;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[14] = 8.215;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[15] = 10.14;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[16] = 14.29;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[17] = 21.47;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[18] = 29.97;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[19] = 39.28;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[20] = 49.17;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[0] = 9.146085e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[1] = 0.000605067;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[2] = 0.00536991;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[3] = 0.01803732;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[4] = 0.04061484;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[5] = 0.12004134999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[6] = 0.177289;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[7] = 0.33005280000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[8] = 0.6545661;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[9] = 0.7846816000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[10] = 0.930606;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[11] = 1.81864;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[12] = 3.00447;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[13] = 4.426322999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[14] = 9.98944;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[15] = 12.259260000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[16] = 17.119419999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[17] = 25.84988;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[18] = 36.383579999999995;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[19] = 48.00016;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[20] = 60.429930000000006;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[0] = 3.8181e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[1] = 0.000297321;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[2] = 0.002805348;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[3] = 0.00975768;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[4] = 0.02248428;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[5] = 0.06858225;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[6] = 0.10264100000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[7] = 0.19487160000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[8] = 0.3955615;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[9] = 0.4778432000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[10] = 0.569906;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[11] = 1.147032;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[12] = 1.9367459999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[13] = 2.9057760000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[14] = 6.834879999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[15] = 8.47704;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[16] = 12.04647;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[17] = 17.97039;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[18] = 24.845129999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[19] = 32.327439999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[20] = 40.17189;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[0] = 5.785e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[1] = 0.000417;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[2] = 0.003822;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[3] = 0.01308;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[4] = 0.02982;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[5] = 0.08965;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[6] = 0.1333;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[7] = 0.2508;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[8] = 0.5039;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[9] = 0.6064;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[10] = 0.7214;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[11] = 1.432;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[12] = 2.394;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[13] = 3.561;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[14] = 8.215;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[15] = 10.14;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[16] = 14.29;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[17] = 21.47;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[18] = 29.97;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[19] = 39.28;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[20] = 49.17;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[0] = 8.937825000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[1] = 0.000490392;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[2] = 0.004227132000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[3] = 0.014113319999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[4] = 0.03178812;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[5] = 0.09440144999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[6] = 0.1396984;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[7] = 0.2615844;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[8] = 0.5225443;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[9] = 0.627624;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[10] = 0.7459276;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[11] = 1.4735279999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[12] = 2.45385;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[13] = 3.6429029999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[14] = 8.387514999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[15] = 10.3428;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[16] = 14.561509999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[17] = 21.877929999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[18] = 30.569399999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[19] = 40.0656;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[20] = 50.202569999999994;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[0] = 3.7081850000000005e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[1] = 0.00035403299999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[2] = 0.003435978;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[3] = 0.01204668;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[4] = 0.027881700000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[5] = 0.08507785;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[6] = 0.1270349;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[7] = 0.24051720000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[8] = 0.4857596;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[9] = 0.5857824;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[10] = 0.6975938;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[11] = 1.390472;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[12] = 2.331756;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[13] = 3.471975;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[14] = 8.03427;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[15] = 9.92706;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[16] = 13.989909999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[17] = 21.019129999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[18] = 29.31066;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[19] = 38.41584;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[20] = 48.03909;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_CT18nloxH1_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
