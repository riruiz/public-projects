// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxwm_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[0] = 6.016e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[1] = 0.0003262;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[2] = 0.002638;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[3] = 0.008194;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[4] = 0.01737;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[5] = 0.04556;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[6] = 0.06417;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[7] = 0.1108;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[8] = 0.1951;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[9] = 0.2276;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[10] = 0.2625;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[11] = 0.4651;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[12] = 0.7082;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[13] = 0.9823;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[14] = 1.988;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[15] = 2.36;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[16] = 3.18;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[17] = 4.571;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[18] = 6.095;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[19] = 7.741;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[20] = 9.434;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[0] = 7.339519999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[1] = 0.000368606;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[2] = 0.0029229040000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[3] = 0.009013400000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[4] = 0.019107000000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[5] = 0.05002488000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[6] = 0.07052283000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[7] = 0.12276640000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[8] = 0.216561;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[9] = 0.2530912;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[10] = 0.29242500000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[11] = 0.5227724;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[12] = 0.8009742000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[13] = 1.1158928000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[14] = 2.2782479999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[15] = 2.70456;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[16] = 3.6506400000000006;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[17] = 5.284075999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[18] = 7.100675;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[19] = 9.087933999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[20] = 11.150987999999998;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[0] = 4.740608e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[1] = 0.00027759620000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[2] = 0.002297698;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[3] = 0.007227108;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[4] = 0.01538982;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[5] = 0.040685080000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[6] = 0.05743215000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[7] = 0.0989444;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[8] = 0.1746145;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[9] = 0.203702;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[10] = 0.23493750000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[11] = 0.4148692;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[12] = 0.6295898000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[13] = 0.8722824;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[14] = 1.753416;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[15] = 2.06972;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[16] = 2.76342;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[17] = 3.9264889999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[18] = 5.186845;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[19] = 6.533403999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[20] = 7.905691999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[0] = 6.016e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[1] = 0.0003262;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[2] = 0.002638;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[3] = 0.008194;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[4] = 0.01737;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[5] = 0.04556;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[6] = 0.06417;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[7] = 0.1108;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[8] = 0.1951;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[9] = 0.2276;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[10] = 0.2625;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[11] = 0.4651;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[12] = 0.7082;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[13] = 0.9823;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[14] = 1.988;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[15] = 2.36;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[16] = 3.18;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[17] = 4.571;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[18] = 6.095;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[19] = 7.741;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[20] = 9.434;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[0] = 9.709824e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[1] = 0.0003986164;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[2] = 0.0029941300000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[3] = 0.009037982;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[4] = 0.01888119;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[5] = 0.04870364;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[6] = 0.06821271;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[7] = 0.1167832;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[8] = 0.2040746;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[9] = 0.2376144;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[10] = 0.273525;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[11] = 0.48091340000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[12] = 0.7287378;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[13] = 1.0078398;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[14] = 2.0297479999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[15] = 2.4072;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[16] = 3.2372400000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[17] = 4.648706999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[18] = 6.198614999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[19] = 7.872596999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[20] = 9.594377999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[0] = 3.056128e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[1] = 0.0002469334;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[2] = 0.002208006;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[3] = 0.0071451679999999995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[4] = 0.0154593;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[5] = 0.041459600000000006;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[6] = 0.05884389000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[7] = 0.1027116;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[8] = 0.18280870000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[9] = 0.213944;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[10] = 0.247275;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[11] = 0.4423101;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[12] = 0.6777474;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[13] = 0.9439903;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[14] = 1.9243839999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[15] = 2.2868399999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[16] = 3.08778;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[17] = 4.447583;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[18] = 5.930434999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[19] = 7.539733999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[20] = 9.188716;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[0] = 3.397e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[1] = 0.0002112;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[2] = 0.001775;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[3] = 0.00559;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[4] = 0.01177;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[5] = 0.03062;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[6] = 0.04268;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[7] = 0.07161;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[8] = 0.1231;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[9] = 0.1425;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[10] = 0.1624;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[11] = 0.2699;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[12] = 0.3915;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[13] = 0.526;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[14] = 0.9604;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[15] = 1.109;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[16] = 1.427;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[17] = 1.926;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[18] = 2.442;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[19] = 2.968;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[20] = 3.493;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[0] = 5.29932e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[1] = 0.000299904;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[2] = 0.0024264250000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[3] = 0.007468240000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[4] = 0.01546578;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[5] = 0.0393467;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[6] = 0.054374320000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[7] = 0.08994216;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[8] = 0.1521516;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[9] = 0.1754175;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[10] = 0.19910239999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[11] = 0.32549939999999994;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[12] = 0.472149;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[13] = 0.6401420000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[14] = 1.1889752;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[15] = 1.380705;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[16] = 1.789458;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[17] = 2.4402419999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[18] = 3.1159920000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[19] = 3.810912;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[20] = 4.509462999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[0] = 2.262402e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[1] = 0.00015312;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[2] = 0.00133125;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[3] = 0.004281940000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[4] = 0.00914529;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[5] = 0.024281660000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[6] = 0.034144;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[7] = 0.058004099999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[8] = 0.10106509999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[9] = 0.11741999999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[10] = 0.13430479999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[11] = 0.22644609999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[12] = 0.328077;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[13] = 0.43658;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[14] = 0.7817656;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[15] = 0.8971809999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[16] = 1.144454;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[17] = 1.527318;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[18] = 1.9218540000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[19] = 2.318008;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[20] = 2.710568;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[0] = 3.397e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[1] = 0.0002112;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[2] = 0.001775;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[3] = 0.00559;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[4] = 0.01177;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[5] = 0.03062;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[6] = 0.04268;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[7] = 0.07161;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[8] = 0.1231;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[9] = 0.1425;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[10] = 0.1624;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[11] = 0.2699;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[12] = 0.3915;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[13] = 0.526;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[14] = 0.9604;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[15] = 1.109;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[16] = 1.427;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[17] = 1.926;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[18] = 2.442;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[19] = 2.968;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[20] = 3.493;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[0] = 5.50314e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[1] = 0.0002589312;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[2] = 0.00201995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[3] = 0.0061769500000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[4] = 0.012817529999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[5] = 0.03279402;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[6] = 0.04549688;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[7] = 0.07576337999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[8] = 0.129255;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[9] = 0.14934;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[10] = 0.1698704;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[11] = 0.28042609999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[12] = 0.40520249999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[13] = 0.5428320000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[14] = 0.9872912;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[15] = 1.1389429999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[16] = 1.464102;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[17] = 1.9741499999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[18] = 2.50305;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[19] = 3.0422;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[20] = 3.583818;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[0] = 1.729073e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[1] = 0.000159456;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[2] = 0.00148035;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[3] = 0.00485771;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[4] = 0.01043999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[5] = 0.02774172;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[6] = 0.038924160000000006;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[7] = 0.06595281;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[8] = 0.1146061;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[9] = 0.1329525;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[10] = 0.151844;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[11] = 0.25478559999999995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[12] = 0.3715335;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[13] = 0.501278;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[14] = 0.9210235999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[15] = 1.06464;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[16] = 1.372774;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[17] = 1.8547379999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[18] = 2.354088;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[19] = 2.8611519999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[20] = 3.3672519999999997;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_CT18nloxH1_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
