// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxa_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[0] = 7.177e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[1] = 3.851e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[2] = 0.0005677;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[3] = 0.002452;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[4] = 0.006592;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[5] = 0.0236;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[6] = 0.0375;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[7] = 0.07691;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[8] = 0.1697;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[9] = 0.2059;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[10] = 0.2539;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[11] = 0.539;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[12] = 0.9264;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[13] = 1.403;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[14] = 3.346;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[15] = 4.124;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[16] = 5.885;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[17] = 8.929;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[18] = 12.53;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[19] = 16.16;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[20] = 20.1;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[0] = 9.444932000000001e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[1] = 4.5518820000000003e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[2] = 0.0006511519;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[3] = 0.0027756640000000006;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[4] = 0.007448959999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[5] = 0.02655;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[6] = 0.042187499999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[7] = 0.08606229;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[8] = 0.1897246;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[9] = 0.2283431;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[10] = 0.28284460000000006;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[11] = 0.597751;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[12] = 1.0227456000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[13] = 1.5433000000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[14] = 3.650486;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[15] = 4.499283999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[16] = 6.402880000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[17] = 9.705823;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[18] = 13.620109999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[19] = 17.50128;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[20] = 21.708000000000002;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[0] = 5.339688e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[1] = 3.165522e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[2] = 0.00047970649999999994;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[3] = 0.002098912;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[4] = 0.00566912;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[5] = 0.0204376;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[6] = 0.032549999999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[7] = 0.06714243;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[8] = 0.1486572;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[9] = 0.1816038;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[10] = 0.22343200000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[11] = 0.47755400000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[12] = 0.8263488;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[13] = 1.257088;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[14] = 3.034822;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[15] = 3.744592;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[16] = 5.36712;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[17] = 8.170035;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[18] = 11.50254;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[19] = 14.89952;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[20] = 18.5925;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[0] = 7.177e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[1] = 3.851e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[2] = 0.0005677;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[3] = 0.002452;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[4] = 0.006592;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[5] = 0.0236;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[6] = 0.0375;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[7] = 0.07691;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[8] = 0.1697;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[9] = 0.2059;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[10] = 0.2539;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[11] = 0.539;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[12] = 0.9264;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[13] = 1.403;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[14] = 3.346;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[15] = 4.124;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[16] = 5.885;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[17] = 8.929;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[18] = 12.53;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[19] = 16.16;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[20] = 20.1;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[0] = 1.0729615e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[1] = 4.740581000000001e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[2] = 0.0006795369;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[3] = 0.0029056200000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[4] = 0.007765375999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[5] = 0.0274704;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[6] = 0.04334999999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[7] = 0.08783122;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[8] = 0.1905731;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[9] = 0.22978440000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[10] = 0.28259070000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[11] = 0.5885880000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[12] = 0.9968064000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[13] = 1.4913889999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[14] = 3.473148;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[15] = 4.264215999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[16] = 6.04978;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[17] = 9.179012;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[18] = 12.918429999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[19] = 16.74176;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[20] = 20.944200000000002;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[0] = 3.624385e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[1] = 2.961419e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[2] = 0.0004558631;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[3] = 0.00199838;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[4] = 0.0054186239999999995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[5] = 0.0197296;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[6] = 0.03165;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[7] = 0.06598878000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[8] = 0.14882689999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[9] = 0.1820156;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[10] = 0.22520930000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[11] = 0.48941200000000007;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[12] = 0.8559936;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[13] = 1.3146110000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[14] = 3.218852;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[15] = 3.9837839999999995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[16] = 5.720219999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[17] = 8.678988;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[18] = 12.14157;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[19] = 15.57824;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[20] = 19.2558;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[0] = 3.47e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[1] = 2.222e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[2] = 0.0003447;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[3] = 0.00152;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[4] = 0.004039;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[5] = 0.01459;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[6] = 0.02321;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[7] = 0.04788;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[8] = 0.1051;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[9] = 0.1281;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[10] = 0.1564;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[11] = 0.3377;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[12] = 0.579;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[13] = 0.8897;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[14] = 2.123;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[15] = 2.651;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[16] = 3.743;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[17] = 5.677;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[18] = 7.813;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[19] = 10.29;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[20] = 12.81;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[0] = 5.77408e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[1] = 3.281894e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[2] = 0.0004901634;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[3] = 0.0021204;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[4] = 0.005561703;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[5] = 0.019784040000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[6] = 0.03126387;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[7] = 0.06387192;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[8] = 0.1383116;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[9] = 0.1680672;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[10] = 0.2044148;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[11] = 0.43461989999999995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[12] = 0.7364879999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[13] = 1.1192426;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[14] = 2.6091670000000007;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[15] = 3.236871;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[16] = 4.521544;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[17] = 6.766983999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[18] = 9.211527;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[19] = 12.111329999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[20] = 15.128610000000002;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[0] = 2.19998e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[1] = 1.562066e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[2] = 0.0002505969;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[3] = 0.0011232800000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[4] = 0.003017133;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[5] = 0.01104463;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[6] = 0.01766281;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[7] = 0.03677184;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[8] = 0.08155760000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[9] = 0.0996618;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[10] = 0.12214840000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[11] = 0.2671207;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[12] = 0.46262099999999995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[13] = 0.7170982000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[14] = 1.7472290000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[15] = 2.195028;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[16] = 3.1254049999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[17] = 4.797065;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[18] = 6.664489;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[19] = 8.818529999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[20] = 10.90131;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[0] = 3.47e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[1] = 2.222e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[2] = 0.0003447;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[3] = 0.00152;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[4] = 0.004039;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[5] = 0.01459;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[6] = 0.02321;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[7] = 0.04788;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[8] = 0.1051;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[9] = 0.1281;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[10] = 0.1564;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[11] = 0.3377;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[12] = 0.579;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[13] = 0.8897;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[14] = 2.123;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[15] = 2.651;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[16] = 3.743;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[17] = 5.677;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[18] = 7.813;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[19] = 10.29;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[20] = 12.81;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[0] = 5.13907e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[1] = 2.71084e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[2] = 0.0004091589;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[3] = 0.0017890400000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[4] = 0.004729669000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[5] = 0.01693899;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[6] = 0.02678434;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[7] = 0.05472684;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[8] = 0.11834259999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[9] = 0.14360009999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[10] = 0.17454240000000004;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[11] = 0.3697815;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[12] = 0.6247409999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[13] = 0.9475305;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[14] = 2.20792;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[15] = 2.7437849999999995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[16] = 3.8515469999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[17] = 5.841632999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[18] = 8.070829;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[19] = 10.68102;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[20] = 13.36083;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[0] = 1.8009299999999995e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[1] = 1.7331600000000002e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[2] = 0.00028024109999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[3] = 0.00125096;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[4] = 0.003348331;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[5] = 0.01224101;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[6] = 0.01963566;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[7] = 0.04103316;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[8] = 0.0918574;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[9] = 0.11259989999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[10] = 0.1382576;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[11] = 0.3056185;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[12] = 0.533259;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[13] = 0.8318695;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[14] = 2.0380800000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[15] = 2.5582149999999997;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[16] = 3.6344529999999997;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[17] = 5.512366999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[18] = 7.555171;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[19] = 9.898979999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[20] = 12.25917;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Xe131_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
