// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxh_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[0] = 4.227e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[1] = 0.0004222;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[2] = 0.003822;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[3] = 0.01269;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[4] = 0.02845;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[5] = 0.08346;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[6] = 0.1233;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[7] = 0.2302;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[8] = 0.4581;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[9] = 0.5539;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[10] = 0.6599;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[11] = 1.321;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[12] = 2.173;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[13] = 3.223;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[14] = 7.383;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[15] = 8.963;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[16] = 12.71;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[17] = 18.98;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[18] = 26.38;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[19] = 34.59;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[20] = 43.88;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[0] = 4.992087e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[1] = 0.00044879859999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[2] = 0.003997812;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[3] = 0.01321029;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[4] = 0.029559549999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[5] = 0.08679840000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[6] = 0.1283553;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[7] = 0.24101939999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[8] = 0.481005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[9] = 0.5827028;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[10] = 0.6961945;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[11] = 1.401581;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[12] = 2.296861;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[13] = 3.4196029999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[14] = 7.82598;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[15] = 9.509742999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[16] = 13.45989;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[17] = 20.156760000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[18] = 27.989179999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[19] = 36.76917;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[20] = 46.7322;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[0] = 3.3816e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[1] = 0.0003711138;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[2] = 0.003447444;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[3] = 0.01156059;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[4] = 0.0260033;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[5] = 0.07644936000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[6] = 0.11306610000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[7] = 0.2108632;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[8] = 0.42099390000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[9] = 0.5090340999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[10] = 0.6057882000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[11] = 1.212678;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[12] = 2.007852;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[13] = 2.978052;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[14] = 6.86619;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[15] = 8.344553;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[16] = 11.87114;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[17] = 17.76528;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[18] = 24.744439999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[19] = 32.48001000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[20] = 41.203320000000005;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[0] = 4.227e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[1] = 0.0004222;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[2] = 0.003822;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[3] = 0.01269;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[4] = 0.02845;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[5] = 0.08346;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[6] = 0.1233;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[7] = 0.2302;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[8] = 0.4581;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[9] = 0.5539;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[10] = 0.6599;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[11] = 1.321;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[12] = 2.173;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[13] = 3.223;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[14] = 7.383;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[15] = 8.963;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[16] = 12.71;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[17] = 18.98;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[18] = 26.38;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[19] = 34.59;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[20] = 43.88;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[0] = 4.916001e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[1] = 0.00046315340000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[2] = 0.0041354040000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[3] = 0.01365444;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[4] = 0.0305553;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[5] = 0.08930220000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[6] = 0.1316844;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[7] = 0.24470259999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[8] = 0.4832955;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[9] = 0.5832567;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[10] = 0.6935549;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[11] = 1.3751609999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[12] = 2.2425360000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[13] = 3.306798;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[14] = 7.478978999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[15] = 9.061592999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[16] = 12.837100000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[17] = 19.22674;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[18] = 26.828459999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[19] = 35.281800000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[20] = 44.93312;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[0] = 3.5379989999999996e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[1] = 0.0003812466;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[2] = 0.003508596;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[3] = 0.011725560000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[4] = 0.0263447;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[5] = 0.0776178;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[6] = 0.11491559999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[7] = 0.2156974;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[8] = 0.43290449999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[9] = 0.5245432999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[10] = 0.6262451;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[11] = 1.2668389999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[12] = 2.103464;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[13] = 3.1392019999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[14] = 7.287021;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[15] = 8.864407;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[16] = 12.5829;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[17] = 18.73326;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[18] = 25.93154;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[19] = 33.8982;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[20] = 42.82688;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[0] = 2.584e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[1] = 0.0003109;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[2] = 0.002945;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[3] = 0.009953;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[4] = 0.02228;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[5] = 0.06467;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[6] = 0.09493;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[7] = 0.1758;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[8] = 0.3444;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[9] = 0.4146;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[10] = 0.4913;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[11] = 0.9651;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[12] = 1.592;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[13] = 2.338;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[14] = 5.288;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[15] = 6.519;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[16] = 9.095;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[17] = 13.61;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[18] = 18.54;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[19] = 24.25;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[20] = 30.06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[0] = 4.1524879999999995e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[1] = 0.0004489396;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[2] = 0.004096495;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[3] = 0.013575891999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[4] = 0.029988880000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[5] = 0.08555841;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[6] = 0.12483295;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[7] = 0.22889160000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[8] = 0.44255399999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[9] = 0.5311026000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[10] = 0.6273901;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[11] = 1.2140958;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[12] = 1.980448;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[13] = 2.8780780000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[14] = 6.372040000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[15] = 7.809762;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[16] = 10.78667;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[17] = 16.08702;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[18] = 22.0626;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[19] = 29.051499999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[20] = 36.2223;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[0] = 1.6925199999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[1] = 0.0002232262;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[2] = 0.00218519;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[3] = 0.007514515;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[4] = 0.01699964;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[5] = 0.05005458000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[6] = 0.07395047;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[7] = 0.13800300000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[8] = 0.2731092;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[9] = 0.3300216;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[10] = 0.39205740000000006;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[11] = 0.7798008;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[12] = 1.2990720000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[13] = 1.924174;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[14] = 4.431344;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[15] = 5.488998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[16] = 7.7307500000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[17] = 11.60933;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[18] = 15.6663;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[19] = 20.34575;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[20] = 25.039979999999996;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[0] = 2.584e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[1] = 0.0003109;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[2] = 0.002945;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[3] = 0.009953;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[4] = 0.02228;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[5] = 0.06467;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[6] = 0.09493;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[7] = 0.1758;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[8] = 0.3444;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[9] = 0.4146;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[10] = 0.4913;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[11] = 0.9651;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[12] = 1.592;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[13] = 2.338;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[14] = 5.288;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[15] = 6.519;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[16] = 9.095;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[17] = 13.61;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[18] = 18.54;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[19] = 24.25;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[20] = 30.06;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[0] = 3.01036e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[1] = 0.00034074640000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[2] = 0.003177655;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[3] = 0.010669616;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[4] = 0.023839600000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[5] = 0.06893822000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[6] = 0.10100552;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[7] = 0.186348;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[8] = 0.36265319999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[9] = 0.4357446;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[10] = 0.5153736999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[11] = 1.0027389;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[12] = 1.6429440000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[13] = 2.3964499999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[14] = 5.351456000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[15] = 6.58419;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[16] = 9.18595;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[17] = 13.80054;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[18] = 18.87372;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[19] = 24.80775;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[20] = 30.871619999999997;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[0] = 2.1576399999999997e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[1] = 0.0002810536;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[2] = 0.002712345;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[3] = 0.009236384;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[4] = 0.0207204;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[5] = 0.06040178;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[6] = 0.08885448;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[7] = 0.165252;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[8] = 0.32614679999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[9] = 0.3934554;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[10] = 0.4672263;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[11] = 0.9274610999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[12] = 1.541056;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[13] = 2.27955;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[14] = 5.224544;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[15] = 6.45381;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[16] = 9.004050000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[17] = 13.419459999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[18] = 18.20628;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[19] = 23.692249999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[20] = 29.248379999999997;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_nCT15xxC12_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
