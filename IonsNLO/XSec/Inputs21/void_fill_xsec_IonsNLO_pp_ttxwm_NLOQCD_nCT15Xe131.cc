// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxwm_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[0] = 1.297e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[1] = 0.0007646;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[2] = 0.005891;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[3] = 0.01723;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[4] = 0.03414;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[5] = 0.0829;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[6] = 0.1129;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[7] = 0.1832;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[8] = 0.3091;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[9] = 0.3578;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[10] = 0.4047;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[11] = 0.6818;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[12] = 1.005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[13] = 1.361;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[14] = 2.634;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[15] = 3.071;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[16] = 4.124;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[17] = 5.806;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[18] = 7.599;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[19] = 9.518;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[20] = 11.46;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[0] = 1.5615879999999999e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[1] = 0.0008563520000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[2] = 0.006444754000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[3] = 0.018729009999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[4] = 0.036905339999999995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[5] = 0.0896149;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[6] = 0.1222707;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[7] = 0.19932160000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[8] = 0.3384645;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[9] = 0.39358000000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[10] = 0.4447653;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[11] = 0.7581616;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[12] = 1.1256;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[13] = 1.531125;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[14] = 2.992224;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[15] = 3.4825139999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[16] = 4.70136;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[17] = 6.636258;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[18] = 8.731251;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[19] = 11.021844;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[20] = 13.36236;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[0] = 1.0453820000000001e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[1] = 0.0006606144;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[2] = 0.005225317;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[3] = 0.01545531;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[4] = 0.0308967;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[5] = 0.0755219;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[6] = 0.10296480000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[7] = 0.16707840000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[8] = 0.2818992;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[9] = 0.325598;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[10] = 0.3686817;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[11] = 0.6177108;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[12] = 0.907515;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[13] = 1.2249;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[14] = 2.35743;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[15] = 2.73319;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[16] = 3.633244;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[17] = 5.0570260000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[18] = 6.557937;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[19] = 8.147408;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[20] = 9.741;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[0] = 1.297e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[1] = 0.0007646;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[2] = 0.005891;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[3] = 0.01723;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[4] = 0.03414;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[5] = 0.0829;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[6] = 0.1129;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[7] = 0.1832;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[8] = 0.3091;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[9] = 0.3578;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[10] = 0.4047;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[11] = 0.6818;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[12] = 1.005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[13] = 1.361;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[14] = 2.634;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[15] = 3.071;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[16] = 4.124;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[17] = 5.806;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[18] = 7.599;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[19] = 9.518;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[20] = 11.46;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[0] = 1.682209e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[1] = 0.0008754670000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[2] = 0.0065507920000000015;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[3] = 0.018866849999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[4] = 0.03700776;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[5] = 0.0886201;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[6] = 0.12012560000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[7] = 0.1930928;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[8] = 0.32300949999999995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[9] = 0.37282760000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[10] = 0.42088800000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[11] = 0.7036176;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[12] = 1.0321349999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[13] = 1.3963860000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[14] = 2.702484;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[15] = 3.160059;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[16] = 4.24772;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[17] = 6.0092099999999995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[18] = 7.895360999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[19] = 9.917756;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[20] = 11.987160000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[0] = 9.117910000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[1] = 0.000653733;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[2] = 0.005231208;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[3] = 0.01559315;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[4] = 0.03127224;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[5] = 0.07717990000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[6] = 0.10567439999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[7] = 0.1733072;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[8] = 0.29519049999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[9] = 0.3427724;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[10] = 0.38851199999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[11] = 0.6599824;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[12] = 0.9778649999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[13] = 1.3256139999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[14] = 2.5655159999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[15] = 2.981941;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[16] = 4.000279999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[17] = 5.60279;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[18] = 7.302639;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[19] = 9.118244;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[20] = 10.93284;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[0] = 7.274e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[1] = 0.0004922;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[2] = 0.003943;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[3] = 0.01161;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[4] = 0.0233;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[5] = 0.05564;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[6] = 0.07512;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[7] = 0.1188;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[8] = 0.1937;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[9] = 0.2214;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[10] = 0.2476;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[11] = 0.391;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[12] = 0.5484;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[13] = 0.7027;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[14] = 1.212;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[15] = 1.384;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[16] = 1.746;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[17] = 2.275;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[18] = 2.842;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[19] = 3.4;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[20] = 3.954;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[0] = 1.120196e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[1] = 0.0006944942000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[2] = 0.005342765;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[3] = 0.015348420000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[4] = 0.0302667;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[5] = 0.07049588;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[6] = 0.0942756;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[7] = 0.1468368;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[8] = 0.23534550000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[9] = 0.26767260000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[10] = 0.2981104;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[11] = 0.464508;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[12] = 0.6575316;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[13] = 0.850267;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[14] = 1.494396;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[15] = 1.7147759999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[16] = 2.1807540000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[17] = 2.87105;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[18] = 3.612182;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[19] = 4.3554;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[20] = 5.092752;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[0] = 4.931772e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[1] = 0.0003612748;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[2] = 0.0029966800000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[3] = 0.009020970000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[4] = 0.0183837;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[5] = 0.04490148;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[6] = 0.06107256;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[7] = 0.0978912;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[8] = 0.1621269;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[9] = 0.185976;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[10] = 0.2087268;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[11] = 0.334305;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[12] = 0.46284959999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[13] = 0.5874572;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[14] = 0.9914160000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[15] = 1.1251919999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[16] = 1.40553;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[17] = 1.8109;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[18] = 2.2423379999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[19] = 2.6622;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[20] = 3.0762120000000004;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[0] = 7.274e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[1] = 0.0004922;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[2] = 0.003943;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[3] = 0.01161;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[4] = 0.0233;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[5] = 0.05564;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[6] = 0.07512;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[7] = 0.1188;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[8] = 0.1937;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[9] = 0.2214;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[10] = 0.2476;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[11] = 0.391;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[12] = 0.5484;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[13] = 0.7027;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[14] = 1.212;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[15] = 1.384;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[16] = 1.746;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[17] = 2.275;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[18] = 2.842;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[19] = 3.4;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[20] = 3.954;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[0] = 9.492569999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[1] = 0.0005645534;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[2] = 0.004388559;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[3] = 0.012724560000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[4] = 0.025257200000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[5] = 0.05942352000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[6] = 0.07977744;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[7] = 0.12497760000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[8] = 0.2020291;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[9] = 0.23025600000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[10] = 0.25725639999999994;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[11] = 0.40390299999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[12] = 0.5664971999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[13] = 0.7279972;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[14] = 1.267752;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[15] = 1.4518159999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[16] = 1.8420299999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[17] = 2.418325;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[18] = 3.0352560000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[19] = 3.6516;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[20] = 4.262412;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[0] = 5.055430000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[1] = 0.0004198466;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[2] = 0.0034974410000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[3] = 0.01049544;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[4] = 0.021342800000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[5] = 0.051856479999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[6] = 0.07046256000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[7] = 0.1126224;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[8] = 0.1853709;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[9] = 0.212544;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[10] = 0.23794359999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[11] = 0.378097;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[12] = 0.5303028;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[13] = 0.6774028;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[14] = 1.156248;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[15] = 1.3161839999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[16] = 1.64997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[17] = 2.131675;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[18] = 2.6487439999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[19] = 3.1483999999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[20] = 3.6455880000000005;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15Xe131_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
