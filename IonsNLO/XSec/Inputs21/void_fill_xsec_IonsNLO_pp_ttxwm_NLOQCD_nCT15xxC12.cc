// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxwm_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[0] = 1.503e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[1] = 0.0007481;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[2] = 0.005571;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[3] = 0.01613;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[4] = 0.03213;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[5] = 0.07758;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[6] = 0.1063;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[7] = 0.1726;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[8] = 0.2927;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[9] = 0.3371;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[10] = 0.3841;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[11] = 0.6443;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[12] = 0.9525;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[13] = 1.296;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[14] = 2.517;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[15] = 2.993;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[16] = 3.952;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[17] = 5.519;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[18] = 7.298;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[19] = 9.238;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[20] = 11.12;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[0] = 1.8020970000000002e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[1] = 0.0008363758000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[2] = 0.006094674000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[3] = 0.01748492;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[4] = 0.03476466;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[5] = 0.08394156;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[6] = 0.1151229;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[7] = 0.1879614;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[8] = 0.3210919;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[9] = 0.3704729;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[10] = 0.42327820000000005;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[11] = 0.7151730000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[12] = 1.0639425;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[13] = 1.455408;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[14] = 2.8517609999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[15] = 3.403041;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[16] = 4.497375999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[17] = 6.286141000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[18] = 8.370806;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[19] = 10.679127999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[20] = 12.943679999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[0] = 1.215927e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[1] = 0.0006478546;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[2] = 0.004947048000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[3] = 0.014500869999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[4] = 0.02904552;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[5] = 0.0705978;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[6] = 0.0969456;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[7] = 0.1574112;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[8] = 0.266357;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[9] = 0.306761;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[10] = 0.3491469;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[11] = 0.5843801;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[12] = 0.86106;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[13] = 1.167696;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[14] = 2.255232;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[15] = 2.666763;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[16] = 3.489616;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[17] = 4.818087;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[18] = 6.305472;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[19] = 7.926203999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[20] = 9.47424;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[0] = 1.503e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[1] = 0.0007481;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[2] = 0.005571;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[3] = 0.01613;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[4] = 0.03213;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[5] = 0.07758;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[6] = 0.1063;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[7] = 0.1726;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[8] = 0.2927;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[9] = 0.3371;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[10] = 0.3841;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[11] = 0.6443;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[12] = 0.9525;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[13] = 1.296;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[14] = 2.517;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[15] = 2.993;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[16] = 3.952;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[17] = 5.519;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[18] = 7.298;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[19] = 9.238;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[20] = 11.12;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[0] = 1.72845e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[1] = 0.0008169252000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[2] = 0.005955399;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[3] = 0.017017149999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[4] = 0.03360798;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[5] = 0.08037288;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[6] = 0.1098079;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[7] = 0.1774328;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[8] = 0.2994321;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[9] = 0.3445162;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[10] = 0.39216609999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[11] = 0.6552530999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[12] = 0.9667874999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[13] = 1.314144;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[14] = 2.552238;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[15] = 3.037895;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[16] = 4.019183999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[17] = 5.634899;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[18] = 7.465853999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[19] = 9.46895;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[20] = 11.43136;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[0] = 1.27755e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[1] = 0.0006792748000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[2] = 0.005186601000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[3] = 0.015242849999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[4] = 0.03065202;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[5] = 0.07478712;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[6] = 0.1027921;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[7] = 0.1677672;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[8] = 0.2859679;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[9] = 0.3296838;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[10] = 0.3760339;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[11] = 0.6333468999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[12] = 0.9382125;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[13] = 1.277856;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[14] = 2.481762;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[15] = 2.948105;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[16] = 3.884816;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[17] = 5.403101;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[18] = 7.130146;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[19] = 9.00705;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[20] = 10.808639999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[0] = 8.501e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[1] = 0.0004862;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[2] = 0.00374;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[3] = 0.01103;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[4] = 0.02185;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[5] = 0.05211;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[6] = 0.07056;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[7] = 0.1122;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[8] = 0.1825;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[9] = 0.2075;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[10] = 0.2334;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[11] = 0.3715;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[12] = 0.5186;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[13] = 0.6773;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[14] = 1.173;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[15] = 1.348;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[16] = 1.698;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[17] = 2.237;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[18] = 2.802;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[19] = 3.359;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[20] = 3.924;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[0] = 1.3057536e-06;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[1] = 0.0006835972;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[2] = 0.00505648;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[3] = 0.0145596;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[4] = 0.02833945;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[5] = 0.06597125999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[6] = 0.08855279999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[7] = 0.1387914;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[8] = 0.22191999999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[9] = 0.2512825;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[10] = 0.28148039999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[11] = 0.44134199999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[12] = 0.6207642;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[13] = 0.8175011000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[14] = 1.4416170000000001;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[15] = 1.66478;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[16] = 2.11401;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[17] = 2.8119090000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[18] = 3.550134;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[19] = 4.282725;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[20] = 5.030568;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[0] = 5.78068e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[1] = 0.0003578432;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[2] = 0.00284988;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[3] = 0.00858134;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[4] = 0.017261500000000003;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[5] = 0.042052769999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[6] = 0.057435839999999995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[7] = 0.09245279999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[8] = 0.15257;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[9] = 0.17409249999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[10] = 0.1965228;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[11] = 0.3176325;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[12] = 0.43873559999999995;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[13] = 0.5675774;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[14] = 0.9618600000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[15] = 1.099968;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[16] = 1.3736819999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[17] = 1.7896;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[18] = 2.2219860000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[19] = 2.643533;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[20] = 3.068568;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[0] = 8.501e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[1] = 0.0004862;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[2] = 0.00374;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[3] = 0.01103;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[4] = 0.02185;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[5] = 0.05211;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[6] = 0.07056;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[7] = 0.1122;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[8] = 0.1825;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[9] = 0.2075;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[10] = 0.2334;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[11] = 0.3715;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[12] = 0.5186;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[13] = 0.6773;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[14] = 1.173;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[15] = 1.348;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[16] = 1.698;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[17] = 2.237;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[18] = 2.802;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[19] = 3.359;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[20] = 3.924;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[0] = 9.77615e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[1] = 0.0005309304;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[2] = 0.0040018;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[3] = 0.01164768;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[4] = 0.02287695;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[5] = 0.05398596;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[6] = 0.07281792;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[7] = 0.11522939999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[8] = 0.1866975;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[9] = 0.212065;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[10] = 0.23830139999999997;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[11] = 0.37893;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[12] = 0.5294905999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[13] = 0.6928778999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[14] = 1.20819;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[15] = 1.3911360000000002;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[16] = 1.759128;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[17] = 2.328717;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[18] = 2.9280899999999996;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[19] = 3.520232;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[20] = 4.124123999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min( double kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[0] = 7.22585e-07;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[1] = 0.00044146960000000004;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[2] = 0.0034781999999999994;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[3] = 0.01041232;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[4] = 0.02082305;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[5] = 0.050234039999999994;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[6] = 0.06830208;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[7] = 0.10917059999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[8] = 0.1783025;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[9] = 0.20293499999999998;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[10] = 0.2284986;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[11] = 0.36407;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[12] = 0.5077094;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[13] = 0.6617221;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[14] = 1.13781;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[15] = 1.304864;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[16] = 1.6368719999999999;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[17] = 2.145283;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[18] = 2.67591;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[19] = 3.197768;	 // [pb]
	kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[20] = 3.7238759999999997;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwm_NLOQCD_nCT15xxC12_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
