// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxa_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[0] = 5.982e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[1] = 3.513e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[2] = 0.0005269;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[3] = 0.002284;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[4] = 0.006147;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[5] = 0.02214;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[6] = 0.0355;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[7] = 0.07461;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[8] = 0.1634;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[9] = 0.2018;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[10] = 0.2426;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[11] = 0.5201;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[12] = 0.9153;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[13] = 1.387;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[14] = 3.351;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[15] = 4.199;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[16] = 5.88;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[17] = 9.001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[18] = 12.65;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[19] = 16.23;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[20] = 20.48;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[0] = 7.818474e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[1] = 4.124261999999999e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[2] = 0.0006011928999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[3] = 0.002587772;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[4] = 0.0069584040000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[5] = 0.02477466;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[6] = 0.03983099999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[7] = 0.08393624999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[8] = 0.18251779999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[9] = 0.22520880000000004;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[10] = 0.2695286;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[11] = 0.5757507;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[12] = 1.0114065;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[13] = 1.5229260000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[14] = 3.659292;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[15] = 4.593706;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[16] = 6.3974400000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[17] = 9.775086;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[18] = 13.737900000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[19] = 17.5284;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[20] = 22.17984;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[0] = 4.480518e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[1] = 2.901738e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[2] = 0.00044733809999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[3] = 0.001957388;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[4] = 0.00528642;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[5] = 0.01923966;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[6] = 0.0308495;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[7] = 0.0649107;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[8] = 0.14330179999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[9] = 0.17718040000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[10] = 0.21373060000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[11] = 0.46132870000000004;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[12] = 0.814617;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[13] = 1.244139;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[14] = 3.032655;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[15] = 3.804294;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[16] = 5.35668;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[17] = 8.235915;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[18] = 11.6127;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[19] = 14.980290000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[20] = 18.90304;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[0] = 5.982e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[1] = 3.513e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[2] = 0.0005269;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[3] = 0.002284;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[4] = 0.006147;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[5] = 0.02214;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[6] = 0.0355;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[7] = 0.07461;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[8] = 0.1634;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[9] = 0.2018;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[10] = 0.2426;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[11] = 0.5201;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[12] = 0.9153;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[13] = 1.387;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[14] = 3.351;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[15] = 4.199;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[16] = 5.88;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[17] = 9.001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[18] = 12.65;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[19] = 16.23;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[20] = 20.48;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[0] = 9.720750000000001e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[1] = 4.524744e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[2] = 0.0006591518999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[3] = 0.00283216;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[4] = 0.007579251;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[5] = 0.02687796;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[6] = 0.042777499999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[7] = 0.08848745999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[8] = 0.1897074;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[9] = 0.2326754;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[10] = 0.2780196;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[11] = 0.5809517;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[12] = 1.0031688;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[13] = 1.4979600000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[14] = 3.5051460000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[15] = 4.36696;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[16] = 6.074039999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[17] = 9.298032999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[18] = 13.105400000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[19] = 16.96035;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[20] = 21.504;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[0] = 2.24325e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[1] = 2.5012559999999996e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[2] = 0.0003946481;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[3] = 0.00173584;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[4] = 0.004714749;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[5] = 0.01740204;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[6] = 0.028222499999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[7] = 0.060732539999999995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[8] = 0.13709259999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[9] = 0.1709246;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[10] = 0.20718040000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[11] = 0.4592483;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[12] = 0.8274312;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[13] = 1.27604;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[14] = 3.1968539999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[15] = 4.03104;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[16] = 5.68596;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[17] = 8.703966999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[18] = 12.1946;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[19] = 15.499649999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[20] = 19.456;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[0] = 2.918e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[1] = 2.029e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[2] = 0.0003223;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[3] = 0.001423;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[4] = 0.003826;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[5] = 0.01388;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[6] = 0.02193;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[7] = 0.04551;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[8] = 0.1005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[9] = 0.1259;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[10] = 0.1532;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[11] = 0.3309;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[12] = 0.573;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[13] = 0.8871;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[14] = 2.119;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[15] = 2.647;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[16] = 3.804;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[17] = 5.752;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[18] = 7.956;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[19] = 10.45;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[20] = 13.08;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[0] = 4.805946e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[1] = 2.980601e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[2] = 0.00045637679999999993;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[3] = 0.0019779700000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[4] = 0.0052569240000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[5] = 0.01879352;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[6] = 0.02953971;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[7] = 0.06071034000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[8] = 0.13245900000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[9] = 0.1654326;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[10] = 0.200692;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[11] = 0.4271919;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[12] = 0.7305749999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[13] = 1.1195202;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[14] = 2.6127270000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[15] = 3.242575;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[16] = 4.606644;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[17] = 6.879391999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[18] = 9.411948;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[19] = 12.29965;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[20] = 15.421320000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[0] = 1.864602e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[1] = 1.432474e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[2] = 0.00023495669999999997;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[3] = 0.001054443;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[4] = 0.002865674;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[5] = 0.01052104;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[6] = 0.016688730000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[7] = 0.03495168;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[8] = 0.07788750000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[9] = 0.09782430000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[10] = 0.11934280000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[11] = 0.2610801;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[12] = 0.457254;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[13] = 0.7132284000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[14] = 1.739699;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[15] = 2.186422;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[16] = 3.1725359999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[17] = 4.848935999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[18] = 6.770556;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[19] = 8.966099999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[20] = 11.15724;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[0] = 2.918e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[1] = 2.029e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[2] = 0.0003223;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[3] = 0.001423;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[4] = 0.003826;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[5] = 0.01388;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[6] = 0.02193;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[7] = 0.04551;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[8] = 0.1005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[9] = 0.1259;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[10] = 0.1532;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[11] = 0.3309;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[12] = 0.573;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[13] = 0.8871;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[14] = 2.119;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[15] = 2.647;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[16] = 3.804;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[17] = 5.752;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[18] = 7.956;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[19] = 10.45;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[20] = 13.08;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[0] = 4.689226e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[1] = 2.5829170000000005e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[2] = 0.0003986851;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[3] = 0.001747444;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[4] = 0.004679198000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[5] = 0.01680868;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[6] = 0.026381790000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[7] = 0.05397486;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[8] = 0.11688150000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[9] = 0.14554040000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[10] = 0.17618;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[11] = 0.3709389;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[12] = 0.629154;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[13] = 0.9589551;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[14] = 2.2207120000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[15] = 2.7555269999999994;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[16] = 3.9333359999999997;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[17] = 5.9475679999999995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[18] = 8.266284;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[19] = 10.941149999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[20] = 13.78632;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[0] = 1.146774e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[1] = 1.475083e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[2] = 0.0002459149;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[3] = 0.001098556;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[4] = 0.002972802;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[5] = 0.010951319999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[6] = 0.01747821;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[7] = 0.03704514;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[8] = 0.0841185;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[9] = 0.10625960000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[10] = 0.13022;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[11] = 0.29086110000000004;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[12] = 0.5168459999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[13] = 0.8152449;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[14] = 2.017288;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[15] = 2.5384729999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[16] = 3.674664;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[17] = 5.556432;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[18] = 7.645716;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[19] = 9.958849999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[20] = 12.37368;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_nCT15Pb208_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
