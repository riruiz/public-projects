// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttx_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[0] = 0.02;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[1] = 1.741;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[2] = 9.469;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[3] = 25.95;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[4] = 52.89;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[5] = 140.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[6] = 202.1;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[7] = 357.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[8] = 670.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[9] = 797.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[10] = 935.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[11] = 1726.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[12] = 2718.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[13] = 3816.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[14] = 7935.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[15] = 9608.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[16] = 12970.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[17] = 18520.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[18] = 24620.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[19] = 31090.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[20] = 37740.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[0] = 0.02358;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[1] = 1.9412150000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[2] = 10.463244999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[3] = 28.5969;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[4] = 58.23189;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[5] = 154.03520000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[6] = 221.09740000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[7] = 392.2584;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[8] = 729.1776000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[9] = 870.1816;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[10] = 1019.695;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[11] = 1867.5320000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[12] = 2940.876;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[13] = 4109.831999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[14] = 8490.45;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[15] = 10299.776;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[16] = 13916.81;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[17] = 19686.76;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[18] = 26343.4;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[19] = 33141.94;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[20] = 40004.4;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[0] = 0.01628;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[1] = 1.495519;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[2] = 8.23803;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[3] = 22.6803;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[4] = 46.43742;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[5] = 124.74880000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[6] = 179.4648;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[7] = 318.8889;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[8] = 602.5098;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[9] = 717.0424;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[10] = 841.95;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[11] = 1568.934;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[12] = 2481.534;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[13] = 3506.904;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[14] = 7363.679999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[15] = 8925.832;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[16] = 12075.070000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[17] = 17168.04;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[18] = 22847.359999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[19] = 28696.07;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[20] = 34683.060000000005;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[0] = 0.02;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[1] = 1.741;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[2] = 9.469;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[3] = 25.95;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[4] = 52.89;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[5] = 140.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[6] = 202.1;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[7] = 357.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[8] = 670.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[9] = 797.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[10] = 935.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[11] = 1726.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[12] = 2718.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[13] = 3816.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[14] = 7935.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[15] = 9608.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[16] = 12970.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[17] = 18520.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[18] = 24620.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[19] = 31090.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[20] = 37740.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[0] = 0.025099999999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[1] = 2.078754;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[2] = 11.173419999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[3] = 30.205799999999996;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[4] = 60.71772000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[5] = 157.41440000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[6] = 223.72469999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[7] = 389.03729999999996;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[8] = 713.763;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[9] = 845.456;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[10] = 986.9525;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[11] = 1789.8619999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[12] = 2791.386;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[13] = 3911.3999999999996;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[14] = 8228.595;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[15] = 10001.928;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[16] = 13579.589999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[17] = 19594.16;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[18] = 26195.68;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[19] = 33266.3;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[20] = 40608.240000000005;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[0] = 0.0149;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[1] = 1.4032460000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[2] = 7.7645800000000005;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[3] = 21.694200000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[4] = 45.06228;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[5] = 124.18560000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[6] = 180.4753;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[7] = 326.7627;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[8] = 626.6370000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[9] = 749.744;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[10] = 884.0474999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[11] = 1662.138;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[12] = 2644.614;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[13] = 3720.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[14] = 7641.405;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[15] = 9214.072;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[16] = 12360.41;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[17] = 17445.84;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[18] = 23044.32;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[19] = 28913.699999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[20] = 34871.76;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[0] = 0.01225;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[1] = 1.169;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[2] = 6.365;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[3] = 17.46;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[4] = 35.63;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[5] = 94.47;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[6] = 134.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[7] = 240.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[8] = 452.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[9] = 537.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[10] = 621.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[11] = 1152.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[12] = 1815.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[13] = 2585.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[14] = 5378.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[15] = 6452.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[16] = 8790.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[17] = 12440.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[18] = 16500.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[19] = 20780.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[20] = 25370.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[0] = 0.0190855;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[1] = 1.6868670000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[2] = 8.949190000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[3] = 24.147180000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[4] = 48.706210000000006;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[5] = 126.58980000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[6] = 179.1492;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[7] = 315.579;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[8] = 581.9813999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[9] = 688.6656000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[10] = 791.9184;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[11] = 1438.8480000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[12] = 2232.45;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[13] = 3138.19;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[14] = 6485.8679999999995;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[15] = 7826.276000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[16] = 10767.75;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[17] = 15438.04;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[18] = 20674.500000000004;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[19] = 26245.14;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[20] = 32270.64;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[0] = 0.00829325;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[1] = 0.8451870000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[2] = 4.69737;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[3] = 13.060080000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[4] = 26.900650000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[5] = 72.36402;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[6] = 103.9308;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[7] = 187.90200000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[8] = 358.1424;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[9] = 427.39200000000005;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[10] = 496.0368;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[11] = 935.4240000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[12] = 1493.745;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[13] = 2150.72;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[14] = 4490.63;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[15] = 5348.708;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[16] = 7199.009999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[17] = 10039.08;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[18] = 13167.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[19] = 16436.98;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[20] = 19890.08;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[0] = 0.01225;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[1] = 1.169;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[2] = 6.365;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[3] = 17.46;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[4] = 35.63;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[5] = 94.47;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[6] = 134.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[7] = 240.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[8] = 452.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[9] = 537.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[10] = 621.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[11] = 1152.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[12] = 1815.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[13] = 2585.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[14] = 5378.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[15] = 6452.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[16] = 8790.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[17] = 12440.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[18] = 16500.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[19] = 20780.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[20] = 25370.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[0] = 0.015202250000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[1] = 1.377082;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[2] = 7.43432;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[3] = 20.18376;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[4] = 40.72509;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[5] = 105.61746000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[6] = 149.2236;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[7] = 262.3401;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[8] = 482.94960000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[9] = 570.9312000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[10] = 657.0312;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[11] = 1195.776;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[12] = 1867.6349999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[13] = 2652.21;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[14] = 5571.608;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[15] = 6710.08;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[16] = 9220.71;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[17] = 13161.52;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[18] = 17572.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[19] = 22276.16;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[20] = 27323.489999999998;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[0] = 0.00929775;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[1] = 0.9609179999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[2] = 5.29568;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[3] = 14.73624;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[4] = 30.53491;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[5] = 83.32254;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[6] = 120.37640000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[7] = 219.4599;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[8] = 421.45039999999995;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[9] = 504.2688;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[10] = 586.1688;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[11] = 1108.224;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[12] = 1762.365;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[13] = 2517.79;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[14] = 5184.392;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[15] = 6193.92;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[16] = 8359.289999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[17] = 11718.48;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[18] = 15427.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[19] = 19283.84;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[20] = 23416.510000000002;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Xe131_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
