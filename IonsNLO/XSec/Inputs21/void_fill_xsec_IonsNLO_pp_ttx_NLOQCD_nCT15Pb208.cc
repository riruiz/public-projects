// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttx_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[0] = 0.01825;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[1] = 1.626;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[2] = 8.91;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[3] = 24.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[4] = 50.46;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[5] = 136.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[6] = 195.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[7] = 348.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[8] = 660.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[9] = 788.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[10] = 918.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[11] = 1715.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[12] = 2709.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[13] = 3872.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[14] = 8081.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[15] = 9738.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[16] = 13190.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[17] = 18970.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[18] = 25110.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[19] = 31850.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[20] = 39330.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[0] = 0.02144375;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[1] = 1.8097379999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[2] = 9.872280000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[3] = 27.048000000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[4] = 55.707840000000004;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[5] = 148.92;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[6] = 214.13439999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[7] = 380.016;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[8] = 723.2475;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[9] = 863.648;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[10] = 997.8168000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[11] = 1862.4900000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[12] = 2931.1380000000004;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[13] = 4189.504;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[14] = 8695.156;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[15] = 10487.826;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[16] = 14179.25;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[17] = 20203.05;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[18] = 26968.140000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[19] = 34079.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[20] = 42122.43;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[0] = 0.014910249999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[1] = 1.4016119999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[2] = 7.74279;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[3] = 21.413;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[4] = 44.25342;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[5] = 120.36;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[6] = 172.94719999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[7] = 310.068;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[8] = 590.487;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[9] = 705.26;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[10] = 827.8388;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[11] = 1553.79;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[12] = 2470.608;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[13] = 3542.88;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[14] = 7474.925;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[15] = 9017.387999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[16] = 12266.699999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[17] = 17585.190000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[18] = 23352.3;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[19] = 29493.1;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[20] = 36144.270000000004;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[0] = 0.01825;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[1] = 1.626;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[2] = 8.91;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[3] = 24.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[4] = 50.46;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[5] = 136.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[6] = 195.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[7] = 348.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[8] = 660.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[9] = 788.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[10] = 918.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[11] = 1715.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[12] = 2709.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[13] = 3872.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[14] = 8081.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[15] = 9738.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[16] = 13190.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[17] = 18970.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[18] = 25110.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[19] = 31850.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[20] = 39330.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[0] = 0.023962249999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[1] = 2.030874;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[2] = 11.00385;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[3] = 29.767500000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[4] = 60.14832;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[5] = 156.808;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[6] = 221.9424;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[7] = 386.28000000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[8] = 714.6610000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[9] = 847.0999999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[10] = 981.2784;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[11] = 1790.46;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[12] = 2795.688;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[13] = 3984.2879999999996;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[14] = 8428.483;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[15] = 10205.424;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[16] = 13955.02;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[17] = 20240.989999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[18] = 26993.25;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[19] = 34493.549999999996;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[20] = 42712.380000000005;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[0] = 0.01253775;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[1] = 1.221126;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[2] = 6.81615;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[3] = 19.2325;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[4] = 40.77168;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[5] = 115.192;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[6] = 168.45759999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[7] = 309.72;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[8] = 606.339;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[9] = 728.9000000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[10] = 856.3215999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[11] = 1639.54;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[12] = 2622.312;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[13] = 3759.712;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[14] = 7733.517;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[15] = 9270.576;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[16] = 12424.98;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[17] = 17699.010000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[18] = 23226.75;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[19] = 29206.45;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[20] = 35947.62;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[0] = 0.01121;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[1] = 1.09;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[2] = 6.002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[3] = 16.68;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[4] = 33.88;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[5] = 91.19;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[6] = 132.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[7] = 234.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[8] = 446.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[9] = 531.4;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[10] = 623.7;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[11] = 1160.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[12] = 1820.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[13] = 2595.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[14] = 5429.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[15] = 6514.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[16] = 8839.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[17] = 12620.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[18] = 16810.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[19] = 21230.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[20] = 25910.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[0] = 0.017364289999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[1] = 1.56851;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[2] = 8.420806;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[3] = 23.06844;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[4] = 46.31396;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[5] = 122.37698;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[6] = 176.6232;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[7] = 308.65860000000004;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[8] = 575.786;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[9] = 682.8489999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[10] = 797.0886;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[11] = 1453.48;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[12] = 2245.88;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[13] = 3160.71;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[14] = 6541.945000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[15] = 7881.94;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[16] = 10810.097000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[17] = 15610.94;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[18] = 21012.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[19] = 26771.030000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[20] = 32905.7;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[0] = 0.007622799999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[1] = 0.79025;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[2] = 4.441479999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[3] = 12.47664;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[4] = 25.579400000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[5] = 69.85154;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[6] = 102.102;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[7] = 182.9871;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[8] = 352.34000000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[9] = 421.4002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[10] = 496.46520000000004;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[11] = 939.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[12] = 1494.2199999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[13] = 2153.85;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[14] = 4544.072999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[15] = 5413.134;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[16] = 7256.8189999999995;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[17] = 10222.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[18] = 13448.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[19] = 16814.16;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[20] = 20365.260000000002;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[0] = 0.01121;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[1] = 1.09;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[2] = 6.002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[3] = 16.68;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[4] = 33.88;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[5] = 91.19;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[6] = 132.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[7] = 234.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[8] = 446.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[9] = 531.4;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[10] = 623.7;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[11] = 1160.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[12] = 1820.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[13] = 2595.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[14] = 5429.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[15] = 6514.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[16] = 8839.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[17] = 12620.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[18] = 16810.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[19] = 21230.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[20] = 25910.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[0] = 0.01452816;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[1] = 1.3407;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[2] = 7.322439999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[3] = 20.0994;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[4] = 40.21556;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[5] = 105.14207;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[6] = 151.0314;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[7] = 261.6786;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[8] = 483.90999999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[9] = 572.8492;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[10] = 667.359;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[11] = 1213.3600000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[12] = 1880.06;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[13] = 2672.85;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[14] = 5662.446999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[15] = 6833.186;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[16] = 9351.662;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[17] = 13490.779999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[18] = 18121.18;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[19] = 23034.55;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[20] = 28267.809999999998;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[0] = 0.007891839999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[1] = 0.8393;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[2] = 4.68156;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[3] = 13.2606;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[4] = 27.54444;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[5] = 77.23792999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[6] = 114.1686;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[7] = 208.1214;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[8] = 408.09000000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[9] = 489.9508;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[10] = 580.041;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[11] = 1106.6399999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[12] = 1759.94;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[13] = 2517.15;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[14] = 5195.553;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[15] = 6194.813999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[16] = 8326.338;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[17] = 11749.220000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[18] = 15498.820000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[19] = 19425.45;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[20] = 23552.190000000002;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15Pb208_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
