// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxz_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[0] = 1.281e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[1] = 0.0008241;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[2] = 0.006985;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[3] = 0.02263;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[4] = 0.04993;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[5] = 0.1441;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[6] = 0.213;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[7] = 0.395;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[8] = 0.7891;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[9] = 0.9457;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[10] = 1.123;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[11] = 2.227;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[12] = 3.681;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[13] = 5.522;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[14] = 12.68;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[15] = 15.47;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[16] = 21.51;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[17] = 32.83;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[18] = 45.94;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[19] = 58.87;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[20] = 74.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[0] = 1.547448e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[1] = 0.0009213438000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[2] = 0.0076555600000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[3] = 0.02464407;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[4] = 0.054223980000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[5] = 0.1563485;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[6] = 0.23110499999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[7] = 0.428575;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[8] = 0.8561735;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[9] = 1.0260844999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[10] = 1.218455;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[11] = 2.414068;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[12] = 3.986523;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[13] = 5.985848000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[14] = 13.732439999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[15] = 16.676660000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[16] = 23.08023;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[17] = 35.19376;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[18] = 49.43144;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[19] = 62.93203;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[20] = 79.106;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[0] = 1.023519e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[1] = 0.0007079019;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[2] = 0.006139815;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[3] = 0.02005018;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[4] = 0.04448763;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[5] = 0.12896950000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[6] = 0.190848;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[7] = 0.35471;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[8] = 0.71019;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[9] = 0.8520757;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[10] = 1.0129460000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[11] = 2.017662;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[12] = 3.3423480000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[13] = 5.0250200000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[14] = 11.6022;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[15] = 14.216930000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[16] = 19.87524;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[17] = 30.43341;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[18] = 42.58638;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[19] = 54.92571;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[20] = 69.116;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[0] = 1.281e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[1] = 0.0008241;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[2] = 0.006985;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[3] = 0.02263;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[4] = 0.04993;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[5] = 0.1441;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[6] = 0.213;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[7] = 0.395;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[8] = 0.7891;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[9] = 0.9457;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[10] = 1.123;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[11] = 2.227;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[12] = 3.681;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[13] = 5.522;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[14] = 12.68;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[15] = 15.47;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[16] = 21.51;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[17] = 32.83;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[18] = 45.94;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[19] = 58.87;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[20] = 74.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[0] = 1.4744310000000002e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[1] = 0.0009007413;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[2] = 0.00751586;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[3] = 0.024214100000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[4] = 0.05332524;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[5] = 0.1534665;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[6] = 0.22641899999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[7] = 0.4187;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[8] = 0.8317114000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[9] = 0.9948764;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[10] = 1.1791500000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[11] = 2.318307;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[12] = 3.8024729999999995;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[13] = 5.671094;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[14] = 12.85752;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[15] = 15.65564;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[16] = 21.70359;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[17] = 33.191129999999994;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[18] = 46.629099999999994;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[19] = 59.98852999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[20] = 75.702;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[0] = 1.087569e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[1] = 0.0007474587;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[2] = 0.006454140000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[3] = 0.0210459;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[4] = 0.04653476;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[5] = 0.1347335;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[6] = 0.199581;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[7] = 0.3713;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[8] = 0.7464886;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[9] = 0.8965236;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[10] = 1.0668499999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[11] = 2.135693;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[12] = 3.559527;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[13] = 5.372906;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[14] = 12.50248;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[15] = 15.284360000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[16] = 21.31641;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[17] = 32.468869999999995;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[18] = 45.250899999999994;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[19] = 57.75147;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[20] = 72.298;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[0] = 7.263e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[1] = 0.0005353;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[2] = 0.004708;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[3] = 0.01544;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[4] = 0.03425;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[5] = 0.09854;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[6] = 0.1454;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[7] = 0.2678;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[8] = 0.5301;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[9] = 0.6369;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[10] = 0.7499;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[11] = 1.487;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[12] = 2.463;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[13] = 3.644;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[14] = 8.347;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[15] = 10.25;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[16] = 14.35;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[17] = 21.72;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[18] = 29.87;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[19] = 39.18;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[20] = 49.03;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[0] = 1.1381121000000002e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[1] = 0.0007665495999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[2] = 0.006515872;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[3] = 0.020967520000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[4] = 0.045963500000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[5] = 0.13026988;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[6] = 0.19105560000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[7] = 0.3486756;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[8] = 0.6827688000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[9] = 0.8171427;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[10] = 0.9591221;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[11] = 1.876594;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[12] = 3.073824;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[13] = 4.507628;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[14] = 10.09987;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[15] = 12.341;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[16] = 17.1052;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[17] = 25.69476;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[18] = 35.42582;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[19] = 46.780919999999995;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[20] = 58.88503000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[0] = 4.858947e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[1] = 0.00038809249999999994;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[2] = 0.0035121680000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[3] = 0.01171896;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[4] = 0.026235500000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[5] = 0.07646704;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[6] = 0.113412;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[7] = 0.2104908;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[8] = 0.42036930000000006;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[9] = 0.5063355;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[10] = 0.5976703;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[11] = 1.198522;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[12] = 2.004882;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[13] = 2.9880800000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[14] = 6.969745;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[15] = 8.59975;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[16] = 12.140099999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[17] = 18.5706;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[18] = 25.35963;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[19] = 32.98956;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[20] = 41.038109999999996;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[0] = 7.263e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[1] = 0.0005353;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[2] = 0.004708;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[3] = 0.01544;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[4] = 0.03425;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[5] = 0.09854;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[6] = 0.1454;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[7] = 0.2678;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[8] = 0.5301;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[9] = 0.6369;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[10] = 0.7499;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[11] = 1.487;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[12] = 2.463;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[13] = 3.644;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[14] = 8.347;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[15] = 10.25;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[16] = 14.35;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[17] = 21.72;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[18] = 29.87;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[19] = 39.18;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[20] = 49.03;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[0] = 8.374239000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[1] = 0.0005850828999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[2] = 0.0050611;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[3] = 0.01648992;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[4] = 0.03647625;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[5] = 0.10474802;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[6] = 0.15441480000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[7] = 0.28360019999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[8] = 0.5587254;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[9] = 0.6700188;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[10] = 0.7873950000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[11] = 1.547967;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[12] = 2.546742;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[13] = 3.746032;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[14] = 8.472204999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[15] = 10.373;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[16] = 14.479149999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[17] = 21.958919999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[18] = 30.31805;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[19] = 39.9636;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[20] = 50.157689999999995;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[0] = 6.151761e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[1] = 0.00048551709999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[2] = 0.004354900000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[3] = 0.01439008;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[4] = 0.032023750000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[5] = 0.09233198000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[6] = 0.13638519999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[7] = 0.2519998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[8] = 0.5014746;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[9] = 0.6037812;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[10] = 0.712405;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[11] = 1.426033;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[12] = 2.379258;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[13] = 3.5419680000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[14] = 8.221795;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[15] = 10.127;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[16] = 14.220849999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[17] = 21.48108;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[18] = 29.42195;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[19] = 38.3964;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[20] = 47.90231;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15xxC12_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
