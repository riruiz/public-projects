// today is: 2024 5 28
// ////////////////
//  IonsNLO_pp_ttxh_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[0] = 4.476e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[1] = 0.0003964;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[2] = 0.003563;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[3] = 0.01206;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[4] = 0.02722;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[5] = 0.0811;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[6] = 0.1204;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[7] = 0.2257;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[8] = 0.4513;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[9] = 0.5425;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[10] = 0.648;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[11] = 1.279;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[12] = 2.134;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[13] = 3.161;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[14] = 7.261;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[15] = 8.893;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[16] = 12.65;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[17] = 19.1;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[18] = 26.47;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[19] = 34.62;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[20] = 43.42;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[0] = 5.263776e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[1] = 0.0004253372;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[2] = 0.003737587;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[3] = 0.01263888;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[4] = 0.028472120000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[5] = 0.0850739;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[6] = 0.1265404;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[7] = 0.2378878;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[8] = 0.47702409999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[9] = 0.571795;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[10] = 0.68688;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[11] = 1.354461;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[12] = 2.266308;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[13] = 3.360143;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[14] = 7.725704;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[15] = 9.453259000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[16] = 13.49755;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[17] = 20.322400000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[18] = 28.243489999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[19] = 36.939539999999994;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[20] = 46.285720000000005;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[0] = 3.5673719999999996e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[1] = 0.0003436788;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[2] = 0.0031675070000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[3] = 0.01080576;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[4] = 0.02452522;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[5] = 0.0734766;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[6] = 0.1092028;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[7] = 0.20516130000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[8] = 0.4115856;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[9] = 0.495845;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[10] = 0.591624;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[11] = 1.1741219999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[12] = 1.961146;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[13] = 2.9112810000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[14] = 6.7309470000000005;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[15] = 8.261597000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[16] = 11.751850000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[17] = 17.820300000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[18] = 24.670039999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[19] = 32.16198;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[20] = 40.20692;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[0] = 4.476e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[1] = 0.0003964;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[2] = 0.003563;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[3] = 0.01206;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[4] = 0.02722;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[5] = 0.0811;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[6] = 0.1204;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[7] = 0.2257;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[8] = 0.4513;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[9] = 0.5425;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[10] = 0.648;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[11] = 1.279;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[12] = 2.134;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[13] = 3.161;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[14] = 7.261;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[15] = 8.893;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[16] = 12.65;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[17] = 19.1;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[18] = 26.47;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[19] = 34.62;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[20] = 43.42;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[0] = 7.098935999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[1] = 0.0004661664;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[2] = 0.003926426;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[3] = 0.01300068;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[4] = 0.029016520000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[5] = 0.08547940000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[6] = 0.12642;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[7] = 0.2358565;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[8] = 0.46890069999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[9] = 0.5625724999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[10] = 0.671328;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[11] = 1.31737;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[12] = 2.1916179999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[13] = 3.2400249999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[14] = 7.420742000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[15] = 9.088646;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[16] = 12.91565;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[17] = 19.482000000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[18] = 27.025869999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[19] = 35.34701999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[20] = 44.33182;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[0] = 2.61846e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[1] = 0.0003341652;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[2] = 0.0032138260000000004;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[3] = 0.0111555;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[4] = 0.02553236;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[5] = 0.077045;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[6] = 0.1148616;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[7] = 0.2164463;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[8] = 0.4350532;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[9] = 0.5235124999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[10] = 0.625968;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[11] = 1.24063;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[12] = 2.0763819999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[13] = 3.078814;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[14] = 7.093997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[15] = 8.697354;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[16] = 12.3717;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[17] = 18.6798;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[18] = 25.887659999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[19] = 33.85836;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[20] = 42.42134;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[0] = 2.809e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[1] = 0.000294;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[2] = 0.00275;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[3] = 0.009387;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[4] = 0.02128;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[5] = 0.06276;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[6] = 0.09289;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[7] = 0.1729;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[8] = 0.3418;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[9] = 0.4116;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[10] = 0.4873;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[11] = 0.9586;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[12] = 1.574;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[13] = 2.327;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[14] = 5.272;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[15] = 6.481;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[16] = 9.08;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[17] = 13.6;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[18] = 18.68;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[19] = 24.42;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[20] = 30.44;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[0] = 4.4972089999999996e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[1] = 0.00042777;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[2] = 0.00386375;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[3] = 0.012935285999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[4] = 0.0289408;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[5] = 0.08378459999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[6] = 0.12317214000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[7] = 0.22667189999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[8] = 0.4419474;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[9] = 0.5301408000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[10] = 0.6256932000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[11] = 1.2107118;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[12] = 1.9659260000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[13] = 2.876172;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[14] = 6.368576;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[15] = 7.790162;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[16] = 10.86876;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[17] = 16.4424;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[18] = 22.770919999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[19] = 29.96334;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[20] = 37.562960000000004;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[0] = 1.834277e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[1] = 0.000209034;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[2] = 0.0020157499999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[3] = 0.0070027019999999995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[4] = 0.0160664;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[5] = 0.04813692;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[6] = 0.07161819;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[7] = 0.1346891;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[8] = 0.26933840000000003;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[9] = 0.3255756;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[10] = 0.3864289;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[11] = 0.7707144;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[12] = 1.2780880000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[13] = 1.9081400000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[14] = 4.407392000000001;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[15] = 5.444039999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[16] = 7.65444;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[17] = 11.3288;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[18] = 15.411;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[19] = 19.99998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[20] = 24.74772;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[0] = 2.809e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[1] = 0.000294;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[2] = 0.00275;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[3] = 0.009387;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[4] = 0.02128;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[5] = 0.06276;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[6] = 0.09289;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[7] = 0.1729;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[8] = 0.3418;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[9] = 0.4116;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[10] = 0.4873;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[11] = 0.9586;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[12] = 1.574;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[13] = 2.327;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[14] = 5.272;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[15] = 6.481;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[16] = 9.08;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[17] = 13.6;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[18] = 18.68;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[19] = 24.42;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[20] = 30.44;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[0] = 4.469119e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[1] = 0.000347214;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[2] = 0.0030305;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[3] = 0.010100412;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[4] = 0.02262064;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[5] = 0.06596076;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[6] = 0.09725582999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[7] = 0.1801618;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[8] = 0.3541048;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[9] = 0.426006;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[10] = 0.5033809;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[11] = 0.9854408;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[12] = 1.614924;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[13] = 2.382848;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[14] = 5.382712;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[15] = 6.617100999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[16] = 9.2616;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[17] = 13.872;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[18] = 19.07228;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[19] = 24.93282;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[20] = 31.10968;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min( double kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[0] = 1.632029e-07;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[1] = 0.00024637199999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[2] = 0.0024749999999999998;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[3] = 0.008673588;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[4] = 0.01993936;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[5] = 0.059684759999999996;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[6] = 0.08870995;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[7] = 0.165984;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[8] = 0.329837;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[9] = 0.3976056;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[10] = 0.47170639999999997;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[11] = 0.9308006;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[12] = 1.533076;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[13] = 2.268825;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[14] = 5.156016;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[15] = 6.338418;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[16] = 8.88024;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[17] = 13.300799999999999;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[18] = 18.26904;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[19] = 23.858340000000002;	 // [pb]
	kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[20] = 29.73988;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxh_NLOQCD_CT18nloxH1_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
