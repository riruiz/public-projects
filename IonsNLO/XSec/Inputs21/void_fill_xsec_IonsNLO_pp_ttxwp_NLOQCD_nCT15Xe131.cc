// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxwp_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[0] = 1.221e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[1] = 0.0006245;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[2] = 0.004706;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[3] = 0.01385;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[4] = 0.02777;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[5] = 0.06856;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[6] = 0.0948;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[7] = 0.1559;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[8] = 0.2644;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[9] = 0.3058;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[10] = 0.3518;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[11] = 0.6;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[12] = 0.8861;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[13] = 1.22;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[14] = 2.358;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[15] = 2.826;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[16] = 3.746;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[17] = 5.318;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[18] = 7.005;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[19] = 8.806;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[20] = 10.74;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[0] = 1.470084e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[1] = 0.000698191;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[2] = 0.005143658;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[3] = 0.0150134;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[4] = 0.0299916;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[5] = 0.07411335999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[6] = 0.1029528;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[7] = 0.16993100000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[8] = 0.289518;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[9] = 0.3357684;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[10] = 0.3876836;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[11] = 0.6678;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[12] = 0.9906598000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[13] = 1.3725;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[14] = 2.671614;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[15] = 3.215988;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[16] = 4.270440000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[17] = 6.073155999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[18] = 8.048745;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[19] = 10.197347999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[20] = 12.522839999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[0] = 9.829049999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[1] = 0.000540817;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[2] = 0.004178928;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[3] = 0.01245115;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[4] = 0.02513185;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[5] = 0.06245816;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[6] = 0.086268;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[7] = 0.1420249;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[8] = 0.24113280000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[9] = 0.27858380000000005;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[10] = 0.3197862;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[11] = 0.543;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[12] = 0.8010344;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[13] = 1.098;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[14] = 2.1104100000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[15] = 2.51514;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[16] = 3.303972;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[17] = 4.631977999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[18] = 6.0453149999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[19] = 7.537935999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[20] = 9.129;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[0] = 1.221e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[1] = 0.0006245;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[2] = 0.004706;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[3] = 0.01385;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[4] = 0.02777;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[5] = 0.06856;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[6] = 0.0948;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[7] = 0.1559;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[8] = 0.2644;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[9] = 0.3058;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[10] = 0.3518;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[11] = 0.6;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[12] = 0.8861;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[13] = 1.22;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[14] = 2.358;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[15] = 2.826;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[16] = 3.746;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[17] = 5.318;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[18] = 7.005;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[19] = 8.806;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[20] = 10.74;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[0] = 1.5311340000000001e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[1] = 0.000709432;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[2] = 0.005214248;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[3] = 0.015151900000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[4] = 0.03013045;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[5] = 0.07349632;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[6] = 0.1011516;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[7] = 0.1649422;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[8] = 0.27709120000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[9] = 0.319561;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[10] = 0.36692739999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[11] = 0.6204;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[12] = 0.9109108;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[13] = 1.2505;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[14] = 2.419308;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[15] = 2.899476;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[16] = 3.854634;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[17] = 5.498812;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[18] = 7.27119;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[19] = 9.175851999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[20] = 11.23404;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[0] = 9.10866e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[1] = 0.0005395679999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[2] = 0.004197752;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[3] = 0.0125481;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[4] = 0.02540955;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[5] = 0.06362367999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[6] = 0.0884484;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[7] = 0.1468578;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[8] = 0.2517088;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[9] = 0.292039;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[10] = 0.3366726;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[11] = 0.5796;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[12] = 0.8612892;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[13] = 1.1895;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[14] = 2.296692;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[15] = 2.752524;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[16] = 3.637366;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[17] = 5.137187999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[18] = 6.73881;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[19] = 8.436148;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[20] = 10.24596;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[0] = 6.871e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[1] = 0.0004042;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[2] = 0.003174;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[3] = 0.009415;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[4] = 0.01904;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[5] = 0.04627;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[6] = 0.06284;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[7] = 0.1008;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[8] = 0.1661;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[9] = 0.1903;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[10] = 0.214;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[11] = 0.3425;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[12] = 0.4821;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[13] = 0.6304;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[14] = 1.102;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[15] = 1.261;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[16] = 1.594;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[17] = 2.106;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[18] = 2.633;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[19] = 3.155;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[20] = 3.693;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[0] = 1.0595081999999999e-06;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[1] = 0.0005691136;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[2] = 0.004297596000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[3] = 0.012437215;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[4] = 0.024713920000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[5] = 0.05862409;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[6] = 0.07886419999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[7] = 0.1245888;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[8] = 0.2018115;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[9] = 0.230263;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[10] = 0.257656;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[11] = 0.40689000000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[12] = 0.5780379;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[13] = 0.7627839999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[14] = 1.3587660000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[15] = 1.5623789999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[16] = 1.9925000000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[17] = 2.657772;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[18] = 3.3518090000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[19] = 4.041555;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[20] = 4.756584;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[0] = 4.658537999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[1] = 0.000297087;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[2] = 0.0024154140000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[3] = 0.00732487;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[4] = 0.015041600000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[5] = 0.037339889999999994;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[6] = 0.05108891999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[7] = 0.0830592;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[8] = 0.1390257;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[9] = 0.159852;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[10] = 0.18040199999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[11] = 0.29283750000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[12] = 0.4068924;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[13] = 0.5270144;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[14] = 0.900334;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[15] = 1.0251929999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[16] = 1.28317;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[17] = 1.676376;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[18] = 2.0748040000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[19] = 2.470365;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[20] = 2.8694610000000003;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[0] = 6.871e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[1] = 0.0004042;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[2] = 0.003174;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[3] = 0.009415;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[4] = 0.01904;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[5] = 0.04627;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[6] = 0.06284;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[7] = 0.1008;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[8] = 0.1661;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[9] = 0.1903;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[10] = 0.214;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[11] = 0.3425;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[12] = 0.4821;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[13] = 0.6304;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[14] = 1.102;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[15] = 1.261;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[16] = 1.594;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[17] = 2.106;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[18] = 2.633;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[19] = 3.155;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[20] = 3.693;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[0] = 8.664331e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[1] = 0.0004599796;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[2] = 0.0035199660000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[3] = 0.01030001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[4] = 0.020639360000000002;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[5] = 0.0495089;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[6] = 0.06686175999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[7] = 0.106344;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[8] = 0.1737406;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[9] = 0.1986732;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[10] = 0.22277399999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[11] = 0.3544875;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[12] = 0.4984914;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[13] = 0.6530944;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[14] = 1.152692;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[15] = 1.3227889999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[16] = 1.68167;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[17] = 2.2386779999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[18] = 2.814677;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[19] = 3.38847;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[20] = 3.984747;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min( double kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[0] = 5.077668999999999e-07;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[1] = 0.00034842040000000003;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[2] = 0.002828034;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[3] = 0.00852999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[4] = 0.01744064;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[5] = 0.043031099999999996;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[6] = 0.05881823999999999;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[7] = 0.095256;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[8] = 0.1584594;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[9] = 0.1819268;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[10] = 0.205226;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[11] = 0.33051250000000004;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[12] = 0.4657086;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[13] = 0.6077056;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[14] = 1.0513080000000001;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[15] = 1.1992109999999998;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[16] = 1.50633;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[17] = 1.973322;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[18] = 2.451323;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[19] = 2.9215299999999997;	 // [pb]
	kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[20] = 3.401253;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxwp_NLOQCD_nCT15Xe131_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
