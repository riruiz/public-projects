// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttx_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[0] = 0.0236;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[1] = 1.912;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[2] = 10.1;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[3] = 26.83;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[4] = 53.83;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[5] = 141.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[6] = 201.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[7] = 354.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[8] = 660.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[9] = 784.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[10] = 913.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[11] = 1669.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[12] = 2625.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[13] = 3709.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[14] = 7816.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[15] = 9405.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[16] = 12740.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[17] = 18220.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[18] = 24430.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[19] = 30650.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[20] = 37830.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[0] = 0.027966;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[1] = 2.139528;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[2] = 11.150400000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[3] = 29.53983;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[4] = 58.94385;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[5] = 154.6595;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[6] = 219.94559999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[7] = 385.7238;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[8] = 719.6111999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[9] = 853.2096000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[10] = 990.605;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[11] = 1812.534;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[12] = 2832.375;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[13] = 4016.8469999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[14] = 8417.832;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[15] = 10100.970000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[16] = 13695.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[17] = 19458.960000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[18] = 26188.960000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[19] = 32734.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[20] = 40440.27;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[0] = 0.019116;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[1] = 1.6385839999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[2] = 8.786999999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[3] = 23.503079999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[4] = 47.47806;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[5] = 125.652;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[6] = 179.6256;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[7] = 317.009;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[8] = 594.72;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[9] = 706.5642;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[10] = 825.352;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[11] = 1515.452;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[12] = 2401.875;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[13] = 3397.444;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[14] = 7229.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[15] = 8727.84;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[16] = 11848.199999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[17] = 16926.38;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[18] = 22622.179999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[19] = 28289.95;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[20] = 34727.94;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[0] = 0.0236;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[1] = 1.912;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[2] = 10.1;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[3] = 26.83;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[4] = 53.83;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[5] = 141.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[6] = 201.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[7] = 354.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[8] = 660.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[9] = 784.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[10] = 913.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[11] = 1669.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[12] = 2625.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[13] = 3709.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[14] = 7816.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[15] = 9405.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[16] = 12740.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[17] = 18220.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[18] = 24430.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[19] = 30650.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[20] = 37830.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[0] = 0.0264556;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[1] = 2.0917280000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[2] = 11.009;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[3] = 29.13738;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[4] = 58.1364;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[5] = 150.9805;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[6] = 213.89759999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[7] = 372.6184;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[8] = 687.232;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[9] = 812.4312000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[10] = 943.1289999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[11] = 1704.0489999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[12] = 2661.75;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[13] = 3749.7989999999995;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[14] = 7948.871999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[15] = 9602.505;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[16] = 13071.24;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[17] = 18803.04;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[18] = 25333.91;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[19] = 31937.300000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[20] = 39532.35;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[0] = 0.0207444;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[1] = 1.732272;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[2] = 9.191;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[3] = 24.52262;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[4] = 49.5236;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[5] = 132.0195;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[6] = 189.3024;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[7] = 335.78159999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[8] = 634.3679999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[9] = 755.9688;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[10] = 882.871;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[11] = 1633.951;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[12] = 2588.25;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[13] = 3668.201;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[14] = 7683.128;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[15] = 9207.494999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[16] = 12408.76;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[17] = 17636.96;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[18] = 23526.09;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[19] = 29362.699999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[20] = 36127.65;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[0] = 0.01438;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[1] = 1.276;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[2] = 6.774;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[3] = 18.18;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[4] = 36.58;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[5] = 95.78;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[6] = 137.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[7] = 238.3;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[8] = 444.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[9] = 525.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[10] = 603.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[11] = 1127.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[12] = 1768.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[13] = 2501.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[14] = 5227.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[15] = 6274.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[16] = 8466.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[17] = 12170.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[18] = 16190.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[19] = 20430.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[20] = 24950.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[0] = 0.02251908;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[1] = 1.842544;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[2] = 9.503922;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[3] = 25.07022;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[4] = 49.785379999999996;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[5] = 127.67474;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[6] = 181.114;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[7] = 310.5049;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[8] = 569.7888;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[9] = 670.3949999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[10] = 765.9684;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[11] = 1401.988;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[12] = 2167.5679999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[13] = 3028.7110000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[14] = 6308.9890000000005;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[15] = 7616.6359999999995;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[16] = 10379.316;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[17] = 15090.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[18] = 20269.88;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[19] = 25782.66;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[20] = 31711.449999999997;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[0] = 0.009692119999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[1] = 0.921272;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[2] = 5.005986;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[3] = 13.61682;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[4] = 27.69106;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[5] = 73.7506;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[6] = 106.175;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[7] = 186.8272;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[8] = 353.61600000000004;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[9] = 420.1141999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[10] = 484.08720000000005;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[11] = 918.505;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[12] = 1458.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[13] = 2085.834;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[14] = 4359.318;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[15] = 5194.872;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[16] = 6933.6539999999995;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[17] = 9833.36;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[18] = 12935.81;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[19] = 16180.560000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[20] = 19585.75;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[0] = 0.01438;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[1] = 1.276;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[2] = 6.774;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[3] = 18.18;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[4] = 36.58;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[5] = 95.78;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[6] = 137.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[7] = 238.3;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[8] = 444.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[9] = 525.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[10] = 603.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[11] = 1127.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[12] = 1768.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[13] = 2501.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[14] = 5227.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[15] = 6274.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[16] = 8466.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[17] = 12170.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[18] = 16190.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[19] = 20430.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[20] = 24950.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[0] = 0.01609122;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[1] = 1.3882880000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[2] = 7.34979;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[3] = 19.67076;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[4] = 39.39666;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[5] = 102.19726;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[6] = 145.494;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[7] = 250.69160000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[8] = 463.03679999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[9] = 545.7804;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[10] = 624.1224000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[11] = 1151.794;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[12] = 1794.5199999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[13] = 2528.511;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[14] = 5315.8589999999995;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[15] = 6405.753999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[16] = 8694.581999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[17] = 12571.609999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[18] = 16821.41;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[19] = 21308.489999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[20] = 26122.649999999998;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min( double kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[0] = 0.012668780000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[1] = 1.163712;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[2] = 6.19821;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[3] = 16.68924;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[4] = 33.76334;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[5] = 89.36274;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[6] = 128.506;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[7] = 225.9084;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[8] = 426.5632;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[9] = 505.8195999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[10] = 583.0776;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[11] = 1102.206;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[12] = 1741.48;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[13] = 2473.489;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[14] = 5138.141;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[15] = 6142.246;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[16] = 8237.418;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[17] = 11768.39;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[18] = 15558.59;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[19] = 19551.51;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[20] = 23777.35;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_nCT15xxC12_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
