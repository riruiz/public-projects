// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxz_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[0] = 1.137e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[1] = 0.0007747;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[2] = 0.00663;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[3] = 0.02159;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[4] = 0.04821;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[5] = 0.141;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[6] = 0.2081;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[7] = 0.4076;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[8] = 0.7861;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[9] = 0.9463;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[10] = 1.124;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[11] = 2.264;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[12] = 3.775;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[13] = 5.561;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[14] = 12.87;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[15] = 15.75;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[16] = 22.42;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[17] = 33.84;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[18] = 46.22;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[19] = 61.03;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[20] = 76.73;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[0] = 1.3723590000000001e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[1] = 0.0008661146000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[2] = 0.0072664800000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[3] = 0.023446740000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[4] = 0.05230785;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[5] = 0.15312599999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[6] = 0.2253723;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[7] = 0.4495828;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[8] = 0.8552768000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[9] = 1.0286281;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[10] = 1.2229120000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[11] = 2.460968;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[12] = 4.0996500000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[13] = 6.011441;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[14] = 13.912469999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[15] = 16.947000000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[16] = 24.1015;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[17] = 36.411840000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[18] = 49.40918;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[19] = 65.48519;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[20] = 82.40802000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[0] = 9.096000000000002e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[1] = 0.000666242;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[2] = 0.0058277699999999995;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[3] = 0.019171920000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[4] = 0.042955110000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[5] = 0.126054;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[6] = 0.1864576;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[7] = 0.3619488;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[8] = 0.7059178;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[9] = 0.8507237000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[10] = 1.0116;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[11] = 2.0443919999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[12] = 3.42015;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[13] = 5.066071;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[14] = 11.78892;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[15] = 14.49;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[16] = 20.69366;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[17] = 31.302000000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[18] = 43.03082;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[19] = 56.7579;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[20] = 71.43563;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[0] = 1.137e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[1] = 0.0007747;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[2] = 0.00663;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[3] = 0.02159;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[4] = 0.04821;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[5] = 0.141;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[6] = 0.2081;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[7] = 0.4076;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[8] = 0.7861;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[9] = 0.9463;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[10] = 1.124;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[11] = 2.264;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[12] = 3.775;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[13] = 5.561;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[14] = 12.87;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[15] = 15.75;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[16] = 22.42;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[17] = 33.84;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[18] = 46.22;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[19] = 61.03;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[20] = 76.73;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[0] = 1.4803740000000002e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[1] = 0.000898652;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[2] = 0.007544939999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[3] = 0.024439880000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[4] = 0.054380880000000006;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[5] = 0.15792;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[6] = 0.23182340000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[7] = 0.4508056000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[8] = 0.8584212000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[9] = 1.0295744;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[10] = 1.2184160000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[11] = 2.415688;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[12] = 3.97885;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[13] = 5.800122999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[14] = 13.191749999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[15] = 16.0965;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[16] = 22.890819999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[17] = 34.65216;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[18] = 47.65282;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[19] = 63.16605;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[20] = 79.79920000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[0] = 7.93626e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[1] = 0.000650748;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[2] = 0.005715059999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[3] = 0.018740120000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[4] = 0.04203912;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[5] = 0.12407999999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[6] = 0.1843766;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[7] = 0.3643944;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[8] = 0.7137788;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[9] = 0.8630256000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[10] = 1.029584;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[11] = 2.1123119999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[12] = 3.57115;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[13] = 5.321877;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[14] = 12.54825;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[15] = 15.4035;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[16] = 21.949180000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[17] = 33.027840000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[18] = 44.78718;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[19] = 58.89395;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[20] = 73.6608;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[0] = 6.431e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[1] = 0.0005042;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[2] = 0.004483;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[3] = 0.01484;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[4] = 0.03308;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[5] = 0.09651;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[6] = 0.1422;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[7] = 0.2665;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[8] = 0.5278;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[9] = 0.6379;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[10] = 0.7554;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[11] = 1.501;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[12] = 2.499;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[13] = 3.713;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[14] = 8.555;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[15] = 10.54;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[16] = 14.84;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[17] = 22.24;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[18] = 30.78;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[19] = 40.32;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[20] = 50.13;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[0] = 1.0058084000000002e-06;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[1] = 0.0007205018000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[2] = 0.006195506000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[3] = 0.02013788;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[4] = 0.04439336;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[5] = 0.12768273;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[6] = 0.18727739999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[7] = 0.3477825;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[8] = 0.6813898;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[9] = 0.8209773;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[10] = 0.9691781999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[11] = 1.9002659999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[12] = 3.1312470000000006;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[13] = 4.60412;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[14] = 10.385769999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[15] = 12.721779999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[16] = 17.71896;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[17] = 26.309919999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[18] = 36.44352;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[19] = 48.101760000000006;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[20] = 60.156;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[0] = 4.30877e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[1] = 0.0003660492;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[2] = 0.003348801;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[3] = 0.01126356;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[4] = 0.02533928;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[5] = 0.07489176;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[6] = 0.11077379999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[7] = 0.208936;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[8] = 0.4174898000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[9] = 0.5058547;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[10] = 0.600543;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[11] = 1.206804;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[12] = 2.026689;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[13] = 3.037234;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[14] = 7.126314999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[15] = 8.821979999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[16] = 12.5398;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[17] = 18.992959999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[18] = 26.163;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[19] = 33.98976;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[20] = 41.95881;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[0] = 6.431e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[1] = 0.0005042;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[2] = 0.004483;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[3] = 0.01484;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[4] = 0.03308;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[5] = 0.09651;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[6] = 0.1422;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[7] = 0.2665;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[8] = 0.5278;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[9] = 0.6379;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[10] = 0.7554;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[11] = 1.501;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[12] = 2.499;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[13] = 3.713;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[14] = 8.555;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[15] = 10.54;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[16] = 14.84;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[17] = 22.24;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[18] = 30.78;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[19] = 40.32;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[20] = 50.13;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[0] = 8.392455e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[1] = 0.0005838636;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[2] = 0.005083722;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[3] = 0.01670984;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[4] = 0.03711576;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[5] = 0.10760865;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[6] = 0.1581264;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[7] = 0.29394950000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[8] = 0.5758298000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[9] = 0.6940352000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[10] = 0.8188536;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[11] = 1.603068;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[12] = 2.636445;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[13] = 3.880085;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[14] = 8.77743;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[15] = 10.792959999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[16] = 15.16648;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[17] = 22.81824;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[18] = 31.764960000000002;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[19] = 41.852160000000005;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[20] = 52.28559;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min( double kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[0] = 4.469545000000001e-07;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[1] = 0.00042453639999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[2] = 0.003882278;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[3] = 0.012970160000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[4] = 0.02904424;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[5] = 0.08541135;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[6] = 0.12627359999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[7] = 0.23905050000000003;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[8] = 0.47977020000000004;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[9] = 0.5817648000000001;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[10] = 0.6919464;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[11] = 1.3989319999999998;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[12] = 2.361555;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[13] = 3.545915;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[14] = 8.332569999999999;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[15] = 10.28704;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[16] = 14.51352;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[17] = 21.661759999999997;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[18] = 29.79504;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[19] = 38.787839999999996;	 // [pb]
	kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[20] = 47.97441;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxz_NLOQCD_nCT15Xe131_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
