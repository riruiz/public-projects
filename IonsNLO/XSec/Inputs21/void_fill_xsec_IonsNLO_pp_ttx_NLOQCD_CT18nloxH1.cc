// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttx_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[0] = 0.02277;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[1] = 1.817;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[2] = 9.723;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[3] = 26.4;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[4] = 53.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[5] = 139.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[6] = 199.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[7] = 349.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[8] = 642.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[9] = 766.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[10] = 894.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[11] = 1650.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[12] = 2604.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[13] = 3595.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[14] = 7746.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[15] = 9331.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[16] = 12740.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[17] = 18370.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[18] = 24650.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[19] = 31300.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[20] = 37810.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[0] = 0.027005219999999996;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[1] = 2.047759;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[2] = 10.821699;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[3] = 29.171999999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[4] = 59.1052;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[5] = 153.4204;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[6] = 218.8515;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[7] = 384.24899999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[8] = 700.4340000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[9] = 833.9520000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[10] = 976.3358999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[11] = 1793.55;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[12] = 2825.3399999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[13] = 3875.4100000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[14] = 8373.426;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[15] = 10049.487;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[16] = 13784.68;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[17] = 19711.01;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[18] = 26695.95;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[19] = 33553.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[20] = 40456.700000000004;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[0] = 0.01823877;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[1] = 1.5353649999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[2] = 8.352057;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[3] = 22.9152;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[4] = 46.284;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[5] = 122.9876;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[6] = 176.5575;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[7] = 309.563;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[8] = 576.4122;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[9] = 689.0835000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[10] = 804.5151;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[11] = 1494.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[12] = 2369.64;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[13] = 3300.21;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[14] = 7118.5740000000005;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[15] = 8537.865;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[16] = 11631.62;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[17] = 16643.22;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[18] = 22283.600000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[19] = 28107.4;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[20] = 33877.76;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[0] = 0.02277;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[1] = 1.817;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[2] = 9.723;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[3] = 26.4;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[4] = 53.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[5] = 139.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[6] = 199.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[7] = 349.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[8] = 642.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[9] = 766.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[10] = 894.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[11] = 1650.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[12] = 2604.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[13] = 3595.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[14] = 7746.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[15] = 9331.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[16] = 12740.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[17] = 18370.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[18] = 24650.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[19] = 31300.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[20] = 37810.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[0] = 0.029396069999999996;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[1] = 2.0259549999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[2] = 10.549455;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[3] = 28.248;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[4] = 56.4452;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[5] = 146.58;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[6] = 208.4775;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[7] = 362.611;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[8] = 663.8058;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[9] = 791.028;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[10] = 922.6418999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[11] = 1696.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[12] = 2671.704;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[13] = 3681.28;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[14] = 7931.904;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[15] = 9554.944;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[16] = 13045.76;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[17] = 18829.25;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[18] = 25290.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[19] = 32145.1;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[20] = 38868.68;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[0] = 0.01746459;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[1] = 1.6353;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[2] = 9.013221000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[3] = 24.7896;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[4] = 50.3804;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[5] = 133.31799999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[6] = 191.32049999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[7] = 336.087;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[8] = 621.3942;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[9] = 742.7384999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[10] = 867.1581;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[11] = 1605.45;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[12] = 2536.296;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[13] = 3505.125;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[14] = 7552.349999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[15] = 9097.725;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[16] = 12421.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[17] = 17892.38;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[18] = 23984.45;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[19] = 30423.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[20] = 36713.51;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[0] = 0.01389;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[1] = 1.203;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[2] = 6.541;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[3] = 17.97;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[4] = 35.93;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[5] = 93.78;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[6] = 133.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[7] = 235.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[8] = 435.1;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[9] = 518.3;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[10] = 604.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[11] = 1123.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[12] = 1761.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[13] = 2496.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[14] = 5202.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[15] = 6243.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[16] = 8512.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[17] = 12350.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[18] = 16400.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[19] = 20740.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[20] = 25340.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[0] = 0.02168229;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[1] = 1.749162;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[2] = 9.235892;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[3] = 24.90642;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[4] = 49.11631;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[5] = 125.38386;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[6] = 177.55140000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[7] = 307.40639999999996;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[8] = 558.2333;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[9] = 661.8690999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[10] = 768.3195;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[11] = 1400.3809999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[12] = 2162.508;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[13] = 3030.144;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[14] = 6398.46;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[15] = 7722.591;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[16] = 10631.488000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[17] = 15598.05;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[18] = 20910.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[19] = 26671.64;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[20] = 32789.96;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[0] = 0.009306299999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[1] = 0.8577390000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[2] = 4.781471000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[3] = 13.351709999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[4] = 26.98343;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[5] = 71.74170000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[6] = 103.2369;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[7] = 183.6912;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[8] = 345.03430000000003;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[9] = 412.5668;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[10] = 482.99549999999994;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[11] = 912.9989999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[12] = 1449.3029999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[13] = 2076.672;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[14] = 4255.236;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[15] = 5075.558999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[16] = 6843.648;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[17] = 9793.550000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[18] = 12874.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[19] = 16135.720000000001;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[20] = 19562.48;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[0] = 0.01389;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[1] = 1.203;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[2] = 6.541;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[3] = 17.97;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[4] = 35.93;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[5] = 93.78;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[6] = 133.9;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[7] = 235.2;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[8] = 435.1;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[9] = 518.3;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[10] = 604.5;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[11] = 1123.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[12] = 1761.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[13] = 2496.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[14] = 5202.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[15] = 6243.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[16] = 8512.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[17] = 12350.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[18] = 16400.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[19] = 20740.0;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[20] = 25340.0;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[0] = 0.01794588;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[1] = 1.336533;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[2] = 7.0708210000000005;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[3] = 19.191959999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[4] = 38.04987;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[5] = 98.28144;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[6] = 139.79160000000002;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[7] = 244.37279999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[8] = 449.4583;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[9] = 534.8856;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[10] = 623.2394999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[11] = 1153.321;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[12] = 1805.0249999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[13] = 2555.904;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[14] = 5321.646;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[15] = 6392.832;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[16] = 8716.288;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[17] = 12658.749999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[18] = 16826.4;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[19] = 21299.98;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[20] = 26049.52;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min( double kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[0] = 0.01061196;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[1] = 1.083903;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[2] = 6.076589;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[3] = 16.909769999999998;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[4] = 34.06164;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[5] = 89.65368;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[6] = 128.4101;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[7] = 226.73279999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[8] = 421.1768;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[9] = 502.23269999999997;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[10] = 586.365;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[11] = 1092.6789999999999;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[12] = 1715.214;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[13] = 2433.6;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[14] = 5071.95;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[15] = 6086.925;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[16] = 8290.688;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[17] = 12016.55;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[18] = 15940.8;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[19] = 20138.54;	 // [pb]
	kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[20] = 24579.8;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttx_NLOQCD_CT18nloxH1_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
