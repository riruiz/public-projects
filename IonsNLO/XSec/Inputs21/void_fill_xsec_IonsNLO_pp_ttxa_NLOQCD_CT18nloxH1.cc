// today is: 2024 2 21
// ////////////////
//  IonsNLO_pp_ttxa_NLOQCD   //
// ////////////////

void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[0] = 1.348e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[1] = 5.231e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[2] = 0.0006873;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[3] = 0.002868;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[4] = 0.007329;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[5] = 0.02601;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[6] = 0.04058;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[7] = 0.08061;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[8] = 0.1751;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[9] = 0.2141;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[10] = 0.263;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[11] = 0.5343;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[12] = 0.9138;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[13] = 1.383;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[14] = 3.203;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[15] = 4.017;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[16] = 5.684;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[17] = 8.629;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[18] = 11.83;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[19] = 15.6;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[20] = 19.57;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[0] = 1.7470080000000003e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[1] = 6.240583e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[2] = 0.0007986425999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[3] = 0.003301068;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[4] = 0.008377047;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[5] = 0.0296514;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[6] = 0.046058299999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[7] = 0.09052503;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[8] = 0.19681240000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[9] = 0.24043430000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[10] = 0.296401;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[11] = 0.5941416;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[12] = 1.014318;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[13] = 1.5295980000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[14] = 3.5136909999999997;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[15] = 4.414683;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[16] = 6.2069280000000004;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[17] = 9.40561;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[18] = 12.847380000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[19] = 16.9416;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[20] = 21.23345;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[0] = 1.012348e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[1] = 4.252803e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[2] = 0.0005738954999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[3] = 0.0024205919999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[4] = 0.006236978999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[5] = 0.02229057;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[6] = 0.03493938;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[7] = 0.0701307;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[8] = 0.1528623;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[9] = 0.18712340000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[10] = 0.229599;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[11] = 0.4728555;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[12] = 0.8123682;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[13] = 1.236402;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[14] = 2.895512;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[15] = 3.6313680000000006;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[16] = 5.17244;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[17] = 7.878277;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[18] = 10.85994;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[19] = 14.305200000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[20] = 17.90655;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[0] = 1.348e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[1] = 5.231e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[2] = 0.0006873;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[3] = 0.002868;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[4] = 0.007329;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[5] = 0.02601;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[6] = 0.04058;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[7] = 0.08061;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[8] = 0.1751;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[9] = 0.2141;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[10] = 0.263;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[11] = 0.5343;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[12] = 0.9138;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[13] = 1.383;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[14] = 3.203;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[15] = 4.017;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[16] = 5.684;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[17] = 8.629;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[18] = 11.83;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[19] = 15.6;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[20] = 19.57;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[0] = 2.36574e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[1] = 6.460285e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[2] = 0.0007821473999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[3] = 0.00316914;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[4] = 0.007973952;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[5] = 0.02788272;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[6] = 0.04325828;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[7] = 0.08528538000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[8] = 0.18385500000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[9] = 0.2241627;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[10] = 0.275098;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[11] = 0.5546034;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[12] = 0.9439553999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[13] = 1.425873;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[14] = 3.286278;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[15] = 4.117425;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[16] = 5.820416000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[17] = 8.836096;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[18] = 12.102089999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[19] = 15.9744;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[20] = 20.03968;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[0] = 6.5243200000000005e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[1] = 4.153414e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[2] = 0.0006013875;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[3] = 0.002601276;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[4] = 0.006764667;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[5] = 0.024397379999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[6] = 0.03826694;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[7] = 0.07649889;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[8] = 0.1673956;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[9] = 0.2051078;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[10] = 0.252217;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[11] = 0.5150652;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[12] = 0.8845584;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[13] = 1.34151;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[14] = 3.116519;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[15] = 3.912558;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[16] = 5.5362160000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[17] = 8.404646;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[18] = 11.52242;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[19] = 15.178799999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[20] = 19.04161;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_NLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[0] = 6.929e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[1] = 2.983e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[2] = 0.0004203;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[3] = 0.001755;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[4] = 0.004541;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[5] = 0.0158;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[6] = 0.02493;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[7] = 0.05028;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[8] = 0.1092;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[9] = 0.133;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[10] = 0.1613;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[11] = 0.335;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[12] = 0.576;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[13] = 0.8731;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[14] = 2.087;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[15] = 2.561;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[16] = 3.619;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[17] = 5.473;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[18] = 7.637;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[19] = 9.932;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[20] = 12.47;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[0] = 1.1321986e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[1] = 4.41484e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[2] = 0.000601029;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[3] = 0.002462265;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[4] = 0.006284743999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[5] = 0.0214722;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[6] = 0.03363057;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[7] = 0.06702324;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[8] = 0.143598;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[9] = 0.17423000000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[10] = 0.21049649999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[11] = 0.430475;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[12] = 0.7303679999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[13] = 1.0966136;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[14] = 2.558662;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[15] = 3.1244199999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[16] = 4.368133;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[17] = 6.529289;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[18] = 9.141489;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[19] = 11.977992;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[20] = 15.138580000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[0] = 4.441489e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[1] = 2.0910830000000002e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[2] = 0.0003034566;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[3] = 0.00128817;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[4] = 0.003373963;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[5] = 0.011929000000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[6] = 0.0189468;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[7] = 0.03856476;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[8] = 0.0847392;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[9] = 0.103607;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[10] = 0.12613660000000002;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[11] = 0.26532000000000006;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[12] = 0.461376;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[13] = 0.7054648;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[14] = 1.721775;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[15] = 2.1230689999999997;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[16] = 3.029103;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[17] = 4.630158;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[18] = 6.4303539999999995;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[19] = 8.283288;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[20] = 10.325160000000002;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XSec_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[0] = 6.929e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[1] = 2.983e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[2] = 0.0004203;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[3] = 0.001755;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[4] = 0.004541;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[5] = 0.0158;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[6] = 0.02493;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[7] = 0.05028;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[8] = 0.1092;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[9] = 0.133;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[10] = 0.1613;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[11] = 0.335;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[12] = 0.576;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[13] = 0.8731;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[14] = 2.087;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[15] = 2.561;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[16] = 3.619;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[17] = 5.473;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[18] = 7.637;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[19] = 9.932;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[20] = 12.47;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Cen[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[0] = 1.2292046000000002e-08;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[1] = 3.710852e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[2] = 0.00047956230000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[3] = 0.0019375200000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[4] = 0.004936067;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[5] = 0.0169218;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[6] = 0.02655045;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[7] = 0.05319624;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[8] = 0.11466000000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[9] = 0.139384;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[10] = 0.1687198;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[11] = 0.348065;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[12] = 0.595584;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[13] = 0.9001661;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[14] = 2.143349;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[15] = 2.627586;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[16] = 3.709475;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[17] = 5.609824999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[18] = 7.827924999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[19] = 10.180299999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[20] = 12.781749999999999;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Max[ii] *= gpbConv; // xsec [pb->fb]
	}
}
void fillIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min( double kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[] ){
	// take care to avoid seg fault here due to size of array
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[0] = 3.235843000000001e-09;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[1] = 2.329723e-05;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[2] = 0.0003648204;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[3] = 0.0015847650000000001;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[4] = 0.0041822610000000005;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[5] = 0.014804600000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[6] = 0.02348406;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[7] = 0.047715719999999996;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[8] = 0.104286;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[9] = 0.127281;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[10] = 0.15452539999999998;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[11] = 0.32260500000000003;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[12] = 0.5569919999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[13] = 0.8460339;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[14] = 2.030651;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[15] = 2.491853;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[16] = 3.521287;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[17] = 5.325228999999999;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[18] = 7.430801;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[19] = 9.653904;	 // [pb]
	kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[20] = 12.120840000000001;	 // [pb]
	for(unsigned int ii=0; ii<gnrBins; ii++){
		kIonsNLO_pp_ttxa_NLOQCD_CT18nloxH1_XPDF_XLO_Min[ii] *= gpbConv; // xsec [pb->fb]
	}
}
